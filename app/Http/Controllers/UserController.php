<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Group;
use App\Person;
use App\InvestmentDetails;
use Carbon\Carbon;
use App\SchemeDetails;
use App\HistoricNavs;
use PHPExcel_Calculation_Financial;
use Excel;
use App\GroupMembers;
use App\Queries;
use App\User;
use App\scheme_type;
use PDF;
use App\historic_nse;
use App\historic_bse;
use App\amcnames;
use App\midcap;
use App\smallcap;
use \NumberFormatter;
use ZipArchive;
use Response;
class UserController extends Controller
{

	public $rbi_fd = [
					['date' => '2015-01-15', 'value' => '7.75'],
					['date' => '2015-03-04', 'value' => '7.50'],
					['date' => '2015-06-02', 'value' => '7.25'],
					['date' => '2015-09-29', 'value' => '6.75'],
					['date' => '2016-04-05', 'value' => '6.50'],
					['date' => '2016-10-04', 'value' => '6.25'],
					['date' => '2016-12-07', 'value' => '6.25'],
					['date' => '2017-02-08', 'value' => '6.25'],
					['date' => '2017-04-06', 'value' => '6.25'],
					['date' => '2017-06-08', 'value' => '6.25'],
					['date' => '2017-08-02', 'value' => '6.20']
				];


	public function index(){
		if (\Auth::user()) {
		$schemes = SchemeDetails::all();
		$groups = Group::where('user_id',\Auth::user()->id)->get();
		$persons = Person::where('user_id',\Auth::user()->id)->get();
		$group_members = GroupMembers::where('user_id',\Auth::user()->id)->get();
		$twoday = Carbon::now()->addDays(2);
        $today = Carbon::now();
 
        $queries = Queries::
        where("date","<", $twoday)
    	->where("date", ">" ,$today)
    	->get();

    	// $SENSEX = file('https://finance.google.com/finance/getprices?q=SENSEX&f=d,o,h,l,c,');
    	// $master = [];
    	// foreach ($SENSEX as $value) {
    	// 	if (strpos($value, ',') !== false){
    	// 		$array = explode(",",$value);
    	// 		array_push($master,$array);
    	// 	}
    	// }
    	// $count = count($master);
    	// $sensex_val = (($master[$count - 1][1]-$master[$count - 2][1])/$master[$count - 2][1])*100;
    	// $sensex_val = round($sensex_val,2);

    	// $nifty = file('https://finance.google.com/finance/getprices?q=NIFTY&f=d,o,h,l,c,');
    	// $master2 = [];
    	// foreach ($nifty as $value) {
    	// 	if (strpos($value, ',') !== false){
    	// 		$array = explode(",",$value);
    	// 		array_push($master2,$array);
    	// 	}
    	// }
    	// $count2 = count($master2);
    	// $nifty_val = (($master2[$count2 - 1][1]-$master2[$count2 - 2][1])/$master2[$count2 - 2][1])*100;
    	// $nifty_val = round($nifty_val,2);

		return view('home')->with(compact('groups','persons','group_members','schemes','queries'));
		
		}
		else{
			return view('login');
		}
	}

    public function addGroup(Request $request){

    		$validator = \Validator::make($request->all(),[

				'group_name' => 'required',

			]);

			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator->errors());
			}
			else{
				$group_name = $request['group_name'];
				$group = new Group();
				$group->name = $group_name;
				$group->user_id = \Auth::user()->id;
				if($group->save()){
					return $group;
				}
				else{
					return response()->json('save failed');
				}
			}
    }

    public function removeGroup(Request $request){

    	$validator = \Validator::make($request->all(),[

				'group_id' => 'required',

			]);

			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator->errors());				
			}
			else{

				$investor_id = array();

				$remove_group = Group::where('id',$request['group_id'])->where('user_id',\Auth::user()->id)->delete();

				$group_mem_id = array();
					$group_mem_id = GroupMembers::where('user_id',\Auth::user()->id)->where('group_id',$request['group_id'])->pluck('id')->toArray();
										foreach ($group_mem_id as $member_id) {
											$investor_id[] = $member_id;
										}
					$remove_investment = InvestmentDetails::where('user_id',\Auth::user()->id)->whereIn('investor_id',$investor_id)
										->where('investor_type','subperson')
										->delete();

				/*$remove_investment = InvestmentDetails::where('investor_id',$request['group_id'])
				->where('investor_type','group')->where('user_id',\Auth::user()->id)->delete();*/
				if ($remove_group && $remove_investment) {
					return response()->json(['msg' => '1']);
				}
				else{
					return response()->json(['msg' => '0']);
				}
				
			}

    }

   //  public function addPerson(Request $request){

   //  	  $validator = \Validator::make($request->all(),[

			// 	'person_name' => 'required',

			// ]);

			// if ($validator->fails()) {
				
			// 	return redirect()->back()->withErrors($validator->errors());
				
			// }
			// else{

			// 	$person_name = $request['person_name'];
			// 	$person = new Person();
			// 	$person->name = $person_name;
			// 	$person->user_id = \Auth::user()->id;
			// 	if($person->save()){
			// 		return $person;
			// 	}
			// 	else{
			// 		return response()->json('save failed');
			// 	}

			// }
   //  }

    public function addPerson(Request $request)
    {
    	$validator = \Validator::make($request->all(),[

			'inv_name' => 'required',
			'pan' => 'required',
			'email_id' => 'required',
			'mobile_number' => 'required',
			'address' => 'required',

		]);
		if ($request['type'] == 'edit') {
			if ($request['investor_type'] == 'individual') {
		   			$save = Person::where('id',$request['group_id2'])
		   									->update(['user_id' => \Auth::user()->id,
		   									'name' => $request['inv_name'],
		   									'person_pan' => $request['pan'],
		   									'email' => $request['email_id'],
		   									'contact' => $request['mobile_number'],
		   									'address' => $request['address']
		   									]);
		   			if ($save == 1) {
		   				return response()->json(["msg" => 1]);
		   			}else{
		   				return response()->json(["msg" => 0]);
		   			}
	       	}else{
	       		$save = GroupMembers::where('id',$request['group_id2'])
	       									->update(['user_id' => \Auth::user()->id,
	       									  'member_name' => $request['inv_name'],
	       									  'member_pan' => $request['pan'],
	       									  'email' => $request['email_id'],
	       									  'contact' => $request['mobile_number'],
	       									  'address' => $request['address']
	       									]);
	       		if ($save == 1) {
	       			return response()->json(["msg" => 1]);
	       		}else{
	       			return response()->json(["msg" => 0]);
	       		}
	       	}
		}else{

			 	if ($request['investor_type'] == 'individual') {
		   			$save = Person::insert(['user_id' => \Auth::user()->id,
		   									'name' => $request['inv_name'],
		   									'person_pan' => $request['pan'],
		   									'email' => $request['email_id'],
		   									'contact' => $request['mobile_number'],
		   									'address' => $request['address']
		   									]);
		   			if ($save == 1) {
		   				return response()->json(["msg" => 1]);
		   			}else{
		   				return response()->json(["msg" => 0]);
		   			}
		       	}else{
		       		$save = GroupMembers::insert(['user_id' => \Auth::user()->id,
		       									  'group_id' => $request['group_id2'],
		       									  'member_name' => $request['inv_name'],
		       									  'member_pan' => $request['pan'],
		       									  'email' => $request['email_id'],
		       									  'contact' => $request['mobile_number'],
		       									  'address' => $request['address']
		       									]);
		       		if ($save == 1) {
		       			return response()->json(["msg" => 1]);
		       		}else{
		       			return response()->json(["msg" => 0]);
		       		}
		       		

		    	}
    }

		// dd($request['inv_name']);
    }


    public function removePerson(Request $request){

    	$validator = \Validator::make($request->all(),[

				'person_id' => 'required',

			]);

			if ($validator->fails()) {
				
				return redirect()->back()->withErrors($validator->errors());
				
			}
			else{

				$remove_person = Person::where('id',$request['person_id'])->delete();
				$remove_investment = InvestmentDetails::where('user_id',\Auth::user()->id)->where('investor_id',$request['person_id'])
				->where('investor_type','individual')->delete();

				if ($remove_person && $remove_investment) {
					return response()->json(['msg' => '1']);
				}
				else{
					return response()->json(['msg' => '0']);
				}
				
			}

    }


    public function addInvestment(Request $request){
    	$is_inv = 0;
    	$is_aum = 0;
    	if ($request['is_inv'] == "on") {
			// dd($request['is_inv']);
			$is_inv = 1;
    	}else{
    		// dd('data');
    		$is_inv = 0;
    	}
    	if ($request['is_aum'] == "on") {
			// dd($request['is_aum']);
			$is_aum = 1;
    	}else{
    		// dd('data');
    		$is_aum = 0;
    	}
    	
    	   $validator = \Validator::make($request->all(),[
    	   		'investor_id' => 'required',
    	   		'investor_type' => 'required',
				'scheme_name' => 'required',
				'dop' => 'required',
				'amt_inv' => 'required',
				'purchase_nav' => 'required',
				'folio_number' => 'required',

			]);


			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator->errors());
				
			}
			else{

				$scheme_name = SchemeDetails::where('scheme_code',$request['scheme_name'])->get();
				//echo $scheme_name[0];
				// dd($request['amt_inv'],$request['purchase_nav']);
				$date = date('Y-m-d', strtotime($request['dop']));
				$units = $request['amt_inv']/$request['purchase_nav'];
				//echo $units;
				//echo $date;
				$investment = new InvestmentDetails();
				$investment->user_id = \Auth::user()->id;
				$investment->investor_id = $request['investor_id'] ;
				$investment->investor_type = $request['investor_type'];
				$investment->scheme_name = $scheme_name[0]['scheme_name'];
				$investment->scheme_code = $request['scheme_name'];
				$investment->scheme_type = $scheme_name[0]['scheme_type'];
				$investment->amount_inv = $request['amt_inv'];
				$investment->investment_type = $request['inv_types'];
				$investment->purchase_date = $date;
				$investment->purchase_nav = $request['purchase_nav'];
				$investment->units = $units;
				$investment->folio_number = $request['folio_number'];
				$investment->is_notseen = 1;
				$investment->is_inv = $is_inv;
				$investment->option_type = $request['option_type'];
				$investment->not_aum = $is_aum;

				

				//print_r($last_inv);

				if ($investment->save()) {

					$latest_inv = InvestmentDetails::where('id',$investment->id)->get();

					$last_inv = array();
					$yesterday_date = Carbon::now()->subDay(1)->toDateString();
					$yesterday_date = date('Y-m-d',strtotime($yesterday_date));

					$profit_or_loss = 0;
					$total_amount = 0;
					$total_m_value = 0;
					$absolute_returns_avg = 0;
					$count = 0;
					foreach ($latest_inv as $investment) {

						$last_inv['investor_id'] = $investment->investor_id;
						$last_inv['investment_id'] = $investment->id;
						$last_inv['investor_type'] = $investment->investor_type;

						$last_inv['scheme_name'] = $investment->scheme_name;
						$last_inv['scheme_type'] = $investment->scheme_type;
						$last_inv['purchase_date'] =  $investment->purchase_date;
						$last_inv['amount_inv'] =  $investment->amount_inv;
						$last_inv['purchase_nav'] =  $investment->purchase_nav;
						$last_inv['units'] =  $investment->units;
						$current_nav = HistoricNavs::where('scheme_code',$investment->scheme_code)->orderBy('date','aesc')->first();
						$last_inv['current_nav'] = $current_nav['nav'];

						$current_m_value = $current_nav['nav'] * $investment->units;
						$last_inv['current_m_value'] = $current_m_value;

						$initial_value = $investment->amount_inv;
						$last_inv['p_or_loss'] = round((($investment->units) * $current_nav['nav']) - (($investment->units) * ($investment->purchase_nav)),2);
						$last_inv['folio_number'] =  $investment->folio_number;
						$absolute_returns = (($current_m_value - $initial_value)/$initial_value) * 100;

						$today = Carbon::now()->subDay(1)->toDateString();
						$today = date('Y-m-d',strtotime($today));
						$purchase_date = $investment->purchase_date;

						$date_diff = date_diff(date_create($purchase_date),date_create($today));
						$date_diff = $date_diff->format("%a");

						if ($date_diff == 0) {
							$annualised_return = 0;
						}else{
							$annualised_return = (($absolute_returns * 365)/$date_diff) ;
						}



						$last_inv['ann_returns'] = $annualised_return;
						$last_inv['abs_returns'] = $absolute_returns;

						$total_amount_invested = 
						$last_inv['total_amt_inv'] = $absolute_returns;
						
						
					}

					//$investor_id = $request['investor_id'];
					$investment_details = InvestmentDetails::where('investor_id',$request['investor_id'])->where('investor_type',$request['investor_type'])->where('user_id',\Auth::user()->id)->get();
					

						foreach ($investment_details as $investment) {
							
							if($investment['is_inv'] == 0){
								$total_amount += $investment->amount_inv;
							}
							$current_nav = HistoricNavs::where('scheme_code',$investment->scheme_code)->orderBy('date','aesc')->first();
							$current_m_value = ($investment->units) * $current_nav['nav']; 
							$total_m_value += $current_m_value;
							if ($investment['is_inv'] == 0) {
								$profit_or_loss +=  (($investment->units) * $current_nav['nav']) - (($investment->units) * ($investment->purchase_nav));
							}
							$count++;
							$initial_value = ($investment->purchase_nav) * ($investment->units);
							$absolute_returns = (($current_m_value - $initial_value)/$initial_value) * 100;
							// $absolute_returns_avg += $absolute_returns;
							$count++;
						}
							$absolute_returns_avg = (($total_m_value - $total_amount)/$total_amount)*100;
							$absolute_returns_avg = round($absolute_returns_avg,2);
							//$current_m_value = round($current_m_value,2);
							$total_amount_inv = round($total_amount,2);
							$profit_or_loss = round($profit_or_loss,2);
							$total_m_value = round($total_m_value,2);

							//echo $total_m_value;


					return response()->json(['msg'=>'1','investment' => $last_inv,'total_amount_inv'=>$total_amount_inv,'absolute_returns_avg'=>$absolute_returns_avg,'profit_or_loss'=>$profit_or_loss,'total_m_value'=>$total_m_value]);
				}
				else{
					return response()->json(['msg'=>'0']);
				}
				


			}
    }

    public function getNav(){



        // Increasing the Maximum execution time and setting it to default value at End of the function 
        
         /*$scheme_code contains the scheme code of the 20 funds. First 5 are Debt funds, Followed by 4 balanced fund, and 11 Equity funds*/

         $scheme_code = SchemeDetails::get()->pluck('scheme_code');

         // dd($scheme_code);


        //$scheme_code = array("106212","112423","114239","128053","111803","133805","133926",  "104685","100122","118191","131666","100081","100520","118102","100349","112090","105758","103360","113177","101672","104481","107745","103196","101979","100499","101350","101862","102142","103174","103504","106235","112092","112496","112600","112938","117716","123690","101537","112096","107249");
        
        $count = 0;
        // $file = file("http://portal.amfiindia.com/spages//NAV0.txt");
        $file = file("https://www.amfiindia.com/spages/NAVAll.txt");

        foreach ($scheme_code as $searchthis) {
            foreach ($file as $line) {
                if (strpos($line, $searchthis) !== FALSE) {
                    $line_exp = explode(";", $line);                    
                    $date = date("Y-m-d",strtotime($line_exp[7]));
                    \Log::info($date.' : '.$line_exp[0].' : '.$line_exp[4]);
                    $store_nav = new HistoricNavs();
                    $store_nav->scheme_code = $line_exp[0];
                    $store_nav->scheme_name = $line_exp[3];

                    $store_nav->nav = $line_exp[4];
                    $store_nav->date = $date;
                    $store_nav->save();

                    //echo $line_exp[3]."<br>";

                    // Storing in historic_navs

                    /*$historic_navs = new HistoricNav();
                    $historic_navs->scheme_code = $line_exp[0];
                    $historic_navs->scheme_name = $line_exp[2];
                    $historic_navs->date = $date;
                    $historic_navs->nav = $line_exp[4];
                    $historic_navs->save();*/


                }
            }               
        }
    }

    public function getInvestment(Request $request){
		
		$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );

    	   	$validator = \Validator::make($request->all(),[
    	   		'investor_id' => 'required',
    	   		'investor_type' => 'required',
    	   		'date' => 'required',
			]);

			if ($validator->fails()) {
				
				return redirect()->back()->withErrors($validator->errors());
				//return "date varala da";
				
			}
			else{

				$todayDate = Carbon::now()->format('Y-m-d');
  //   	$startTimeStamp = strtotime($request['date']);
		// $endTimeStamp = strtotime($todayDate);

		// $timeDiff = abs($endTimeStamp - $startTimeStamp);

		// $numberDays = $timeDiff/86400;  
		// $numberDays = intval($numberDays) + 3;

  //   	$SENSEX = file('https://finance.google.com/finance/getprices?q=SENSEX&p='.$numberDays.'d&f=d,o,h,l,c,');
  //   	$master = [];
  //   	foreach ($SENSEX as $value) {
  //   		if (strpos($value, ',') !== false){
  //   			$array = explode(",",$value);
  //   			array_push($master,$array);
  //   		}
  //   	}
  //   	$sensex_val = (($master[2][1]-$master[1][1])/$master[1][2])*100;
  //   	$sensex_val = round($sensex_val,2);

  //   	$nifty = file('https://finance.google.com/finance/getprices?q=NIFTY&p='.$numberDays.'d&f=d,o,h,l,c,');
  //   	$master2 = [];
  //   	foreach ($nifty as $value) {
  //   		if (strpos($value, ',') !== false){
  //   			$array = explode(",",$value);
  //   			array_push($master2,$array);
  //   		}
  //   	}
  //   	$nifty_val = (($master2[2][1]-$master2[1][1])/$master2[1][2])*100;
  //   	$nifty_val = round($nifty_val,2);


				$myDate = date('Y-m-d',strtotime ($request['date']));
				$nifty_day_one = historic_nse::where('date','<=',$myDate)->orderBy('date','desc')->first();
				$nifty_day_two = historic_nse::where('date','<',$nifty_day_one['date'])->orderBy('date','desc')->first();
				$nifty_val = (($nifty_day_one['closing_index']-$nifty_day_two['closing_index'])/$nifty_day_two['closing_index'])*100;
				$nifty_val = round($nifty_val, 2);

				$sensex_day_one = historic_bse::where('date','<=',$myDate)->orderBy('date','desc')->first();
				$sensex_day_two = historic_bse::where('date','<',$sensex_day_one['date'])->orderBy('date','desc')->first();
				$sensex_val = (($sensex_day_one['closing_index']-$sensex_day_two['closing_index'])/$sensex_day_two['closing_index'])*100;
				$sensex_val = round($sensex_val, 2);

				$smallcap_day_one = smallcap::where('date','<=',$myDate)->orderBy('date','desc')->first();
				$smallcap_day_two = smallcap::where('date','<',$smallcap_day_one['date'])->orderBy('date','desc')->first();
				$smallcap_val = (($smallcap_day_one['close']-$smallcap_day_two['close'])/$smallcap_day_two['close'])*100;
				$smallcap_val = round($smallcap_val, 2);

				$midcap_day_one = midcap::where('date','<=',$myDate)->orderBy('date','desc')->first();
				$midcap_day_two = midcap::where('date','<',$midcap_day_one['date'])->orderBy('date','desc')->first();
				$midcap_val = (($midcap_day_one['close']-$midcap_day_two['close'])/$midcap_day_two['close'])*100;
				$midcap_val = round($midcap_val, 2);

				// dd($midcap_day_one,$midcap_day_two);

				$investor_type = $request['investor_type'];
				$investor_id = $request['investor_id'];


				//echo $investor_type;
				//echo $investor_id;

				if ($investor_type == "subperson") {
					
					$pan = GroupMembers::where('user_id',\Auth::user()->id)->where('id',$investor_id)->pluck('member_pan');
					//cho "The pan is".$pan[0];
				}


				if ($investor_type == "individual") {
					
					$pan = Person::where('user_id',\Auth::user()->id)->where('id',$investor_id)->pluck('person_pan');
					//echo "The pan is".$pan[0];
				}

				$yesterday_date = Carbon::now()->subDay(1)->toDateString();
				$yesterday_date = date('Y-m-d',strtotime($yesterday_date));


				if ($request['date'] == 'empty') {
								
				$investments = InvestmentDetails::where('user_id',\Auth::user()->id)->where('investor_id',$request['investor_id'])
				->where('investor_type',$request['investor_type'])
				->where('purchase_date','<=',$yesterday_date)
				->get();

				$total_amount = InvestmentDetails::where('user_id',\Auth::user()->id)->where('investor_id',$request['investor_id'])
				->where('investor_type',$request['investor_type'])
				->where('purchase_date','<=',$yesterday_date)
				->where('is_inv', 0)
				->sum('amount_inv');


								
				}

				if ($request['date'] != 'empty') {
				$change_date = $request['date'];
				$change_date = date('Y-m-d',strtotime($change_date));

				$investments = InvestmentDetails::where('user_id',\Auth::user()->id)->where('investor_id',$request['investor_id'])
				->where('investor_type',$request['investor_type'])
				->where('purchase_date','<=',$change_date)
				->get();


				$total_amount = InvestmentDetails::where('user_id',\Auth::user()->id)->where('investor_id',$request['investor_id'])
				->where('investor_type',$request['investor_type'])
				->where('purchase_date','<=',$change_date)
				->where('is_inv', 0)
				->sum('amount_inv');

				//return $total_amount;
					
				}

				


				

				$count = 0;
				$investment_details = array();
				$current_m_value = 0;
				$profit_or_loss = 0;
				//$total_amount = 0;
				$total_m_value = 0;
				$absolute_returns_avg = 0;
				$nav_date = '';
				$xirr_total = 0;

				


				/*if (empty($total_amount)) {
					$total_amount = 0;
				}*/

				if (count($investments) == 0) {
					return response()->json(['msg'=>'0','investment_details'=>$investment_details,'total_amount'=>$total_amount,'total_m_value'=>$total_m_value,'profit_or_loss'=>$profit_or_loss,'absolute_return_avg' => $absolute_returns_avg,'pan'=>$pan,'inv_id'=>$investor_id,'inv_type'=>$investor_type]);
				}

				else{
					$date_Array = [];
					$amount_Array = [];
					foreach ($investments as $investment) {
						// dd($investment['is_inv']);
						$Scheme_sub = SchemeDetails::where('scheme_code',$investment->scheme_code)->value('scheme_sub');
						if ($Scheme_sub != "liquid" && $investment['is_inv'] == 0) {

							array_push($date_Array, $investment->purchase_date);
							array_push($amount_Array, (-1 * $investment->amount_inv));
						}

						/*sensex & nifty cal*/
						// $inv_date = $investment->purchase_date;
						// $inv_amt = $investment->amount_inv;
						// $dateOrder = date('Y-m-d',strtotime ($request['date']));

						// $get_sensex = historic_bse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
						// $buy_sensex = $inv_amt/$get_sensex['closing_index'];
						// $current_sensex = historic_bse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
						// $current_sensex_val = $buy_sensex * $current_sensex['closing_index'];
						// $current_sensex_val = round($current_sensex_val, 2);

						// $get_nifty = historic_nse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
						// $buy_nifty = $inv_amt/$get_nifty['closing_index'];
						// $current_nifty = historic_nse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
						// $current_nifty_val = $buy_nifty * $current_nifty['closing_index'];
						// $current_nifty_val = round($current_nifty_val, 2);

						// $Scheme_type = SchemeDetails::where('scheme_code',$investment->scheme_code)->value('scheme_type');
						// dd($Scheme_type);
						// $investment_details[$count]['invest_id'] = $request['id'];
						$investment_details[$count]['investor_id'] = $request['investor_id'];
						$investment_details[$count]['investor_type'] = $request['investor_type'];
						$investment_details[$count]['investment_id'] = $investment->id;
						$investment_details[$count]['scheme_name'] = $investment->scheme_name;
						$investment_details[$count]['scheme_type'] = $Scheme_sub;
						$investment_details[$count]['dop'] = $investment->purchase_date;
						$investment_details[$count]['amount_inv'] = $fmt->format($investment->amount_inv);
						$investment_details[$count]['purchase_nav'] = $investment->purchase_nav;
						$investment_details[$count]['units'] = round($investment->units,4);
						// $investment_details[$count]['sensex'] = $current_sensex_val;
						// $investment_details[$count]['nifty'] = $current_nifty_val;


						//$current_nav = HistoricNavs::where('scheme_code','106212')->first()->pluck('nav');


							if ($request['date'] == 'empty') {
								$current_nav = HistoricNavs::where('scheme_code',$investment->scheme_code)->orderBy('date','aesc')->first();
								//return $current_nav;
								//echo $current_nav['nav'];
								$nav_date = $current_nav['date'];
								$current_nav = $current_nav['nav'];
								
								//echo $current_nav['nav'];
								//echo $nav_date;
								//echo $current_nav;
								//echo "empty date";
								
							}

							if ($request['date'] != 'empty') {

								$change_date = $request['date'];



								$change_date = date('Y-m-d',strtotime($change_date));
								//return $change_date;
								//return $change_date;
								$current_nav = HistoricNavs::where('scheme_code',$investment->scheme_code)->where('date','<=' ,$change_date)->orderBy('date','aesc')->first();

								//return $current_nav;
								$nav_date = $current_nav['date'];
								$current_nav = $current_nav['nav'];
								// dd($nav_date);
								//echo "non empty";
								//return $nav_date;

								/*foreach ($current_nav as $nav) {
									$pass_nav = $nav->nav;
								}*/
								$nav_date_one = HistoricNavs::where('scheme_code',$investment->scheme_code)->where('date','<=',$change_date)->orderBy('date','desc')->first();

								$nav_date_two = HistoricNavs::where('scheme_code',$investment->scheme_code)->where('date','<',$nav_date_one['date'])->orderBy('date','desc')->first();
								$navState = (($nav_date_one['nav']-$nav_date_two['nav'])/$nav_date_two['nav']) * 100;
								$navState = round($navState, 2);
							}


						//return $nav_date;
						$nav_date = date('d-m-Y',strtotime($nav_date));
						//return $nav_date;
						//return $nav_date;
						//echo $current_nav['nav'];
						//echo $investment->dop;
						$profi_loss = round((($investment->units) * $current_nav) - (($investment->units) * ($investment->purchase_nav)),2);
						$investment_details[$count]['current_nav'] = $current_nav;
						$investment_details[$count]['current_market_value'] = $fmt->format(round(($investment->units) * $current_nav,2));
						$investment_details[$count]['folio_number'] = $investment->folio_number;
						$investment_details[$count]['p_or_loss'] = $fmt->format($profi_loss);
						// $investment_details[$count]['delta'] = $navState;
						
						

						$current_m_value = ($investment->units) * $current_nav; 
						$total_m_value += $current_m_value;
						if ($Scheme_sub != "liquid") {
							$xirr_total += $current_m_value;
						}


							// $profit_or_loss +=  (($investment->units) * $current_nav) - (($investment->units) * ($investment->purchase_nav));
							$initial_value = ($investment->purchase_nav) * ($investment->units);
							$absolute_returns = (($current_m_value - $initial_value)/$initial_value) * 100;




						//echo $absolute_returns;
						$investment_details[$count]['abs_returns'] = round($absolute_returns,2);

						// $absolute_returns_avg += $absolute_returns;
						//echo $absolute_returns;
						//echo $absolute_returns_avg;

						$today = Carbon::now()->subDay(1)->toDateString();
						$today = date('Y-m-d',strtotime($today));
						$purchase_date = $investment->purchase_date;

						$date_diff = date_diff(date_create($purchase_date),date_create($today));
						$date_diff = $date_diff->format("%a");

						if ($date_diff == 0) {
							$annualised_return = 0;
						}else{
							$annualised_return = (($absolute_returns * 365)/$date_diff) ;
						}
						$investment_details[$count]['ann_returns'] = round($annualised_return,2);

						
						$count++;
				}

				//echo ($absolute_returns_avg/$count);
				$profit_or_loss = $total_m_value - $total_amount;

				$absolute_returns_avg = (($total_m_value - $total_amount)/$total_amount) * 100;

				$absolute_returns_avg = round(($profit_or_loss/$total_amount)*100,2);
				$current_m_value = round($current_m_value,2);
				$profit_or_loss = round($profit_or_loss,2);
				$total_m_value = round($total_m_value,2);

				//print_r($total_amount);
				//print_r($current_m_value);
				//print_r($profit_or_loss);

				//return $investment_details;
				//print_r($current_nav);*/

				//return $total_amount;
				array_push($date_Array, $change_date);
				array_push($amount_Array, $xirr_total);
				$xirr_Val = PHPExcel_Calculation_Financial::XIRR($amount_Array,$date_Array);
				$xirr_Val = $xirr_Val * 100;
				$xirr_Val = round($xirr_Val, 2);

				$total_amount = $fmt->format($total_amount);
				$total_m_value = $fmt->format($total_m_value);
				$profit_or_loss = $fmt->format($profit_or_loss);

				// dd($amount_Array);	
				return response()->json(['msg'=>'1','investment_details'=>$investment_details,'total_amount'=>$total_amount,'total_m_value'=>$total_m_value,'profit_or_loss'=>$profit_or_loss,'absolute_return_avg' => $absolute_returns_avg,'pan'=>$pan,'inv_id'=>$investor_id,'inv_type'=>$investor_type,'nav_date'=>$nav_date,'sensex_val'=>$sensex_val,'nifty_val'=>$nifty_val,'xirr_value'=>$xirr_Val,'smallcap'=>$smallcap_val,'midcap'=>$midcap_val]);
				}




				
			}

    }

    public function downloadInvestments(Request $request){
    	set_time_limit(600);
    	$cis = $_SERVER['DOCUMENT_ROOT'].'/cis.zip';
    	$is = $_SERVER['DOCUMENT_ROOT'].'/Investment Summary.pdf';
   	    	if (file_exists($cis)) {
				unlink($cis);
   	    	}
   	    	if (file_exists($is)) {
				unlink($is);
   	    	}

    	   	$validator = \Validator::make($request->all(),[
    	   		'investor_id' => 'required',
    	   		'investor_type' => 'required',
    	   		'ex_nav_date' => 'required',
			]);

			if ($validator->fails()) {
				
				return redirect()->back()->withErrors($validator->errors());
				
			}
			else{

				$investor_id = $request['investor_id'];
				$investor_type = $request['investor_type'];


				if ($investor_type == 'cis') {

				$group_id = $request['investor_id'];
				$group_mem_id = array();
				$group_mem_id = GroupMembers::where('user_id',\Auth::user()->id)->where('group_id',$group_id)->pluck('id')->toArray();

				$count = 0;
				$cis_details = array();
				$yesterday_date = Carbon::now()->subDay(1)->toDateString();
				$yesterday_date = date('Y-m-d',strtotime($yesterday_date));
				$investment_detail = array();
				$total_m_value = 0;
				$profit_or_loss = 0;
				$absolute_returns_avg = 0;
				$total_amount_inv =0;
				$scheme_inv_total = 0;
				$current_market_value = 0;

				$grand_total = 0;
				$grand_market_value = 0;
				$grand_profit_or_loss = 0;
				$grand_absolute_returns = 0;
				$units = 0;

				$unique_scheme = array();

				$check_them = array();
				$investor_ids = array();
				$investor_id = array();
				$separate_inv = array();
				$names = [];

				$sipcount = 0;
				$liquidcount = 0;

				$inv_pass = array();

				$change_date = $request['ex_nav_date'];

				foreach ($group_mem_id as $member_id) {
					$investor_id[] = $member_id;
				}

				$investment_detail = InvestmentDetails::where('user_id',\Auth::user()->id)->whereIn('investor_id',$investor_id)->where('investor_type','subperson')->where('purchase_date','<=',$change_date)->orderBy('scheme_name', 'ASC')->get();

				foreach ($investment_detail as $sep_inv) {
					
						$investor_data = GroupMembers::where('id',$sep_inv->investor_id)->get();

						
						$investor_name = $investor_data[0]['member_name'];
						$investor_pan = $investor_data[0]['member_pan'];
						$investor_email = $investor_data[0]['email'];
						$investor_contact = $investor_data[0]['contact'];
						$investor_add = $investor_data[0]['address'];

						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Investor Name'] = $investor_name ;

						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Scheme Name'] = $sep_inv->scheme_name; 
						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Amount Invested'] = $sep_inv->amount_inv; 
						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Purchase date'] = date('d-M-Y',strtotime($sep_inv->purchase_date));

						$current_nav = HistoricNavs::where('scheme_code',$sep_inv->scheme_code)->where('date','<=' ,$change_date)->orderBy('date','aesc')->first();

						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Purchase NAV'] = round($sep_inv->purchase_nav, 2);
						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Units'] = round($sep_inv->units, 3);

						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current NAV'] = round($current_nav['nav'], 2);

						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'] = round($current_nav['nav'] * $sep_inv->units,2);

						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Unrealised Profit/Loss'] = round(($current_nav['nav'] * $sep_inv->units) - ($sep_inv->amount_inv),2);

						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Absolute Returns'] = round((($separate_inv[$sep_inv->investor_id][$sep_inv->id]['Unrealised Profit/Loss'])/($sep_inv->amount_inv)) * 100,2);

						$today = Carbon::now()->subDays(1)->toDateString();

						$date_diff = date_diff(date_create($separate_inv[$sep_inv->investor_id][$sep_inv->id]['Purchase date']),date_create($today));
						$date_diff = $date_diff->format("%a");
						if ($date_diff == 0) {
							$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Annualised Returns'] = 0;
						}else{
							$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Annualised Returns'] = round((($separate_inv[$sep_inv->investor_id][$sep_inv->id]['Absolute Returns'])*(365/$date_diff)),2);
						}

						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Folio Number'] = $sep_inv->folio_number;
						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['invest_type'] = $sep_inv->investment_type;
						$separate_inv[$sep_inv->investor_id]['one_time'] = count(InvestmentDetails::where('investor_id',$sep_inv->investor_id)->where('investment_type',1)->get());
						$separate_inv[$sep_inv->investor_id]['sip'] = count(InvestmentDetails::where('investor_id',$sep_inv->investor_id)->where('investment_type',2)->get());
						$separate_inv[$sep_inv->investor_id]['liquid'] = count(InvestmentDetails::where('investor_id',$sep_inv->investor_id)->where('investment_type',3)->get());



						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['pan'] = $investor_pan;
						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['email'] = $investor_email;
						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['contact'] = $investor_contact;
						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['address'] = $investor_add;

						
				}

					foreach ($investment_detail as $investments) {
						// dd($investments);
						if (array_key_exists($investments->scheme_code, $inv_pass)) {
							
							
							$inv_pass[$investments->scheme_code]['Scheme Name'] = $investments->scheme_name;

							$inv_pass[$investments->scheme_code]['Amount Invested'] += $investments->amount_inv;

							$current_nav = HistoricNavs::where('scheme_code',$investments->scheme_code)->where('date','<=' ,$change_date)->orderBy('date','aesc')->first();

							$inv_pass[$investments->scheme_code]['Units'] += round(($investments->units),4);
							$inv_pass[$investments->scheme_code]['Current NAV'] = $current_nav['nav'];
										
							$current_market_value = (($inv_pass[$investments->scheme_code]['Units']) * $current_nav['nav']);

							$current_market_value = round($current_market_value,2);
							$inv_pass[$investments->scheme_code]['Current Market Value'] = $current_market_value;

							$inv_pass[$investments->scheme_code]['Unrealised Profit/Loss'] = (($inv_pass[$investments->scheme_code]['Current Market Value']) - ($inv_pass[$investments->scheme_code]['Amount Invested']));

							$inv_pass[$investments->scheme_code]['Absolute Returns (%)'] = round(((($inv_pass[$investments->scheme_code]['Current Market Value']) - $inv_pass[$investments->scheme_code]['Amount Invested'])/($inv_pass[$investments->scheme_code]['Amount Invested'])) * 100,2);

							
						}
						else{

							$inv_pass[$investments->scheme_code]['Scheme Name'] = $investments->scheme_name;

							$inv_pass[$investments->scheme_code]['Amount Invested'] = $investments->amount_inv;
							
							$current_nav = HistoricNavs::where('scheme_code',$investments->scheme_code)->where('date','<=' ,$change_date)->orderBy('date','aesc')->first();
							$inv_pass[$investments->scheme_code]['Current NAV'] = $current_nav['nav'];

							$inv_pass[$investments->scheme_code]['Units'] = round(($investments->units),4);
										
							$current_market_value = (($investments->units) * $current_nav['nav']);

							$current_market_value = round($current_market_value,2);

							$inv_pass[$investments->scheme_code]['Current Market Value'] = $current_market_value;

							$inv_pass[$investments->scheme_code]['Unrealised Profit/Loss'] = (($inv_pass[$investments->scheme_code]['Current Market Value']) - ($inv_pass[$investments->scheme_code]['Amount Invested']));

							$inv_pass[$investments->scheme_code]['Absolute Returns (%)'] = round(((($inv_pass[$investments->scheme_code]['Current Market Value']) - $inv_pass[$investments->scheme_code]['Amount Invested'])/($inv_pass[$investments->scheme_code]['Amount Invested'])) * 100,2);


						}

						$grand_total += $investments->amount_inv;

						$current_nav = HistoricNavs::where('scheme_code',$investments->scheme_code)->where('date','<=' ,$change_date)->orderBy('date','aesc')->first();
						$current_nav = $current_nav['nav'];
						$units_held = $investments->units;

						$current_market_value = $units_held * $current_nav;

						$grand_market_value += $current_market_value;

						$grand_market_value = round($grand_market_value, 2);
						

						$count++;	
					}

							$final_array = array();

							$count = 0;
							$pro_array =  array();
							$member_name = '';
							$member_pan = ''; 
							$member_add = ''; 
							$member_email = ''; 
							$member_contact = ''; 
							$names = array();

							$profit_or_loss_total = $grand_market_value - $grand_total;

							$abs_total = (($grand_market_value - $grand_total)/$grand_total)*100;
							$abs_total = round($abs_total, 2);

							// dd($grand_total,$grand_market_value);
							//dd(array_push($names, 'var'));
							// dd($inv_pass);
							$pdf = PDF::loadView('mastercispdf', ['inv' => $inv_pass,'date' => $change_date, 'total'=>$grand_total, 'current'=>$grand_market_value, 'pl'=>$profit_or_loss_total, 'abs'=>$abs_total])->setPaper('a4', 'landscape')->setWarnings(false)->save('CIS Report.pdf');
							// dd('cis Done!!!!!!!!!!');
							
							$file_content = \Excel::create('CIS Report', function($excel) use ($inv_pass,$separate_inv,$change_date) {
			                $excel->sheet('CIS Report', function($sheet) use ($inv_pass)
			                {
			                    $sheet->fromArray($inv_pass);
			                    $sheet->setWidth('A', 50);
			                    $sheet->setWidth('B', 15);
			                    $sheet->setWidth('C', 20);
			                    $sheet->setWidth('D', 18);
			                    $sheet->setWidth('E', 20);
			                    $sheet->setWidth('F', 20);
			                    $sheet->setWidth('G', 20);

			                    
			                });

		                // dd($separate_inv);
			               
			               foreach ($separate_inv as $inv) {

			               		foreach($inv as $key => $in){
			               			if (is_numeric($key)) {
										$member_name = $in['Investor Name'];
										$member_pan = $in['pan'];
										$member_email = $in['email'];
										$member_contact = $in['contact'];
										$member_add = $in['address'];
			               			}
			               		}
			               		// dd($change_date);
			               		
								
			               		$pdf = PDF::loadView('cispdf', ['inv' => $inv, 'name' => $member_name, 'date' => $change_date, 'pan'=>$member_pan, 'email'=>$member_email, 'contact'=>$member_contact, 'address'=>$member_add])->setPaper('a4', 'landscape')->setWarnings(false)->save($member_name.'.pdf');
			               		// dd($inv);
								unset($inv['one_time']);
								unset($inv['sip']);
								unset($inv['liquid']);
								
				               		$excel->sheet($member_name, function($sheet) use ($inv)
					                {
					                 	$sheet->fromArray($inv);  
					                 	$sheet->setWidth('A', 20); 
					                 	$sheet->setWidth('B', 50); 
					                 	$sheet->setWidth('C', 20);
					                 	$sheet->setWidth('D', 15);
					                 	$sheet->setWidth('E', 15);
					                 	$sheet->setWidth('F', 15);
					                 	$sheet->setWidth('G', 15);
					                 	$sheet->setWidth('H', 25);
					                 	$sheet->setWidth('I', 25);
					                 	$sheet->setWidth('J', 18);
					                 	$sheet->setWidth('K', 25);
					                 	$sheet->setWidth('L', 20);

					                });
					                // array_push($names, $member_name);
					                // dd($names);
			               }
			            	})->store('xlsx',$_SERVER['DOCUMENT_ROOT']);

							foreach ($separate_inv as $inv) {

			               		foreach($inv as $key => $in){
			               			if (is_numeric($key)) {
										$member_name = $in['Investor Name'];
			               			}
			               		}
			               		array_push($names, $member_name);
			               	}

						$zip = new ZipArchive;
						if ($zip->open('cis.zip', ZipArchive::CREATE) === TRUE)
							{
							   foreach ($names as $value) {
								   	$zip->addFile($value.'.pdf');
							   } 
							   $zip->addFile('CIS Report.xlsx');
							   $zip->addFile('CIS Report.pdf'); 
							    $zip->close();
							   foreach ($names as $value) {
						   	    	$path = $_SERVER['DOCUMENT_ROOT'].'/'.$value.'.pdf';
									unlink($path);
							   }
							   $xls = $_SERVER['DOCUMENT_ROOT'].'/CIS Report.xlsx';
							   unlink($xls);
							   $pdf = $_SERVER['DOCUMENT_ROOT'].'/CIS Report.pdf';
							   unlink($pdf);
							}

						 $file= public_path(). "/cis.zip";

					     $headers = array(
				              'Content-Type: application/zip',
				            );

				return response()->download($file, 'cis.zip', $headers);

				}
				else{
					$change_date = $request['ex_nav_date'];
					$investor_id = $request['investor_id'];
					$investor_type = $request['investor_type'];


					$change_date = date('Y-m-d',strtotime($change_date));
					$investment_details = InvestmentDetails::where('user_id',\Auth::user()->id)
					->where('investor_type',$investor_type)
					->where('investor_id',$investor_id)
					->where('purchase_date','<=',$change_date)->orderBy('scheme_name','ASC')->get();

					//->orderby('purchase_date','desc')
					//->get();
					$investor_detail = [];
					$name = '';
					$email = '';
					$address = '';
					$contact = '';
					$pan = '';
					if ($investor_type == 'individual') {
						$investor_detail = Person::where('id', $investor_id)->get();
						$name = $investor_detail[0]['name'];
						$email = $investor_detail[0]['email'];
						$address = $investor_detail[0]['address'];
						$contact = $investor_detail[0]['contact'];
						$pan = $investor_detail[0]['person_pan'];
					}elseif ($investor_type == 'subperson') {
						$investor_detail = GroupMembers::where('id',$investor_id)->get();
						$name = $investor_detail[0]['member_name'];
						$email = $investor_detail[0]['email'];
						$address = $investor_detail[0]['address'];
						$contact = $investor_detail[0]['contact'];
						$pan = $investor_detail[0]['member_pan'];
					}
					$export = array();

					$export['one_time'] = count(InvestmentDetails::where('investor_id',$investor_id)->where('investment_type',1)->get());
					$export['sip'] = count(InvestmentDetails::where('investor_id',$investor_id)->where('investment_type',2)->get());
					$export['liquid'] = count(InvestmentDetails::where('investor_id',$investor_id)->where('investment_type',3)->get());


					//$investment_details = $investment_details->toArray();

					//print_r($investment_details);

					$yesterday_date = Carbon::now()->subDay(1)->toDateString();
					$yesterday_date = date('Y-m-d',strtotime($yesterday_date));
					foreach ($investment_details as $investment_detail) {

						$current_nav = HistoricNavs::where('scheme_code',$investment_detail->scheme_code)->where('date','<=' ,$change_date)->orderBy('date','aesc')->first();

						$current_market_value = ($investment_detail->units) * $current_nav['nav'];
						$p_or_loss = (($investment_detail->units) * $current_nav['nav']) - (($investment_detail->units) * ($investment_detail->purchase_nav));

						$today = Carbon::now()->subDay(1)->toDateString();
						$today = date('Y-m-d',strtotime($today));
						$purchase_date = $investment_detail->purchase_date;

						$date_diff = date_diff(date_create($purchase_date),date_create($today));
						$date_diff = $date_diff->format("%a");

						$abs_returns = (($current_market_value - $investment_detail->amount_inv)/$investment_detail->amount_inv) * 100;
						if ($date_diff == 0) {
							$ann_returns = 0;
						}else{
							$ann_returns = (($abs_returns *365)/$date_diff);
						}

						$date=date_create($investment_detail->purchase_date);
						$purchase_date = date_format($date,"d-M-y");

						$export[] = array(

							'Scheme Name' => $investment_detail->scheme_name,
							//'Scheme Type' => $investment_detail->scheme_type,
							'Folio Number' => $investment_detail->folio_number,
							'Purchase date' => $purchase_date,
							'Amount Invested' => $investment_detail->amount_inv,
							'Purchase NAV' => $investment_detail->purchase_nav,
							'Units' => round($investment_detail->units,4),
							'Current NAV' => $current_nav['nav'],
							'Current Market Value' => round($current_market_value,2),
							'Unrealised Profit/Loss' => round($p_or_loss,2),		
							'Absolute Returns' => round($abs_returns,2),
							'Annualised Returns' => round($ann_returns,2),
							'invest_type' => $investment_detail->investment_type
							);
					}



					$pdf = PDF::loadView('cispdf', ['inv' => $export,'date' => $change_date, 'name' => $name, 'address' => $address, 'email'=>$email, 'contact'=>$contact, 'pan'=>$pan])->setPaper('a4', 'landscape')->setWarnings(false)->save('Investment Summary.pdf');
					//print_r($export);
					unset($export['one_time']);
					unset($export['sip']);
					unset($export['liquid']);
					$file_content = \Excel::create('Investment Summary', function($excel) use ($export) {
	                $excel->sheet('Investment Summary', function($sheet) use ($export)
	                {
	                    $sheet->fromArray($export);
	                });
	            	})->store('xlsx',$_SERVER['DOCUMENT_ROOT']);
						$zip = new ZipArchive;
						if ($zip->open('Investment Summary.zip', ZipArchive::CREATE) === TRUE)
							{
								// ADDING FILES TO ZIP
							   	$zip->addFile('Investment Summary.pdf');
							    $zip->addFile('Investment Summary.xlsx');
							    $zip->close();
							    // REMOVING FILES FROM SERVER
					   	    	$pdf = $_SERVER['DOCUMENT_ROOT'].'/Investment Summary.pdf';
					   	    	$xls = $_SERVER['DOCUMENT_ROOT'].'/Investment Summary.xlsx';
								unlink($pdf);
								unlink($xls);
							}

						 $file= public_path(). "/Investment Summary.zip";

					     $headers = array(
				              'Content-Type: application/zip',
				            );

				return response()->download($file, 'Investment Summary.zip', $headers);
				}
            	
			}
    }


    public function addSubPerson(Request $request){
    	   	$validator = \Validator::make($request->all(),[

				'sub_person_name' => 'required',
				'group_id' => 'required',

			]);

			if ($validator->fails()) {
				
				return redirect()->back()->withErrors($validator->errors());
				
			}
			else{

				$sub_person_name = $request['sub_person_name'];
				$group_id = $request['group_id'];
				//return $group_id;
				$sub_person = new GroupMembers();
				$sub_person->user_id = \Auth::user()->id;
				$sub_person->group_id = $group_id;
				$sub_person->member_name = $sub_person_name;
				if($sub_person->save()){
					return $sub_person;
				}
				else{
					return response()->json('save failed');
				}

			}
    }

    public function removeSubPerson(Request $request){
    	$validator = \Validator::make($request->all(),[

				'sub_person_id' => 'required',

			]);

			if ($validator->fails()) {
				
				return redirect()->back()->withErrors($validator->errors());
				
			}
			else{

				$remove_sub_person = GroupMembers::where('user_id',\Auth::user()->id)->where('id',$request['sub_person_id'])->delete();
				$investments = InvestmentDetails::where('user_id',\Auth::user()->id)->where('investor_id',$request['sub_person_id'])->where('investor_type','subperson')->get();

				$remove_investments = InvestmentDetails::where('user_id',\Auth::user()->id)->where('investor_id',$request['sub_person_id'])->where('investor_type','subperson')->delete();

					if (count($investments) == 0) {
						
						//echo "zero inv";
						if ($remove_sub_person) {
							return response()->json(['msg' => '1']);
						}
						else{
							return response()->json(['msg' => '0']);
						}
					}

					if (count($investments) != 0) {
						//echo "investment is there";
						if ($remove_sub_person && $remove_investments) {
							return response()->json(['msg' => '1']);
						}
						else{
							return response()->json(['msg' => '0']);
						}
					}				
			}
    }


    public function cis(Request $request){
		$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );

    	    $validator = \Validator::make($request->all(),[

				'group_id' => 'required',
				'date' => 'required',

			]);


			if ($validator->fails()) {
				
				return redirect()->back()->withErrors($validator->errors());
				
			}
			else{
				$group_id = $request['group_id'];
				$group_mem_id = array();
				$group_mem_id = GroupMembers::where('user_id',\Auth::user()->id)->where('group_id',$group_id)->pluck('id')->toArray();

				$count = 0;
				$cis_details = array();
				$yesterday_date = Carbon::now()->subDay(1)->toDateString();
				$yesterday_date = date('Y-m-d',strtotime($yesterday_date));
				$investment_detail = array();
				$total_m_value = 0;
				$profit_or_loss = 0;
				$absolute_returns_avg = 0;
				$total_amount_inv =0;
				$scheme_inv_total = 0;
				$current_market_value = 0;
				$xirr_market_val = 0;

				$grand_total = 0;
				$grand_market_value = 0;
				$grand_profit_or_loss = 0;
				$grand_absolute_returns = 0;
				$units = 0;
				$change_date = '';

				$current_nav = '';

				$unique_scheme = array();

				$check_them = array();
				$investor_ids = array();

				$inv_pass = array();
				$cis_date = '';

					foreach ($group_mem_id as $member_id) {
						$investor_id[] = $member_id;
					}

					if ($request['date'] == 'empty') {

						$investment_detail = InvestmentDetails::where('user_id',\Auth::user()->id)->whereIn('investor_id',$investor_id)
						->where('investor_type','subperson')
						->where('purchase_date','<=',$yesterday_date)
						->get();
					}
					if ($request['date'] != 'empty') {

						$change_date = $request['date'];
						$change_date = date('Y-m-d',strtotime($change_date));
						$investment_detail = InvestmentDetails::where('user_id',\Auth::user()->id)->whereIn('investor_id',$investor_id)
						->where('investor_type','subperson')
						->where('purchase_date','<=',$change_date)
						->get();
					}

					$dateArray = [];
					$amountArray = [];


					foreach ($investment_detail as $investments) {
						$Scheme_sub = SchemeDetails::where('scheme_code',$investments->scheme_code)->value('scheme_sub');
						if ($Scheme_sub != "liquid") {
							array_push($dateArray, $investments->purchase_date);
							array_push($amountArray, (-1 * $investments->amount_inv));
						}


						// 						/*sensex & nifty cal*/
						// $inv_date = $investments->purchase_date;
						// $inv_amt = $investments->amount_inv;
						// $dateOrder = date('Y-m-d',strtotime ($request['date']));

						// $get_sensex = historic_bse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
						// $buy_sensex = $inv_amt/$get_sensex['closing_index'];
						// $current_sensex = historic_bse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
						// $current_sensex_val = $buy_sensex * $current_sensex['closing_index'];
						// $current_sensex_val = round($current_sensex_val, 2);

						// $get_nifty = historic_nse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
						// $buy_nifty = $inv_amt/$get_nifty['closing_index'];
						// $current_nifty = historic_nse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
						// $current_nifty_val = $buy_nifty * $current_nifty['closing_index'];
						// $current_nifty_val = round($current_nifty_val, 2);

						if ($request['date'] == 'empty') {
								$current_nav = HistoricNavs::where('scheme_code',$investments->scheme_code)->orderBy('date','aesc')->first();
								//return $current_nav;
								//echo $current_nav['nav'];
								$nav_date = $current_nav['date'];
								$change_date = date('d-m-Y',strtotime($nav_date));

								$current_nav = $current_nav['nav'];
								
								//echo $current_nav['nav'];
								//echo $nav_date;
								//echo $current_nav;
								//echo "empty date";
								
							}

							if ($request['date'] != 'empty') {

								$change_date = $request['date'];
								//return $change_date;
								$change_date = date('Y-m-d',strtotime($change_date));
								//return $change_date;
								$current_nav = HistoricNavs::where('scheme_code',$investments->scheme_code)->where('date','<=' ,$change_date)->orderBy('date','aesc')->first();
								$nav_date_one = HistoricNavs::where('scheme_code',$investments->scheme_code)->orderBy('date','desc')->first();
								$nav_date_two = HistoricNavs::where('scheme_code',$investments->scheme_code)->where('date','<',$nav_date_one['date'])->orderBy('date','desc')->first();
								$navState = (($nav_date_one['nav']-$nav_date_two['nav'])/$nav_date_two['nav']) * 100;
								$navState = round($navState, 2);
								// dd($navState);
									//return $current_nav;
								$nav_date = $current_nav['date'];
								$change_date = date('d-m-Y',strtotime($nav_date));
								//return $nav_date;
								$current_nav = $current_nav['nav'];
								
								//echo "non empty";
								//return $nav_date;

								/*foreach ($current_nav as $nav) {
									$pass_nav = $nav->nav;
								}*/
							}

						if (array_key_exists($investments->scheme_code, $inv_pass)) {
							// $inv_pass[$investments->scheme_code]['sensex'] = $current_sensex_val;
							// $inv_pass[$investments->scheme_code]['nifty'] = $current_nifty_val;
							// $inv_pass[$investments->scheme_code]['delta'] = $navState;

							$inv_pass[$investments->scheme_code]['total_amount_inv'] += $investments->amount_inv;
							$inv_pass[$investments->scheme_code]['scheme_name'] = $investments->scheme_name;



							$inv_pass[$investments->scheme_code]['units'] += ($investments->units);
							//$inv_pass[$investments->scheme_code]['units'] = $units;
							$inv_pass[$investments->scheme_code]['current_nav'] = $current_nav;

										
							$current_market_value = (($inv_pass[$investments->scheme_code]['units']) * $current_nav);

							$current_market_value = round($current_market_value,2);
							$inv_pass[$investments->scheme_code]['current_market_value'] = $current_market_value;

							$inv_pass[$investments->scheme_code]['profit_or_loss'] = (($inv_pass[$investments->scheme_code]['current_market_value']) - ($inv_pass[$investments->scheme_code]['total_amount_inv']));

							$inv_pass[$investments->scheme_code]['absolute_returns'] = round(((($inv_pass[$investments->scheme_code]['current_market_value']) - $inv_pass[$investments->scheme_code]['total_amount_inv'])/($inv_pass[$investments->scheme_code]['total_amount_inv'])) * 100,2);
						}
						else{
							// $inv_pass[$investments->scheme_code]['sensex'] = $current_sensex_val;
							// $inv_pass[$investments->scheme_code]['nifty'] = $current_nifty_val;
							// $inv_pass[$investments->scheme_code]['delta'] = $navState;

							$inv_pass[$investments->scheme_code]['total_amount_inv'] = $investments->amount_inv;
							$inv_pass[$investments->scheme_code]['scheme_name'] = $investments->scheme_name;


							$inv_pass[$investments->scheme_code]['current_nav'] = $current_nav;

							$inv_pass[$investments->scheme_code]['units'] = ($investments->units);
										
							$current_market_value = (($investments->units) * $current_nav);

							$current_market_value = round($current_market_value,2);

							$inv_pass[$investments->scheme_code]['current_market_value'] = $current_market_value;

							$inv_pass[$investments->scheme_code]['profit_or_loss'] = (($inv_pass[$investments->scheme_code]['current_market_value']) - ($inv_pass[$investments->scheme_code]['total_amount_inv']));

							if ($investments->scheme_code == "106212") {
								//echo $inv_pass[$investments->scheme_code]['profit_or_loss']."<br>";
							}

							$inv_pass[$investments->scheme_code]['absolute_returns'] = round(((($inv_pass[$investments->scheme_code]['current_market_value']) - $inv_pass[$investments->scheme_code]['total_amount_inv'])/($inv_pass[$investments->scheme_code]['total_amount_inv'])) * 100,2);

						}

						$grand_total += $investments->amount_inv;

						//$current_nav = HistoricNavs::where('scheme_code',$investments->scheme_code)->orderBy('date','aesc')->first();
						//$current_nav = $current_nav['nav'];
						$units_held = $investments->units;

						$current_market_value = $units_held * $current_nav;

						$grand_market_value += $current_market_value;

						if ($Scheme_sub != "liquid") {
							$xirr_market_val += $current_market_value;
						}

						

						$count++;	
					}

						$profit_or_loss = $grand_market_value - $grand_total;
						$grand_profit_or_loss += $profit_or_loss;
						$grand_absolute_returns = ((($grand_market_value -$grand_total)/$grand_total) * 100);

						$grand_total = round($grand_total,2);
						$grand_market_value = round($grand_market_value,2);
						$grand_profit_or_loss = round($grand_profit_or_loss,2);	
						$grand_absolute_returns = round($grand_absolute_returns,2);

				

					if ($count == 0) {
						return response()->json(['msg' => '0']);
					}
					else{

						$export_date = date('Y-m-d',strtotime($change_date));
						$change_date = date('d-m-Y',strtotime($change_date));

						//return response()->json(['msg'=> '1','response'=>$cis_details]);
						$absolute_returns_avg = round(($absolute_returns_avg/$count),2);
						$current_market_value = round($current_market_value,2);
						$profit_or_loss = round($profit_or_loss,2);
						$total_m_value = round($total_m_value,2);

						
						array_push($dateArray, $export_date);
						array_push($amountArray, $xirr_market_val);
						
						$xirrVal = PHPExcel_Calculation_Financial::XIRR($amountArray,$dateArray);
						$xirrVal = $xirrVal * 100;
						$xirrVals = round($xirrVal,2);
						$grand_total = $fmt->format($grand_total);
						$grand_profit_or_loss = $fmt->format($grand_profit_or_loss);
						$grand_market_value = $fmt->format($grand_market_value);
						// dd($inv_pass);

						return response()->json(['msg'=>'1','absolute_returns_avg'=>$grand_absolute_returns,'total_amount_inv'=>$grand_total,'profit_or_loss'=>$grand_profit_or_loss,'total_m_value'=>$grand_market_value,'cis_details'=>$inv_pass,'date'=>$change_date,'export_date'=>$export_date, 'xirr_value'=>$xirrVals]);
						//print_r($inv_pass);
					}
				
				
			}
    }


    public function rename(Request $request){
    	   	$validator = \Validator::make($request->all(),[

				'id' => 'required',
				'rename_type' => 'required',
				'new_name' => 'required',

			]);

			if ($validator->fails()) {
				
				return redirect()->back()->withErrors($validator->errors());
				
			}
			else{

				if ($request['rename_type'] == 'group') {
					$rename = Group::where('user_id',\Auth::user()->id)->where('id',$request['id'])->update(['name'=>$request['new_name']]);
					if ($rename) {
						return response()->json(['msg'=>'1']);
					}
				}

				else if ($request['rename_type'] == 'individual') {
					
					$rename = Person::where('user_id',\Auth::user()->id)->where('id',$request['id'])->update(['name'=>$request['new_name']]);
					if ($rename) {
						return response()->json(['msg'=>'1']);
					}
				}

				else if ($request['rename_type'] == 'subperson') {
					
					$rename = GroupMembers::where('user_id',\Auth::user()->id)->where('id',$request['id'])->update(['member_name'=>$request['new_name']]);
					if ($rename) {
						return response()->json(['msg'=>'1']);
					}
				}
			}
    }

    public function deleteInvestment(Request $request){
    	   	$validator = \Validator::make($request->all(),[

				'inv_id' => 'required',
				'investor_id' => 'required',
				'investor_type' => 'required',

			]);

			if ($validator->fails()) {
				
				return redirect()->back()->withErrors($validator->errors());
				
			}
			else{ 

				$delete_inv = InvestmentDetails::where('user_id',\Auth::user()->id)->where('id',$request['inv_id'])->where('investor_id',$request['investor_id'])->where('investor_type',$request['investor_type'])->delete();

				if ($delete_inv) {
						$investments = InvestmentDetails::where('user_id',\Auth::user()->id)->where('investor_id',$request['investor_id'])
					->where('investor_type',$request['investor_type'])
					->get();

					$count = 0;
					$investment_details = array();
					$current_m_value = 0;
					$profit_or_loss = 0;
					$total_amount = 0;
					$total_m_value = 0;
					$absolute_returns_avg = 0;

					$yesterday_date = Carbon::now()->subDay(1)->toDateString();
					$yesterday_date = date('Y-m-d',strtotime($yesterday_date));

					$total_amount = InvestmentDetails::where('user_id',\Auth::user()->id)->where('investor_id',$request['investor_id'])
					->where('investor_type',$request['investor_type'])
					->sum('amount_inv');

					if (empty($total_amount)) {
						$total_amount = 0;
					}

					if (count($investments) == 0) {
						return response()->json(['msg'=>'1','investment_details'=>$investment_details,'total_amount'=>$total_amount,'total_m_value'=>$total_m_value,'profit_or_loss'=>$profit_or_loss,'absolute_return_avg' => $absolute_returns_avg]);
					}

					else{

						foreach ($investments as $investment) {

							$investment_details[$count]['investor_id'] = $request['investor_id'];
							$investment_details[$count]['investor_type'] = $request['investor_type'];
							$investment_details[$count]['investment_id'] = $investment->id;
							$investment_details[$count]['scheme_name'] = $investment->scheme_name;
							$investment_details[$count]['scheme_type'] = $investment->scheme_type;
							$investment_details[$count]['dop'] = $investment->purchase_date;
							$investment_details[$count]['amount_inv'] = $investment->amount_inv;
							$investment_details[$count]['purchase_nav'] = $investment->purchase_nav;
							$investment_details[$count]['units'] = round($investment->units,4);


							//$current_nav = HistoricNavs::where('scheme_code','106212')->first()->pluck('nav');
							$current_nav = HistoricNavs::where('scheme_code',$investment->scheme_code)->orderBy('date','aesc')->first();
							//echo $current_nav['nav'];
							//echo $investment->dop;
							$investment_details[$count]['current_nav'] = $current_nav['nav'];
							$investment_details[$count]['current_market_value'] = round(($investment->units) * $current_nav['nav'],2);
							$investment_details[$count]['folio_number'] = $investment->folio_number;
							$investment_details[$count]['p_or_loss'] = round((($investment->units) * $current_nav['nav']) - (($investment->units) * ($investment->purchase_nav)),2);
							
							

							$current_m_value = ($investment->units) * $current_nav['nav']; 
							$total_m_value += $current_m_value;
							$profit_or_loss +=  (($investment->units) * $current_nav['nav']) - (($investment->units) * ($investment->purchase_nav));

							$initial_value = ($investment->purchase_nav) * ($investment->units);
							$absolute_returns = (($current_m_value - $initial_value)/$initial_value) * 100;
							//echo $absolute_returns;
							$investment_details[$count]['abs_returns'] = round($absolute_returns,2);

							$absolute_returns_avg += $absolute_returns;
							//echo $absolute_returns;
							//echo $absolute_returns_avg;

							$today = Carbon::now()->subDay(1)->toDateString();
							$today = date('Y-m-d',strtotime($today));
							$purchase_date = $investment->purchase_date;

							$date_diff = date_diff(date_create($purchase_date),date_create($today));
							$date_diff = $date_diff->format("%a");

							if ($date_diff == 0) {
								$annualised_return = 0;
							}else{
								$annualised_return = (($absolute_returns * 365)/$date_diff) ;
							}
							$investment_details[$count]['ann_returns'] = round($annualised_return,2);

							
							$count++;
					}

					//echo ($absolute_returns_avg/$count);

					$absolute_returns_avg = round(($absolute_returns_avg/$count),2);
					$current_m_value = round($current_m_value,2);
					$profit_or_loss = round($profit_or_loss,2);
					$total_m_value = round($total_m_value,2);

					//print_r($total_amount);
					//print_r($current_m_value);
					//print_r($profit_or_loss);

					//return $investment_details;
					//print_r($current_nav);*/
					return response()->json(['msg'=>'1','investment_details'=>$investment_details,'total_amount'=>$total_amount,'total_m_value'=>$total_m_value,'profit_or_loss'=>$profit_or_loss,'absolute_return_avg' => $absolute_returns_avg]);
					}
				}
				else{
					return response()->json(['msg'=>'0']);
				}	

			}
    }


    public function addPan(Request $request){
    	   	$validator = \Validator::make($request->all(),[

				'investor_id' => 'required',
				'investor_type' => 'required',
				'pan' => 'required',

			]);

			if ($validator->fails()) {
				
				return redirect()->back()->withErrors($validator->errors());
				
			}
			else{ 

					$investor_id = $request['investor_id'];
					$investor_type = $request['investor_type'];
					$pan = $request['pan'];

					if ($investor_type == "individual") {
						
						$add_pan = Person::where('id',$investor_id)->update(['person_pan'=>$pan]);

						if ($add_pan) {
							return response()->json(['msg' => "1",'pan'=>$pan]);
						}
						else{
							return response()->json(['msg' => "0"]);
						}

					}
					if ($investor_type == "subperson") {
						$add_pan = GroupMembers::where('id',$investor_id)->update(['member_pan'=>$pan]);

						if ($add_pan) {
							return response()->json(['msg' => "1",'pan'=>$pan]);
						}
						else{
							return response()->json(['msg' => "0"]);
						}

					}
			}
    }



    public function addQuery(Request $request){

    		$validator = \Validator::make($request->all(),[

				'investor_id' => 'required',
				'query_text' => 'required',
				'query_date' => 'required',
				'investor_type' => 'required',


			]);

			if ($validator->fails()) {
				
				return redirect()->back()->withErrors($validator->errors());
				
			}
			else{ 

				$new_query = new Queries();
				$new_query->investor_id = $request['investor_id'];
				$new_query->investor_type = $request['investor_type'];
				$new_query->query = $request['query_text'];

				$date = date('Y-m-d',strtotime($request['query_date']));
				//return $date;
				$new_query->date = $date;
				$new_query->status = 0;

				$save_query  = $new_query->save();

				if ($save_query) {
					$investor_id = $request['investor_id'];
					$queries = Queries::where('investor_id',$investor_id)->where('investor_type',$request['investor_type'])->get();
					return response()->json(['msg'=>'1','query'=>$queries]);
				}


			}

    }

    public function getQuery(Request $request){

    		$validator = \Validator::make($request->all(),[

				'investor_id' => 'required',
				'investor_type' => 'required'

			]);

			if ($validator->fails()) {
				
				return redirect()->back()->withErrors($validator->errors());
				
			}
			else{ 

				$investor_id = $request['investor_id'];
				$queries = Queries::where('investor_id',$investor_id)->where('investor_type',$request['investor_type'])->get();
				return response()->json(['msg'=>'1','query'=>$queries]);

			}

    }

        public function updateQuery(Request $request){

    		$validator = \Validator::make($request->all(),[

				'query_id' => 'required',
				'date' => 'required',
				'status' => 'required',
				'query' => 'required',
				'investor_id' => 'required',

			]);

			if ($validator->fails()) {
				
				return redirect()->back()->withErrors($validator->errors());
				
			}
			else{ 

				$date = date('Y-m-d',strtotime($request['date']));

				if ($request['status'] == "InProcess") {
					$status = 0;
				}

				if ($request['status'] == "Complete") {
					$status = 1;
				}
				if ($request['status'] == "CompleteComplete") {
					$status = 1;
					//return $request;
				}



				$query_id = $request['query_id'];
				$update_query = Queries::where('id',$query_id)->update(['date'=>$date,'status'=>$status,'query'=>$request['query']]);

				$queries = Queries::where('investor_id',$request['investor_id'])->get();

				if ($update_query) {
					return response()->json(['msg'=>'1','query'=>$queries]);
				}
				else{
					return response()->json(['msg'=>'0']);
				}

				

			}

    }

    public function deleteQuery(Request $request){
    	$validator = \Validator::make($request->all(),[

				'investor_id' => 'required',
				'query_id' => 'required'

			]);

			if ($validator->fails()) {
				
				return redirect()->back()->withErrors($validator->errors());
				
			}
			else{ 

				$delete_query = Queries::where('id',$request['query_id'])->delete();

				if ($delete_query) {
					$queries = Queries::where('investor_id',$request['investor_id'])->get();
					return response()->json(['msg'=>'1','query'=>$queries]);
				}
			}
    }

    public function checkPass(Request $request){

    	   $validator = \Validator::make($request->all(),[

				'pass' => 'required',
				

			]);

			if ($validator->fails()) {
				
				return redirect()->back()->withErrors($validator->errors());
				
			}
			else{ 

				$password = $request['pass'];

				$hashed = \Hash::make($password);

				$admin_pass = User::where('email','vasudevgupta@gmail.com')->pluck('password');

				$admin_pass = $admin_pass[0];


				if (\Hash::check($password, $admin_pass))
				{
					    return response()->json(['msg'=>'1']);
				}
				else{
						return response()->json(['msg'=>'0']);
				}
			}

    }


	// public function portfolio()
	// {
	// 	$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );
	// 	$SchemeDetail = SchemeDetails::get()
 //                     ->groupBy('scheme_sub');
 //        $scheme_type = scheme_type::pluck('scheme_sub');
 //        $total_amount_invested = 0;

 //        $type_count = count($scheme_type) - 1;
 //        $scheme_array = [];
 //        for ($i=0; $i <= $type_count ; $i++) { 
 //        	$data_set = [];
 //        	if (isset($SchemeDetail[$scheme_type[$i]])) {
 //        		$total_amount = 0;
 //        		array_push($data_set, $scheme_type[$i]);
 //        		foreach ($SchemeDetail[$scheme_type[$i]] as $value) {
 //        			$invest_amount = InvestmentDetails::where('scheme_code',$value['scheme_code'])
 //        							->sum('units');
 //        			$historic_navs = HistoricNavs::where('scheme_code',$value['scheme_code'])
 //        							->orderBy('date', 'desc')
 //        							->first();
 //        			$sum_amount = $invest_amount * $historic_navs['nav'];
 //        			$total_amount += $sum_amount;
	//           	}
	//           	$total_amount = round($total_amount, 2);
	//           	$total_amount_invested += $total_amount;
	//           	array_push($data_set, $total_amount);
	//           	array_push($scheme_array, $data_set);
 //        	}
 //        }

 //        $final_array = [];
 //        $equity = 0;
 //        $debt = 0;
 //        $balanced = 0;
 //        foreach ($scheme_array as $value) {
 //        	$data_set_array = [];
 //        	$avg = ($value[1]/$total_amount_invested) * 100;
 //        	$avg = round($avg, 2);

 //        	$type = scheme_type::where('scheme_sub',$value[0])->value('scheme_type');
 //        	if ($type == 'equity') {
 //        		$equity += $value[1];
 //        	}else if ($type == 'balanced') {
 //        		$balanced += $value[1];
 //        	}else{
 //        		$debt += $value[1];
 //        	}
 //        	$amount = $fmt->format($value[1]);
 //        	array_push($data_set_array, $value[0], $amount, $avg);
 //        	array_push($final_array, $data_set_array);
 //        }

 //        $equity_avg = round(($equity/$total_amount_invested)*100, 2);
 //        $debt_avg = round(($debt/$total_amount_invested)*100, 2);
 //        $balanced_avg = round(($balanced/$total_amount_invested)*100, 2);

 //        $equity = $fmt->format($equity);
 //        $debt = $fmt->format($debt);
 //        $balanced = $fmt->format($balanced);
 //        $total_amount_invested = $fmt->format($total_amount_invested);
        
	// 	return view('sector',['sector_array' => $final_array, 'equity' => $equity, 'debt' => $debt, 'balanced' => $balanced, 'balanced_avg' => $balanced_avg, 'debt_avg' => $debt_avg, 'equity_avg' => $equity_avg, 'total' => $total_amount_invested]);
	// }

	// public function getAmc()
	// {
	// 	$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );
	// 	$grand_total_inv = 0;
	// 	$grand_total_crt = 0;
	// 	$amcNames = amcnames::pluck('name');
	// 	$schemebyamc = SchemeDetails::get()
	// 					->groupBy('amc_name');
	// 	$nameCount = count($amcNames);
	// 	$final_data_set = [];
	// 	for ($i=0; $i < $nameCount  ; $i++) { 
	// 		$data_set = [];
	// 		if (isset($schemebyamc[$amcNames[$i]])) {
	// 			array_push($data_set, $amcNames[$i]);
	// 			$amount_invs = 0;
	// 			$current_val = 0;
	// 			foreach ($schemebyamc[$amcNames[$i]] as $value) {
	// 				$investments = InvestmentDetails::where('scheme_code',$value['scheme_code'])->get();
 //        			$historic_navs = HistoricNavs::where('scheme_code',$value['scheme_code'])
 //        							->orderBy('date', 'desc')
 //        							->first();

 //        			foreach ($investments as $investment) {

	// 	        			$amount_invs += $investment['amount_inv'];	
	// 	        			$current_val += $investment['units'] * $historic_navs['nav'];	
 //    				}	
	// 			}

	// 			$current_val = round($current_val, 2);
	// 			$grand_total_inv += $amount_invs; 
	// 			$grand_total_crt += $current_val;
	// 			array_push($data_set, $amount_invs);
	// 			array_push($data_set, $current_val);
	// 			array_push($final_data_set, $data_set);
	// 		}
	// 	}


	// 	$final_array = [];
	// 	foreach ($final_data_set as $value) {
 //        	$data_set_array = [];
 //        	$avg = 0;
 //        		$avg = ($value[1]/$grand_total_inv) * 100;
	//         	$avg = round($avg, 2);
     	
 //        	// array_push($data_set_array, $value[0], $value[1], $value[2], $avg);
	// 		$amount = $fmt->format($value[1]);
	// 		$amount2 = $fmt->format($value[2]);
	//         $data_set_array['name'] = $value[0];
	//         $data_set_array['AmountInvet'] = $amount;
	//         $data_set_array['CurrentVal'] = $amount2;
	//         $data_set_array['Avg'] = $avg;
 //        	array_push($final_array, $data_set_array);
 //        }

 //        $total['AmountInvet'] = $fmt->format($grand_total_inv);
 //        $total['CurrentVal'] = $fmt->format($grand_total_crt);
 //        $total['Avg'] = 100;
 //        // dd($final_array);
 //        return view('amc',['amc_arrya' => $final_array, 'grand_total' => $total]);

		
	// }

	// public function amcDetails(Request $request)
	// {
	// 	$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );

	// 	$name = $request['name'];
	// 	$grand_total_inv = 0;
	// 	$grand_total_crt = 0;
	// 	if ($name == 'L'){
	// 		$name = "L&T";
	// 	}
	// 	$schemes = SchemeDetails::where('amc_name',$name)->pluck('scheme_code');
	// 	$count = count($schemes);
	// 	$investment_details = [];
	// 	for ($i=0; $i < $count ; $i++) { 
	// 		$HistoricNav = HistoricNavs::where('scheme_code',$schemes[$i])
 //        							->orderBy('date', 'desc')
 //        							->first();
 //        	$investments = InvestmentDetails::where('scheme_code',$schemes[$i])
 //        							->get();
 //        	foreach ($investments as $investment) {
 //        		$investment_detail = [];
 //        		$pan = "";
 //        		$investor_type = $investment['investor_type'];
	// 			$current_val = $investment['units'] * $HistoricNav['nav'];
	// 			$current_val = round($current_val, 2);
 //        		if ($investor_type == "subperson") {
	// 				$pan = GroupMembers::where('id',$investment['investor_id'])->value('member_pan');
	// 			}else if ($investor_type == "individual") {
	// 				$pan = Person::where('id',$investment['investor_id'])->value('person_pan');
	// 			}

	// 			$grand_total_inv += $investment['amount_inv'];
	// 			$grand_total_crt += $current_val;

	// 			$investment_detail['name'] = $investment['scheme_name'];
	// 			$investment_detail['pan'] = $pan;
	// 			$investment_detail['invest_amount'] = $fmt->format($investment['amount_inv']);
	// 			$investment_detail['current_val'] = $fmt->format($current_val);
	// 			$investment_detail['date'] = $investment['purchase_date'];
	// 			array_push($investment_details, $investment_detail);
 //        	}

	// 	}
	// 	$grand_total_inv = $fmt->format($grand_total_inv);
	// 	$grand_total_crt = $fmt->format($grand_total_crt);
	// 	return response()->json(['msg'=>'1', 'investments'=>$investment_details, 'investamt'=>$grand_total_inv, 'CurrentVal'=>$grand_total_crt]);
	// }

	// public function portfolioBygroup($id)
	// {
	// 	$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );
	// 	$total_amount_invested = 0;

	// 	$SchemeDetail = SchemeDetails::get()
 //                     ->groupBy('scheme_sub');
 //        $scheme_type = scheme_type::pluck('scheme_sub');
	// 	$group_id = $id;
	// 	$group_mem_id = GroupMembers::where('user_id',\Auth::user()->id)->where('group_id',$group_id)->pluck('id')->toArray();
	// 	$change_date = Carbon::now();
	// 	$change_date = date('Y-m-d',strtotime($change_date));
	// 	$type_count = count($scheme_type) - 1;
 //        $scheme_array = [];
 //        for ($i=0; $i <= $type_count ; $i++) { 
 //        	$data_set = [];
 //        	if (isset($SchemeDetail[$scheme_type[$i]])) {
 //        		$total_amount = 0;
 //        		array_push($data_set, $scheme_type[$i]);
 //        		foreach ($SchemeDetail[$scheme_type[$i]] as $value) {
 //        			$invest_amount = InvestmentDetails::where('user_id',\Auth::user()->id)->whereIn('investor_id',$group_mem_id)
	// 									->where('investor_type','subperson')
	// 									->where('purchase_date','<=',$change_date)
	// 									->where('scheme_code',$value['scheme_code'])
	// 									->sum('units');
 //        			$historic_navs = HistoricNavs::where('scheme_code',$value['scheme_code'])
 //        							->orderBy('date', 'desc')
 //        							->first();
 //        			$sum_amount = $invest_amount * $historic_navs['nav'];
 //        			$total_amount += $sum_amount;
	//           	}
	//           	$total_amount = round($total_amount, 2);
	//           	$total_amount_invested += $total_amount;
	//           	array_push($data_set, $total_amount);
	//           	array_push($scheme_array, $data_set);
 //        	}
 //        }

	// 	$final_array = [];
 //        $equity = 0;
 //        $debt = 0;
 //        $balanced = 0;
 //        foreach ($scheme_array as $value) {
 //        	$data_set_array = [];
 //        	$avg = 0;
 //    		$avg = ($value[1]/$total_amount_invested) * 100;
 //        	$avg = round($avg, 2);

 //        	$type = scheme_type::where('scheme_sub',$value[0])->value('scheme_type');
 //        	if ($type == 'equity') {
 //        		$equity += $value[1];
 //        	}else if ($type == 'balanced') {
 //        		$balanced += $value[1];
 //        	}else{
 //        		$debt += $value[1];
 //        	}
	// 		$amount = $fmt->format($value[1]);
 //        	array_push($data_set_array, $value[0], $amount, $avg);
 //        	array_push($final_array, $data_set_array);
 //        }

 //        $equity_avg = round(($equity/$total_amount_invested)*100, 2);
 //        $debt_avg = round(($debt/$total_amount_invested)*100, 2);
 //        $balanced_avg = round(($balanced/$total_amount_invested)*100, 2);

 //        $equity = $fmt->format($equity);
 //        $debt = $fmt->format($debt);
 //        $balanced = $fmt->format($balanced);
 //        $total_amount_invested = $fmt->format($total_amount_invested);

 //        return view('sector',['sector_array' => $final_array, 'equity' => $equity, 'debt' => $debt, 'balanced' => $balanced, 'balanced_avg' => $balanced_avg, 'debt_avg' => $debt_avg, 'equity_avg' => $equity_avg, 'total' => $total_amount_invested]);
	// }

	// public function portfolioByperson($id)
	// {
	// 	$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );
	// 	$total_amount_invested = 0;

	// 	$SchemeDetail = SchemeDetails::get()
 //                     ->groupBy('scheme_sub');
 //        $scheme_type = scheme_type::pluck('scheme_sub');
 //        $change_date = Carbon::now();
	// 	$change_date = date('Y-m-d',strtotime($change_date));
	// 	$type_count = count($scheme_type) - 1;
 //        $scheme_array = [];
 //        for ($i=0; $i <= $type_count ; $i++) { 
 //        	$data_set = [];
 //        	if (isset($SchemeDetail[$scheme_type[$i]])) {
 //        		$total_amount = 0;
 //        		array_push($data_set, $scheme_type[$i]);
 //        		foreach ($SchemeDetail[$scheme_type[$i]] as $value) {
 //        			$invest_amount = InvestmentDetails::where('investor_id',$id)
	// 									->where('purchase_date','<=',$change_date)
	// 									->where('scheme_code',$value['scheme_code'])
	// 									->sum('units');
 //        			$historic_navs = HistoricNavs::where('scheme_code',$value['scheme_code'])
 //        							->orderBy('date', 'desc')
 //        							->first();
 //        			$sum_amount = $invest_amount * $historic_navs['nav'];
 //        			$total_amount += $sum_amount;
	//           	}
	//           	$total_amount = round($total_amount, 2);
	//           	$total_amount_invested += $total_amount;
	//           	array_push($data_set, $total_amount);
	//           	array_push($scheme_array, $data_set);
 //        	}
 //        }

	// 	$final_array = [];
 //        $equity = 0;
 //        $debt = 0;
 //        $balanced = 0;
 //        foreach ($scheme_array as $value) {
 //        	$data_set_array = [];
 //        	$avg = 0;
 //    		$avg = ($value[1]/$total_amount_invested) * 100;
 //        	$avg = round($avg, 2);
 //        	$type = scheme_type::where('scheme_sub',$value[0])->value('scheme_type');
 //        	if ($type == 'equity') {
 //        		$equity += $value[1];
 //        	}else if ($type == 'balanced') {
 //        		$balanced += $value[1];
 //        	}else{
 //        		$debt += $value[1];
 //        	}
        	
 //        	$amount = $fmt->format($value[1]);
 //        	array_push($data_set_array, $value[0], $amount, $avg);
 //        	array_push($final_array, $data_set_array);
 //        }



 //        $equity_avg = round(($equity/$total_amount_invested)*100, 2);
 //        $debt_avg = round(($debt/$total_amount_invested)*100, 2);
 //        $balanced_avg = round(($balanced/$total_amount_invested)*100, 2);

 //        $equity = $fmt->format($equity);
 //        $debt = $fmt->format($debt);
 //        $balanced = $fmt->format($balanced);
 //        $total_amount_invested = $fmt->format($total_amount_invested);


 //        return view('sector',['sector_array' => $final_array, 'equity' => $equity, 'debt' => $debt, 'balanced' => $balanced, 'balanced_avg' => $balanced_avg, 'debt_avg' => $debt_avg, 'equity_avg' => $equity_avg, 'total' => $total_amount_invested]);
	// }

	public function cispdf()
	{
   //  	   	$validator = \Validator::make($request->all(),[
   //  	   		'investor_id' => 'required',
   //  	   		'investor_type' => 'required',
   //  	   		'ex_nav_date' => 'required',
			// ]);
   	    	$cis = $_SERVER['DOCUMENT_ROOT'].'/cis.zip';
   	    	if ($cis == true) {
				unlink($cis);
   	    	}
			set_time_limit(600);
			$group_id = 2;
			$group_mem_id = array();
			$group_mem_id = GroupMembers::where('user_id',\Auth::user()->id)->where('group_id',$group_id)->pluck('id')->toArray();

				$count = 0;
				$cis_details = array();
				$yesterday_date = Carbon::now()->subDay(1)->toDateString();
				$yesterday_date = date('Y-m-d',strtotime($yesterday_date));
				$investment_detail = array();
				$total_m_value = 0;
				$profit_or_loss = 0;
				$absolute_returns_avg = 0;
				$total_amount_inv =0;
				$scheme_inv_total = 0;
				$current_market_value = 0;

				$grand_total = 0;
				$grand_market_value = 0;
				$grand_profit_or_loss = 0;
				$grand_absolute_returns = 0;
				$units = 0;

				$unique_scheme = array();

				$check_them = array();
				$investor_ids = array();
				$investor_id = array();
				$separate_inv = array();

				$inv_pass = array();

				$change_date = $yesterday_date;
					foreach ($group_mem_id as $member_id) {
						$investor_id[] = $member_id;
					}

				$investment_detail = InvestmentDetails::where('user_id',\Auth::user()->id)->whereIn('investor_id',$investor_id)->where('investor_type','subperson')->where('purchase_date','<=',$change_date)->get();



				foreach ($investment_detail as $sep_inv) {
					

						$investor_name = GroupMembers::where('id',$sep_inv->investor_id)->pluck('member_name');

						$investor_name = $investor_name[0];

						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Investor Name'] = $investor_name ;

						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Scheme Name'] = $sep_inv->scheme_name; 
						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Amount Invested'] = $sep_inv->amount_inv; 
						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Purchase date'] = date('d-M-Y',strtotime($sep_inv->purchase_date));

						$current_nav = HistoricNavs::where('scheme_code',$sep_inv->scheme_code)->where('date','<=' ,$change_date)->orderBy('date','aesc')->first();

						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Purchase NAV'] = $sep_inv->purchase_nav;
						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Units'] = $sep_inv->units;

						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current NAV'] = $current_nav['nav'];

						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'] = round($current_nav['nav'] * $sep_inv->units,2);

						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Unrealised Profit/Loss'] = round(($current_nav['nav'] * $sep_inv->units) - ($sep_inv->amount_inv),2);

						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Absolute Returns'] = round((($separate_inv[$sep_inv->investor_id][$sep_inv->id]['Unrealised Profit/Loss'])/($sep_inv->amount_inv)) * 100,2);

						$today = Carbon::now()->subDays(1)->toDateString();

						$date_diff = date_diff(date_create($separate_inv[$sep_inv->investor_id][$sep_inv->id]['Purchase date']),date_create($today));
						$date_diff = $date_diff->format("%a");

						if ($date_diff == 0) {
							$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Annualised Returns'] = 0;
						}else{
							$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Annualised Returns'] = round((($separate_inv[$sep_inv->investor_id][$sep_inv->id]['Absolute Returns'])*(365/$date_diff)),2);
						}

						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Folio Number'] = $sep_inv->folio_number;      



				}



					foreach ($investment_detail as $investments) {
						
						if (array_key_exists($investments->scheme_code, $inv_pass)) {
							
							
							$inv_pass[$investments->scheme_code]['Scheme Name'] = $investments->scheme_name;

							$inv_pass[$investments->scheme_code]['Amount Invested'] += $investments->amount_inv;


							$current_nav = HistoricNavs::where('scheme_code',$investments->scheme_code)->where('date','<=' ,$change_date)->orderBy('date','aesc')->first();


							$inv_pass[$investments->scheme_code]['Units'] += round(($investments->units),4);
							//$inv_pass[$investments->scheme_code]['units'] = $units;
							$inv_pass[$investments->scheme_code]['Current NAV'] = $current_nav['nav'];

										
							$current_market_value = (($inv_pass[$investments->scheme_code]['Units']) * $current_nav['nav']);

							$current_market_value = round($current_market_value,2);
							$inv_pass[$investments->scheme_code]['Current Market Value'] = $current_market_value;

							$inv_pass[$investments->scheme_code]['Unrealised Profit/Loss'] = (($inv_pass[$investments->scheme_code]['Current Market Value']) - ($inv_pass[$investments->scheme_code]['Amount Invested']));

							$inv_pass[$investments->scheme_code]['Absolute Returns (%)'] = round(((($inv_pass[$investments->scheme_code]['Current Market Value']) - $inv_pass[$investments->scheme_code]['Amount Invested'])/($inv_pass[$investments->scheme_code]['Amount Invested'])) * 100,2);

							
						}
						else{

							$inv_pass[$investments->scheme_code]['Scheme Name'] = $investments->scheme_name;

							$inv_pass[$investments->scheme_code]['Amount Invested'] = $investments->amount_inv;
							
							$current_nav = HistoricNavs::where('scheme_code',$investments->scheme_code)->where('date','<=' ,$change_date)->orderBy('date','aesc')->first();
							$inv_pass[$investments->scheme_code]['Current NAV'] = $current_nav['nav'];

							$inv_pass[$investments->scheme_code]['Units'] = round(($investments->units),4);
										
							$current_market_value = (($investments->units) * $current_nav['nav']);

							$current_market_value = round($current_market_value,2);

							$inv_pass[$investments->scheme_code]['Current Market Value'] = $current_market_value;

							$inv_pass[$investments->scheme_code]['Unrealised Profit/Loss'] = (($inv_pass[$investments->scheme_code]['Current Market Value']) - ($inv_pass[$investments->scheme_code]['Amount Invested']));

							$inv_pass[$investments->scheme_code]['Absolute Returns (%)'] = round(((($inv_pass[$investments->scheme_code]['Current Market Value']) - $inv_pass[$investments->scheme_code]['Amount Invested'])/($inv_pass[$investments->scheme_code]['Amount Invested'])) * 100,2);

							

						}

						$grand_total += $investments->amount_inv;

						$current_nav = HistoricNavs::where('scheme_code',$investments->scheme_code)->orderBy('date','aesc')->first();
						$current_nav = $current_nav['nav'];
						$units_held = $investments->units;

						$current_market_value = $units_held * $current_nav;

						$grand_market_value += $current_market_value;

						

						$count++;	
					}

							$final_array = array();

							$count = 0;
							$pro_array =  array();
							$member_name = '';
							$names = [];
			               foreach ($separate_inv as $inv) {

			               		foreach($inv as $in){
									$member_name = $in['Investor Name'];
			               		}
							$pdf = PDF::loadView('cispdf', ['inv' => $inv])->setPaper('a4', 'landscape')->setWarnings(false)->save($member_name.'.pdf');
							// return $pdf->download($member_name.'.pdf');
							// array_push($final_array, $pdf);
							// array_push($names, $member_name);

							}

				$zip = new ZipArchive;
				if ($zip->open('cis.zip', ZipArchive::CREATE) === TRUE)
					{
					   foreach ($names as $value) {
						   	$zip->addFile($value.'.pdf');
					   } 
					    
					    $zip->close();
					   foreach ($names as $value) {
				   	    	$path = $_SERVER['DOCUMENT_ROOT'].'/'.$value.'.pdf';
							unlink($path);
					   }
					}
				 $file= public_path(). "/cis.zip";
				     $headers = array(
				              'Content-Type: application/zip',
			            );
					return response()->download($file, 'cis.zip', $headers);
	}

	public function indexupdate()
	{

		// $nifty = file('http://www.nseindia.com/homepage/Indices1.json')

		$client = new \GuzzleHttp\Client();
		$res = $client->get('https://www.nseindia.com/live_market/dynaContent/live_watch/stock_watch/niftyStockWatch.json');
		$data = json_decode($res->getBody(), true); 
		// dd($data);
		$niftydate = date('Y-m-d', strtotime($data['time']));
		$latest_data = $data['latestData'];
		$open_nifty = str_replace(',', '', $latest_data[0]['open']);
		$high_nifty = str_replace(',', '', $latest_data[0]['high']);
		$low_nifty = str_replace(',', '', $latest_data[0]['low']);
		$close_hifty = str_replace(',', '', $latest_data[0]['ltp']);
		// dd($open_nifty);
		$save = historic_nse::insert(['date' => $niftydate, 
									     'opening_index' => $open_nifty,
									     'high' => $high_nifty,
									     'low' => $low_nifty,
									     'closing_index' => $close_hifty
									]);


		$sensex = file('http://www.bseindia.com/SensexView/SensexViewbackPage.aspx?flag=INDEX&indexcode=16');

		foreach ($sensex as $value) {

				$array = explode("@",$value);
				$date_split = explode("|", $array[9]);
				$mydate = date('Y-m-d', strtotime($date_split[0]));
				// dd($array)
				$save = historic_bse::insert(['date' => $mydate, 
											     'opening_index' => $array[2],
											     'high' => $array[3],
											     'low' => $array[4],
											     'closing_index' => $array[5]
											]);
		}



    	$smallcap_data = file('http://www.bseindia.com/SensexView/SensexViewbackPage.aspx?flag=INDEX&indexcode=82');

		foreach ($smallcap_data as $value) {

				$array = explode("@",$value);
				$date_split = explode("|", $array[9]);
				$mydate = date('Y-m-d', strtotime($date_split[0]));
				$save = smallcap::insert(['date' => $mydate, 
											     'open' => $array[2],
											     'high' => $array[3],
											     'low' => $array[4],
											     'close' => $array[5]
											]);


		}

		$midcap_data = file('http://www.bseindia.com/SensexView/SensexViewbackPage.aspx?flag=INDEX&indexcode=81');

		foreach ($midcap_data as $value) {

				$array = explode("@",$value);
				$date_split = explode("|", $array[9]);
				$mydate = date('Y-m-d', strtotime($date_split[0]));
				$save = midcap::insert(['date' => $mydate, 
											     'open' => $array[2],
											     'high' => $array[3],
											     'low' => $array[4],
											     'close' => $array[5]
											]);


		}
		dd('sucessfuly update!!!!!');
	}


	// public function benchmark($id)
	// {
	// 	$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );

	// 	$esensex_total = 0;
	// 	$nifty_total = 0;

	// 	$compound = 0;
	// 	$rbi_current = 0;

	// 	$etotal_current = 0;
	// 	$etotal_inv = 0;

	// 	$dtotal_current = 0;
	// 	$dtotal_inv = 0;

	// 	$btotal_current = 0;
	// 	$btotal_inv = 0;

	// 	$equity = [];
	// 	$debt = [];
	// 	$balanced = [];

	// 	$today = Carbon::now();
	// 	$dateOrder = date('Y-m-d',strtotime($today));
	// 	$investments = InvestmentDetails::where('investor_id',$id)
	// 			->where('investor_type','individual')
	// 			->get();
	// 	$investor_name = Person::where('id',$id)->value('name');


	// 	foreach ($investments as $investment) {
	// 		$data_set = [];
	// 		$Scheme_type = SchemeDetails::where('scheme_code',$investment['scheme_code'])->value('scheme_type');
	// 		$historic_navs = HistoricNavs::where('scheme_code',$investment['scheme_code'])->orderBy('date','aesc')->first();
	// 		if ($Scheme_type == 'equity') {
	// 			$etotal_current += $historic_navs['nav'] * $investment['units'];

	// 			$inv_date = $investment['purchase_date'];
	// 			$inv_amt = $investment['amount_inv'];
	// 			$etotal_inv += $inv_amt;

	// 			$get_sensex = historic_bse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
	// 			$buy_sensex = $inv_amt/$get_sensex['closing_index'];
	// 			$current_sensex = historic_bse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
	// 			$current_sensex_val = $buy_sensex * $current_sensex['closing_index'];
	// 			$current_sensex_val = round($current_sensex_val, 2);
	// 			$esensex_total += $current_sensex_val;

	// 			$get_nifty = historic_nse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
	// 			$buy_nifty = $inv_amt/$get_nifty['closing_index'];
	// 			$current_nifty = historic_nse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
	// 			$current_nifty_val = $buy_nifty * $current_nifty['closing_index'];
	// 			$current_nifty_val = round($current_nifty_val, 2);
	// 			$nifty_total += $current_nifty_val;

	// 			$data_set['name'] = $investor_name;
	// 			$data_set['dop'] = $inv_date;
	// 			$data_set['inv_amt'] = $fmt->format($inv_amt);
	// 			$data_set['if_sensex'] = $fmt->format($current_sensex_val);
	// 			$data_set['if_nifty'] = $fmt->format($current_nifty_val);
	// 			// dd($data_set);
	// 			array_push($equity, $data_set);

	// 		}elseif ($Scheme_type == 'debt') {

	// 			$rbi = $this->rbi_fd;
	// 			// $rbi = array_reverse($rbi);
	// 			$dtotal_current += $historic_navs['nav'] * $investment['units'];
	// 			// dd($rbi);
	// 			$inv_date = $investment['purchase_date'];
	// 			$inv_amt = $investment['amount_inv'];
	// 			$dtotal_inv += $inv_amt;
	// 			$rate = 0;
	// 			foreach ($rbi as $value) {
	// 				if ($inv_date <= $value['date']) {
	// 					break;
	// 				}
	// 				$rate = $value['value'];	
	// 			}

	// 			$rbi_set = ($rate/100) * $inv_amt;

	// 			$to = Carbon::createFromFormat('Y-m-d', $dateOrder);
	// 			$from = Carbon::createFromFormat('Y-m-d', $inv_date);
	// 			$diff_in_days = $to->diffInDays($from);
	// 			$rbi_val = (($rbi_set)/365) * $diff_in_days;
	// 			$rbi_current += $rbi_val + $inv_amt;

	// 			$data_set['name'] = $investor_name;
	// 			$data_set['dop'] = $inv_date;
	// 			$data_set['inv_amt'] = $fmt->format($inv_amt);
	// 			$data_set['rbi_amt'] = $fmt->format(round(($rbi_val + $inv_amt), 2));

	// 			array_push($debt, $data_set);

	// 		}else{
	// 			$btotal_current += $historic_navs['nav'] * $investment['units'];

	// 			$inv_date = $investment['purchase_date'];
	// 			$inv_amt = $investment['amount_inv'];
	// 			$btotal_inv += $inv_amt;

	// 			$get_sensex = historic_bse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
	// 			$buy_sensex = $inv_amt/$get_sensex['closing_index'];
	// 			$current_sensex = historic_bse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
	// 			$current_sensex_val = $buy_sensex * $current_sensex['closing_index'];
	// 			$current_sensex_val = round($current_sensex_val, 2);
	// 			$current_sensex_val = (60/100) * $current_sensex_val;

	// 			$rbi = $this->rbi_fd;
	// 			$rate = 0;
	// 			foreach ($rbi as $value) {
	// 				if ($inv_date <= $value['date']) {
	// 					break;
	// 				}
	// 				$rate = $value['value'];	
	// 			}

	// 			$rbi_set = ($rate/100) * $inv_amt;

	// 			$to = Carbon::createFromFormat('Y-m-d', $dateOrder);
	// 			$from = Carbon::createFromFormat('Y-m-d', $inv_date);
	// 			$diff_in_days = $to->diffInDays($from);
	// 			$rbi_val = (($rbi_set)/365) * $diff_in_days;
	// 			$rbi_val = $rbi_val + $inv_amt;
	// 			$rbi_val = (40/100) * $rbi_val;

	// 			$data_set['name'] = $investor_name;
	// 			$data_set['dop'] = $inv_date;
	// 			$data_set['inv_amt'] = $fmt->format($inv_amt);
	// 			$data_set['compound'] = $fmt->format(round(($rbi_val + $current_sensex_val), 2));

	// 			$compound += $current_sensex_val + $rbi_val;

	// 			array_push($balanced, $data_set);

	// 		}
	// 	}
	// 	// dd($debt);

	// 	$etotal_current = round($etotal_current, 2);
	// 	$dtotal_current = round($dtotal_current, 2);
	// 	$btotal_current = round($btotal_current, 2);
	// 	$sensex_diff = $esensex_total - $etotal_current;
	// 	$nifty_diff = $nifty_total - $etotal_current;
	// 	$rbi_deff = $rbi_current - $dtotal_current;
	// 	$compound_deff = $compound - $btotal_current;
	// 	$sensex_diff = round($sensex_diff, 2);
	// 	$nifty_diff = round($nifty_diff, 2);
	// 	$compound = round($compound, 2);
	// 	$rbi_current = round($rbi_current, 2);
	// 	$rbi_deff = round($rbi_deff, 2);
	// 	$compound_deff = round($compound_deff, 2);

	// 	// dd($etotal_current,$dtotal_current,$btotal_current);
	// 	$etotal_current = $fmt->format($etotal_current);
	// 	$dtotal_current = $fmt->format($dtotal_current);
	// 	$btotal_current = $fmt->format($btotal_current);
	// 	$esensex_total = $fmt->format($esensex_total);
	// 	$nifty_total = $fmt->format($nifty_total);
	// 	$etotal_inv = $fmt->format($etotal_inv);
	// 	$dtotal_inv = $fmt->format($dtotal_inv);
	// 	$btotal_inv = $fmt->format($btotal_inv);
	// 	$rbi_current = $fmt->format($rbi_current);
	// 	$compound = $fmt->format($compound);

	// 	return view('benchmark',['RFtotal_equity' => $etotal_current, 'RFtotal_debt' => $dtotal_current,'RFtotal_balanced' => $btotal_current, 'sensex' => $esensex_total, 'nifty' => $nifty_total, 'equity' => $equity, 'debt' => $debt,'balanced' => $balanced, 'sensex_diff' => $sensex_diff, 'nifty_diff' => $nifty_diff, 'invest_amount_equity' => $etotal_inv, 'invest_amount_debt' => $dtotal_inv, 'invest_amount_balanced' => $btotal_inv, 'rbi_current' => $rbi_current, 'compound' => $compound, 'rbi_deff' => $rbi_deff, 'compound_deff' => $compound_deff]);
	// }

	// public function bmGroup($id)
	// {
	// 	$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );

	// 	$esensex_total = 0;
	// 	$nifty_total = 0;

	// 	$bsensex_total = 0;

	// 	$etotal_current = 0;
	// 	$etotal_inv = 0;

	// 	$dtotal_current = 0;
	// 	$dtotal_inv = 0;

	// 	$btotal_current = 0;
	// 	$btotal_inv = 0;

	// 	$compound = 0;
	// 	$rbi_current = 0;


	// 	$equity = [];
	// 	$debt = [];
	// 	$balanced = [];

	// 	$today = Carbon::now();
	// 	$dateOrder = date('Y-m-d',strtotime($today));
	// 	$investments = InvestmentDetails::where('investor_id',$id)
	// 			->where('investor_type','subperson')
	// 			->get();
	// 	$investor_name = GroupMembers::where('id',$id)->value('member_name');


	// 	foreach ($investments as $investment) {
	// 		$data_set = [];
	// 		$Scheme_type = SchemeDetails::where('scheme_code',$investment['scheme_code'])->value('scheme_type');
	// 		$historic_navs = HistoricNavs::where('scheme_code',$investment['scheme_code'])->orderBy('date','aesc')->first();
	// 		if ($Scheme_type == 'equity') {
	// 			$etotal_current += $historic_navs['nav'] * $investment['units'];

	// 			$inv_date = $investment['purchase_date'];
	// 			$inv_amt = $investment['amount_inv'];
	// 			$etotal_inv += $inv_amt;

	// 			$get_sensex = historic_bse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
	// 			$buy_sensex = $inv_amt/$get_sensex['closing_index'];
	// 			$current_sensex = historic_bse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
	// 			$current_sensex_val = $buy_sensex * $current_sensex['closing_index'];
	// 			$current_sensex_val = round($current_sensex_val, 2);
	// 			$esensex_total += $current_sensex_val;

	// 			$get_nifty = historic_nse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
	// 			$buy_nifty = $inv_amt/$get_nifty['closing_index'];
	// 			$current_nifty = historic_nse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
	// 			$current_nifty_val = $buy_nifty * $current_nifty['closing_index'];
	// 			$current_nifty_val = round($current_nifty_val, 2);
	// 			$nifty_total += $current_nifty_val;

	// 			$data_set['name'] = $investor_name;
	// 			$data_set['dop'] = $inv_date;
	// 			$data_set['inv_amt'] = $fmt->format($inv_amt);
	// 			$data_set['if_sensex'] = $fmt->format($current_sensex_val);
	// 			$data_set['if_nifty'] = $fmt->format($current_nifty_val);
	// 			// dd($data_set);
	// 			array_push($equity, $data_set);

	// 		}elseif ($Scheme_type == 'debt') {

	// 			$rbi = $this->rbi_fd;
	// 			$rbi = array_reverse($rbi);
	// 			$dtotal_current += $historic_navs['nav'] * $investment['units'];

	// 			$inv_date = $investment['purchase_date'];
	// 			$inv_amt = $investment['amount_inv'];
	// 			$dtotal_inv += $inv_amt;

	// 			$rbi_set = 0;
	// 			foreach ($rbi as $value) {
	// 				if ($inv_date <= $value['date']) {
	// 					$temp = $value['value'];
	// 					$rbi_set = ($temp/100) * $inv_amt;
	// 					break;
	// 				}
	// 			}


	// 			$to = Carbon::createFromFormat('Y-m-d', $dateOrder);
	// 			$from = Carbon::createFromFormat('Y-m-d', $inv_date);
	// 			$diff_in_days = $to->diffInDays($from);
	// 			$rbi_val = (($rbi_set)/365) * $diff_in_days;
	// 			$rbi_current += $rbi_val + $inv_amt;

	// 			$data_set['name'] = $investor_name;
	// 			$data_set['dop'] = $inv_date;
	// 			$data_set['inv_amt'] = $fmt->format($inv_amt);
	// 			$data_set['rbi_amt'] = $fmt->format(round(($rbi_val + $inv_amt), 2));

	// 			array_push($debt, $data_set);

	// 		}else{
	// 			$btotal_current += $historic_navs['nav'] * $investment['units'];

	// 			$inv_date = $investment['purchase_date'];
	// 			$inv_amt = $investment['amount_inv'];
	// 			$btotal_inv += $inv_amt;

	// 			$get_sensex = historic_bse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
	// 			$buy_sensex = $inv_amt/$get_sensex['closing_index'];
	// 			$current_sensex = historic_bse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
	// 			$current_sensex_val = $buy_sensex * $current_sensex['closing_index'];
	// 			$current_sensex_val = round($current_sensex_val, 2);
	// 			$bsensex_total += $current_sensex_val;
	// 			$current_sensex_val = (60/100) * $current_sensex_val;

	// 			$rbi = $this->rbi_fd;
	// 			$rate = 0;
	// 			foreach ($rbi as $value) {
	// 				if ($inv_date <= $value['date']) {
	// 					break;
	// 				}
	// 				$rate = $value['value'];	
	// 			}

	// 			$rbi_set = ($rate/100) * $inv_amt;

	// 			$to = Carbon::createFromFormat('Y-m-d', $dateOrder);
	// 			$from = Carbon::createFromFormat('Y-m-d', $inv_date);
	// 			$diff_in_days = $to->diffInDays($from);
	// 			$rbi_val = (($rbi_set)/365) * $diff_in_days;
	// 			$rbi_val = $rbi_val + $inv_amt;
	// 			$rbi_val = (40/100) * $rbi_val;

	// 			$data_set['name'] = $investor_name;
	// 			$data_set['dop'] = $inv_date;
	// 			$data_set['inv_amt'] = $fmt->format($inv_amt);
	// 			$data_set['compound'] = $fmt->format(round(($rbi_val + $current_sensex_val), 2));

	// 			$compound += $current_sensex_val + $rbi_val;

	// 			array_push($balanced, $data_set);

	// 		}
	// 	}

	// 	$etotal_current = round($etotal_current, 2);
	// 	$dtotal_current = round($dtotal_current, 2);
	// 	$btotal_current = round($btotal_current, 2);
	// 	$sensex_diff = $esensex_total - $etotal_current;
	// 	$nifty_diff = $nifty_total - $etotal_current;
	// 	$rbi_deff = $rbi_current - $dtotal_current;
	// 	$compound_deff = $compound - $btotal_current;
	// 	$sensex_diff = round($sensex_diff, 2);
	// 	$nifty_diff = round($nifty_diff, 2);
	// 	$compound = round($compound, 2);
	// 	$rbi_current = round($rbi_current, 2);
	// 	$rbi_deff = round($rbi_deff, 2);
	// 	$compound_deff = round($compound_deff, 2);

	// 	$etotal_current = $fmt->format($etotal_current);
	// 	$dtotal_current = $fmt->format($dtotal_current);
	// 	$btotal_current = $fmt->format($btotal_current);
	// 	$esensex_total = $fmt->format($esensex_total);
	// 	$nifty_total = $fmt->format($nifty_total);
	// 	$etotal_inv = $fmt->format($etotal_inv);
	// 	$dtotal_inv = $fmt->format($dtotal_inv);
	// 	$btotal_inv = $fmt->format($btotal_inv);
	// 	$rbi_current = $fmt->format($rbi_current);
	// 	$compound = $fmt->format($compound);

	// 	return view('benchmark',['RFtotal_equity' => $etotal_current, 'RFtotal_debt' => $dtotal_current,'RFtotal_balanced' => $btotal_current, 'sensex' => $esensex_total, 'nifty' => $nifty_total, 'equity' => $equity, 'debt' => $debt,'balanced' => $balanced, 'sensex_diff' => $sensex_diff, 'nifty_diff' => $nifty_diff, 'invest_amount_equity' => $etotal_inv, 'invest_amount_debt' => $dtotal_inv, 'invest_amount_balanced' => $btotal_inv, 'rbi_current' => $rbi_current, 'compound' => $compound, 'rbi_deff' => $rbi_deff, 'compound_deff' => $compound_deff]);
	// }

	// public function bmCIS($id)
	// {
	// 	$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );

	// 	$esensex_total = 0;
	// 	$nifty_total = 0;

	// 	$bsensex_total = 0;

	// 	$etotal_current = 0;
	// 	$etotal_inv = 0;

	// 	$dtotal_current = 0;
	// 	$dtotal_inv = 0;

	// 	$btotal_current = 0;
	// 	$btotal_inv = 0;

	// 	$compound = 0;
	// 	$rbi_current = 0;

	// 	$equity = [];
	// 	$debt = [];
	// 	$balanced = [];

	// 	$today = Carbon::now();
	// 	$dateOrder = date('Y-m-d',strtotime($today));

	// 	$group_mem_id = GroupMembers::where('user_id',\Auth::user()->id)->where('group_id',$id)->pluck('id')->toArray();

	// 	$investment_detail = InvestmentDetails::where('user_id',\Auth::user()->id)->whereIn('investor_id',$group_mem_id)
	// 					->where('investor_type','subperson')
	// 					->get();

	// 	foreach ($investment_detail as $investment) {
	// 		$investor_name = GroupMembers::where('id',$investment['investor_id'])->value('member_name');
	// 		$data_set = [];
	// 		$Scheme_type = SchemeDetails::where('scheme_code',$investment['scheme_code'])->value('scheme_type');
	// 		$historic_navs = HistoricNavs::where('scheme_code',$investment['scheme_code'])->orderBy('date','aesc')->first();
	// 		if ($Scheme_type == 'equity') {
	// 			$etotal_current += $historic_navs['nav'] * $investment['units'];

	// 			$inv_date = $investment['purchase_date'];
	// 			$inv_amt = $investment['amount_inv'];
	// 			$etotal_inv += $inv_amt;

	// 			$get_sensex = historic_bse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
	// 			$buy_sensex = $inv_amt/$get_sensex['closing_index'];
	// 			$current_sensex = historic_bse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
	// 			$current_sensex_val = $buy_sensex * $current_sensex['closing_index'];
	// 			$current_sensex_val = round($current_sensex_val, 2);
	// 			$esensex_total += $current_sensex_val;

	// 			$get_nifty = historic_nse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
	// 			$buy_nifty = $inv_amt/$get_nifty['closing_index'];
	// 			$current_nifty = historic_nse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
	// 			$current_nifty_val = $buy_nifty * $current_nifty['closing_index'];
	// 			$current_nifty_val = round($current_nifty_val, 2);
	// 			$nifty_total += $current_nifty_val;

	// 			$data_set['name'] = $investor_name;
	// 			$data_set['dop'] = $inv_date;
	// 			$data_set['inv_amt'] = $fmt->format($inv_amt);
	// 			$data_set['if_sensex'] = $fmt->format($current_sensex_val);
	// 			$data_set['if_nifty'] = $fmt->format($current_nifty_val);
	// 			// dd($data_set);
	// 			array_push($equity, $data_set);

	// 		}elseif ($Scheme_type == 'debt') {
				
	// 			$rbi = $this->rbi_fd;
	// 			// $rbi = array_reverse($rbi);
	// 			$dtotal_current += $historic_navs['nav'] * $investment['units'];
	// 			// dd($rbi);
	// 			$inv_date = $investment['purchase_date'];
	// 			$inv_amt = $investment['amount_inv'];
	// 			$dtotal_inv += $inv_amt;
	// 			$rate = 0;
	// 			foreach ($rbi as $value) {
	// 				if ($inv_date <= $value['date']) {
	// 					break;
	// 				}
	// 				$rate = $value['value'];	
	// 			}

	// 			$rbi_set = ($rate/100) * $inv_amt;

	// 			$to = Carbon::createFromFormat('Y-m-d', $dateOrder);
	// 			$from = Carbon::createFromFormat('Y-m-d', $inv_date);
	// 			$diff_in_days = $to->diffInDays($from);
	// 			$rbi_val = (($rbi_set)/365) * $diff_in_days;
	// 			$rbi_current = $rbi_val + $inv_amt;

	// 			$data_set['name'] = $investor_name;
	// 			$data_set['dop'] = $inv_date;
	// 			$data_set['inv_amt'] = $fmt->format($inv_amt);
	// 			$data_set['rbi_amt'] = $fmt->format(round(($rbi_val + $inv_amt), 2));

	// 			array_push($debt, $data_set);

	// 		}else{
	// 			$btotal_current += $historic_navs['nav'] * $investment['units'];

	// 			$inv_date = $investment['purchase_date'];
	// 			$inv_amt = $investment['amount_inv'];
	// 			$btotal_inv += $inv_amt;

	// 			$get_sensex = historic_bse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
	// 			$buy_sensex = $inv_amt/$get_sensex['closing_index'];
	// 			$current_sensex = historic_bse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
	// 			$current_sensex_val = $buy_sensex * $current_sensex['closing_index'];
	// 			$current_sensex_val = round($current_sensex_val, 2);
	// 			$bsensex_total += $current_sensex_val;
	// 			$current_sensex_val = (60/100) * $current_sensex_val;

	// 			$rbi = $this->rbi_fd;
	// 			$rate = 0;
	// 			foreach ($rbi as $value) {
	// 				if ($inv_date <= $value['date']) {
	// 					break;
	// 				}
	// 				$rate = $value['value'];	
	// 			}

	// 			$rbi_set = ($rate/100) * $inv_amt;

	// 			$to = Carbon::createFromFormat('Y-m-d', $dateOrder);
	// 			$from = Carbon::createFromFormat('Y-m-d', $inv_date);
	// 			$diff_in_days = $to->diffInDays($from);
	// 			$rbi_val = (($rbi_set)/365) * $diff_in_days;
	// 			$rbi_val = $rbi_val + $inv_amt;
	// 			$rbi_val = (40/100) * $rbi_val;

	// 			$data_set['name'] = $investor_name;
	// 			$data_set['dop'] = $inv_date;
	// 			$data_set['inv_amt'] = $fmt->format($inv_amt);
	// 			$data_set['compound'] = $fmt->format(round(($rbi_val + $current_sensex_val), 2));

	// 			$compound += $current_sensex_val + $rbi_val;

	// 			array_push($balanced, $data_set);

	// 		}
	// 	}
	// 	// dd($equity);
	// 	$etotal_current = round($etotal_current, 2);
	// 	$dtotal_current = round($dtotal_current, 2);
	// 	$btotal_current = round($btotal_current, 2);
	// 	$sensex_diff = $esensex_total - $etotal_current;
	// 	$nifty_diff = $nifty_total - $etotal_current;
	// 	$rbi_deff = $rbi_current - $dtotal_current;
	// 	$compound_deff = $compound - $btotal_current;
	// 	$sensex_diff = round($sensex_diff, 2);
	// 	$nifty_diff = round($nifty_diff, 2);
	// 	$compound = round($compound, 2);
	// 	$rbi_current = round($rbi_current, 2);
	// 	$rbi_deff = round($rbi_deff, 2);
	// 	$compound_deff = round($compound_deff, 2);

	// 	$etotal_current = $fmt->format($etotal_current);
	// 	$dtotal_current = $fmt->format($dtotal_current);
	// 	$btotal_current = $fmt->format($btotal_current);
	// 	$esensex_total = $fmt->format($esensex_total);
	// 	$nifty_total = $fmt->format($nifty_total);
	// 	$etotal_inv = $fmt->format($etotal_inv);
	// 	$dtotal_inv = $fmt->format($dtotal_inv);
	// 	$btotal_inv = $fmt->format($btotal_inv);
	// 	$rbi_current = $fmt->format($rbi_current);
	// 	$compound = $fmt->format($compound);

	// 	return view('benchmark',['RFtotal_equity' => $etotal_current, 'RFtotal_debt' => $dtotal_current,'RFtotal_balanced' => $btotal_current, 'sensex' => $esensex_total, 'nifty' => $nifty_total, 'equity' => $equity, 'debt' => $debt,'balanced' => $balanced, 'sensex_diff' => $sensex_diff, 'nifty_diff' => $nifty_diff, 'invest_amount_equity' => $etotal_inv, 'invest_amount_debt' => $dtotal_inv, 'invest_amount_balanced' => $btotal_inv, 'rbi_current' => $rbi_current, 'compound' => $compound, 'rbi_deff' => $rbi_deff, 'compound_deff' => $compound_deff]);
	// }

	public function csvUpdate()
	{
		// set_time_limit(2400);
		

		dd('smallcap & midcap update sucessfuly');

		
	}

	public function getPerson(Request $request)
	{
		// dd($request['investor_type']);
		if ($request['investor_type'] == 'subperson') {
			$data =  GroupMembers::where('id',$request['investor_id'])->get();
			return response()->json(['msg' => 1, 'data' => $data]);
			// dd($data);
		}else{
			$data = Person::where('id',$request['investor_id'])->get();
			return response()->json(['msg' => 2, 'data' => $data]);
		}
	}

	public function invUpdate(Request $request)
	{
		// edit_name
		// inv_id
		// dd($request['inv_id']);
		$save = InvestmentDetails::where('id',$request['inv_id'])
									->update(['investment_type' => $request['edit_name']]);
		if ($save == 1) {
			return response()->json(['msg' => 1]);
		}else{
			return response()->json(['msg' => 2]);
		}
	}

	public function updateAum(Request $request)
	{
		// http_response_code(500);
		// dd($request->all());
		$save = InvestmentDetails::where('id',$request['inv_id'])
									->update(['not_aum' => $request['aum_state']]);
		if ($save == 1) {
			return response()->json(['msg' => 1]);
		}else{
			return response()->json(['msg' => 2]);
		}

	}


}
