<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Group;
use App\GroupMembers;
use App\Person;
use App\SchemeDetails;
use App\HistoricNavs;
use Carbon\Carbon;
use App\InvestmentDetails;
use App\scheme_type;
use PHPExcel_Calculation_Financial;
use Excel;
use App\Queries;
use PDF;
use App\historic_nse;
use App\historic_bse;
use App\amcnames;
use App\midcap;
use App\smallcap;
use \NumberFormatter;
use DateTimeZone;
use App\ticketDetails;
use ZipArchive;

class AdminController extends Controller
{
		public $rbi_fd = [
					['date' => '2015-01-15', 'value' => '7.75'],
					['date' => '2015-03-04', 'value' => '7.50'],
					['date' => '2015-06-02', 'value' => '7.25'],
					['date' => '2015-09-29', 'value' => '6.75'],
					['date' => '2016-04-05', 'value' => '6.50'],
					['date' => '2016-10-04', 'value' => '6.25'],
					['date' => '2016-12-07', 'value' => '6.25'],
					['date' => '2017-02-08', 'value' => '6.25'],
					['date' => '2017-04-06', 'value' => '6.25'],
					['date' => '2017-06-08', 'value' => '6.25'],
					['date' => '2017-08-02', 'value' => '6.20']
				];

    public function home(){
		set_time_limit(600);
    	$users = User::where('role',0)->get();
    	$groups = Group::all();
    	$group_members = GroupMembers::all();
    	$persons = Person::all();
    	$investment_details = InvestmentDetails::where('not_aum',0)->get();
    	$scheme_types = scheme_type::all();
    	$amc_names = amcnames::all();
    	$is_notseen = InvestmentDetails::where('is_notseen',1)->get();
    	$seen_status = 0;

    	if (count($is_notseen) > 0) {
    		$seen_status = 1;
    	}else{
    		$seen_status = 0;
    	}

    	// dd($seen_status);
    	$aum = 0;

    	foreach ($investment_details as $investment) {
    		$date = Carbon::now()->subDays(1)->toDateString();

    		$current_nav = HistoricNavs::where('scheme_code',$investment->scheme_code)->orderBy('date','aesc')->first();

    		$current_value = (($investment->units) * ($current_nav['nav']));
    		//echo "Current Value -".$current_value."<br>";

    		$aum += $current_value;

    		//echo "AUM - ".$aum."<br><br>";
    	}

    	//$amount = $aum;
		setlocale(LC_MONETARY, 'en_IN');
		//$aum = money_format('%!i', $aum);
		//return $amount;

    	//return $aum;
    	// $SENSEX = file('https://finance.google.com/finance/getprices?q=SENSEX&f=d,o,h,l,c,');
    	// $master = [];
    	// foreach ($SENSEX as $value) {
    	// 	if (strpos($value, ',') !== false){
    	// 		$array = explode(",",$value);
    	// 		array_push($master,$array);
    	// 	}
    	// }
    	// // dd($master);
    	// $count = count($master);
    	// $sensex_val = (($master[$count - 1][1]-$master[$count - 2][1])/$master[$count - 2][1])*100;
    	// $sensex_val = round($sensex_val,2);

    	// $nifty = file('https://finance.google.com/finance/getprices?q=NIFTY&f=d,o,h,l,c,');
    	// $master2 = [];

    	// foreach ($nifty as $value) {
    	// 	if (strpos($value, ',') !== false){
    	// 		$array = explode(",",$value);
    	// 		array_push($master2,$array);
    	// 	}
    	// }
    	// $count2 = count($master2);
    	// $nifty_val = (($master2[$count2 - 1][1]-$master2[$count2 - 2][1])/$master2[$count2 - 2][1])*100;
    	// $nifty_val = round($nifty_val,2);
		$nifty_day_one = historic_nse::orderBy('date','desc')->first();
		$nifty_day_two = historic_nse::where('date','<',$nifty_day_one['date'])->orderBy('date','desc')->first();
		$nifty_val = (($nifty_day_one['closing_index']-$nifty_day_two['closing_index'])/$nifty_day_two['closing_index'])*100;
		$nifty_val = round($nifty_val, 2);

		$sensex_day_one = historic_bse::orderBy('date','desc')->first();
		$sensex_day_two = historic_bse::where('date','<',$sensex_day_one['date'])->orderBy('date','desc')->first();
		$sensex_val = (($sensex_day_one['closing_index']-$sensex_day_two['closing_index'])/$sensex_day_two['closing_index'])*100;
		$sensex_val = round($sensex_val, 2);

		$smallcap_day_one = smallcap::orderBy('date','desc')->first();
		$smallcap_day_two = smallcap::where('date','<',$smallcap_day_one['date'])->orderBy('date','desc')->first();
		$smallcap_val = (($smallcap_day_one['close']-$smallcap_day_two['close'])/$smallcap_day_two['close'])*100;
		$smallcap_val = round($smallcap_val, 2);

		$midcap_day_one = midcap::orderBy('date','desc')->first();
		$midcap_day_two = midcap::where('date','<',$midcap_day_one['date'])->orderBy('date','desc')->first();
		$midcap_val = (($midcap_day_one['close']-$midcap_day_two['close'])/$midcap_day_two['close'])*100;
		$midcap_val = round($midcap_val, 2);

		// dd($smallcap_val, $midcap_val);

    	$aum = round($aum,0);
    	return view('admin-home')->with(compact('users','groups','group_members','persons','aum','sensex_val','nifty_val','scheme_types','smallcap_val','midcap_val','amc_names','seen_status'));
    }

    public function addUser(Request $request){

    	    $validator = \Validator::make($request->all(),[

				'new_user_name' => 'required',
				'email' => 'required',
				'password' => 'required',

			]);

			if ($validator->fails()) {
				
				return redirect()->back()->withErrors($validator->errors());
				
			}
			else{

				$new_user = new User();
				$new_user->name = $request['new_user_name'];
				$new_user->email = $request['email'];
				$password = bcrypt($request['password']);
				$new_user->password = $password;

				$new_user_save = $new_user->save();

				if ($new_user_save) {
					return response()->json(['msg'=>'1']);

				}
				else{

					return response()->json(['msg'=>'0']);
				}

			}
    }


        public function addScheme(Request $request){

    	    $validator = \Validator::make($request->all(),[

				'new_scheme_name' => 'required',
				'scheme_code' => 'required',
				'scheme_file' => 'required',
				'scheme_type' => 'required',

			]);

			if ($validator->fails()) {
				
				return redirect()->back()->withErrors($validator->errors());
				
			}
			else{

				$scheme_info = scheme_type::where('id',$request['scheme_type'])->get();
				$amc_name = amcnames::where('id',$request['amc_name'])->value('name');
				$scheme_sub = $scheme_info[0]['scheme_sub'];
				$scheme_type = $scheme_info[0]['scheme_type'];


				if ($request->hasFile('scheme_file')) {
					//return "file irukku";
					$date = '';
					$new_scheme = new SchemeDetails();
					$new_scheme->scheme_name = $request['new_scheme_name'];
					$new_scheme->scheme_code = $request['scheme_code'];
					$new_scheme->scheme_type = $scheme_type;
					$new_scheme->scheme_sub = $scheme_sub;
					$new_scheme->amc_name = $amc_name;
					$save_scheme = $new_scheme->save();

					if ($save_scheme) {
						$file = $request->file('scheme_file');
					
						//$status = $file->move($destinationPath, $file->getClientOriginalName());
						$status = $file->move(base_path() . '/storage/csv/', $file->getClientOriginalName());

						if ($status) {
							if (($handle = fopen(base_path().'/storage/csv/'.$file->getClientOriginalName(),'r')) !== FALSE)
				    		{

				    			$no_of_lines = file(base_path().'/storage/csv/'.$file->getClientOriginalName());
								$no_of_lines = count($no_of_lines);
								$count = 0;
				        		while (($data = fgetcsv($handle, 1000, ',')) !==FALSE)
				        		{

					        		$nav = new HistoricNavs();

					        		//echo $data[5];
					                $nav->scheme_code = $data[0];
					                $nav->scheme_name = $data[1];
					                $nav->nav = $data[2];


					                $date = date('Y-m-d',strtotime($data[3]));
					                $nav->date = $date;
					                $nav->save();
					                $count++;
				        		}

				        		if ($no_of_lines == $count) {
				        			//echo "equla line";
				        			return response()->json(['msg'=>'1']);
				        		}else{
				        			//echo "not equla line";
				        			return response()->json(['msg'=>'0']);
				        		}				        		
				    		}

				    		//else of handle
				    		else{
				    			return response()->json(['msg'=>'0']);
				    		}

						}

						//else of status

						else{
							return response()->json(['msg'=>'0']);
						}

					}
					//else of failed to save scheme

					else{
						return response()->json(['msg'=>'0']);
					}
					
				}


				//else of no file

				else{
					return response()->json(['msg'=>'0']);
				}

				

				//return "file vandhuduchu daaww";

				//$new_scheme_save = $new_scheme->save();

				/*if ($new_scheme_save) {
					return response()->json(['msg'=>'1']);

				}
				else{

					return response()->json(['msg'=>'0']);
				}*/

			}
    }



    public function getInvestment(Request $request){
    	$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );

    	   	$validator = \Validator::make($request->all(),[
    	   		'investor_id' => 'required',
    	   		'investor_type' => 'required',
    	   		'date' => 'required',
			]);

			if ($validator->fails()) {
				
				return redirect()->back()->withErrors($validator->errors());
				//return "date varala da";
				
			}
			else{

				$investor_type = $request['investor_type'];
				$investor_id = $request['investor_id'];

				//echo $investor_type;
				//echo $investor_id;
				$myDate = date('Y-m-d',strtotime ($request['date']));
				$nifty_day_one = historic_nse::where('date','<=',$myDate)->orderBy('date','desc')->first();
				$nifty_day_two = historic_nse::where('date','<',$nifty_day_one['date'])->orderBy('date','desc')->first();
				$nifty_val = (($nifty_day_one['closing_index']-$nifty_day_two['closing_index'])/$nifty_day_two['closing_index'])*100;
				$nifty_val = round($nifty_val, 2);

				$sensex_day_one = historic_bse::where('date','<=',$myDate)->orderBy('date','desc')->first();
				$sensex_day_two = historic_bse::where('date','<',$sensex_day_one['date'])->orderBy('date','desc')->first();
				$sensex_val = (($sensex_day_one['closing_index']-$sensex_day_two['closing_index'])/$sensex_day_two['closing_index'])*100;
				$sensex_val = round($sensex_val, 2);

				$smallcap_day_one = smallcap::where('date','<=',$myDate)->orderBy('date','desc')->first();
				$smallcap_day_two = smallcap::where('date','<',$smallcap_day_one['date'])->orderBy('date','desc')->first();
				$smallcap_val = (($smallcap_day_one['close']-$smallcap_day_two['close'])/$smallcap_day_two['close'])*100;
				$smallcap_val = round($smallcap_val, 2);

				$midcap_day_one = midcap::where('date','<=',$myDate)->orderBy('date','desc')->first();
				$midcap_day_two = midcap::where('date','<',$midcap_day_one['date'])->orderBy('date','desc')->first();
				$midcap_val = (($midcap_day_one['close']-$midcap_day_two['close'])/$midcap_day_two['close'])*100;
				$midcap_val = round($midcap_val, 2);

				if ($investor_type == "subperson") {
					
					$pan = GroupMembers::where('id',$investor_id)->pluck('member_pan');
					//cho "The pan is".$pan[0];
				}


				if ($investor_type == "individual") {
					
					$pan = Person::where('id',$investor_id)->pluck('person_pan');
					//echo "The pan is".$pan[0];
				}

				$yesterday_date = Carbon::now()->subDay(1)->toDateString();
				$yesterday_date = date('Y-m-d',strtotime($yesterday_date));


				if ($request['date'] == 'empty') {
								
				$investments = InvestmentDetails::where('investor_id',$request['investor_id'])
				->where('investor_type',$request['investor_type'])
				->where('purchase_date','<=',$yesterday_date)
				->get();

				$total_amount = InvestmentDetails::where('investor_id',$request['investor_id'])
				->where('investor_type',$request['investor_type'])
				->where('purchase_date','<=',$yesterday_date)
				->where('is_inv',0)
				->sum('amount_inv');


								
				}

				if ($request['date'] != 'empty') {

					//return "not empty";
				$change_date = $request['date'];
				$change_date = date('Y-m-d',strtotime($change_date));

				$investments = InvestmentDetails::where('investor_id',$request['investor_id'])
				->where('investor_type',$request['investor_type'])
				->where('purchase_date','<=',$change_date)
				->get();

				//return $investments;


				$total_amount = InvestmentDetails::where('investor_id',$request['investor_id'])
				->where('investor_type',$request['investor_type'])
				->where('purchase_date','<=',$change_date)
				->where('is_inv',0)
				->sum('amount_inv');

				//return $total_amount;
					
				}

				


				

				$count = 0;
				$investment_details = array();
				$current_m_value = 0;
				$profit_or_loss = 0;
				//$total_amount = 0;
				$total_m_value = 0;
				$absolute_returns_avg = 0;
				$nav_date = '';
				$xirr_total = 0;

				


				/*if (empty($total_amount)) {
					$total_amount = 0;
				}*/

				if (count($investments) == 0) {
					return response()->json(['msg'=>'0','investment_details'=>$investment_details,'total_amount'=>$total_amount,'total_m_value'=>$total_m_value,'profit_or_loss'=>$profit_or_loss,'absolute_return_avg' => $absolute_returns_avg,'pan'=>$pan,'inv_id'=>$investor_id,'inv_type'=>$investor_type]);
				}

				else{
					$date_Array = [];
					$amount_Array = [];
					foreach ($investments as $investment) {
						$Scheme_type = SchemeDetails::where('scheme_code',$investment->scheme_code)->value('scheme_sub');
						if ($Scheme_type != "liquid" && $investment['is_inv'] == 0) {
							array_push($date_Array, $investment->purchase_date);
							array_push($amount_Array, (-1 * $investment->amount_inv));
						}

						$investment_details[$count]['investor_id'] = $request['investor_id'];
						$investment_details[$count]['investor_type'] = $request['investor_type'];
						$investment_details[$count]['investment_id'] = $investment->id;
						$investment_details[$count]['scheme_name'] = $investment->scheme_name;
						$investment_details[$count]['scheme_type'] = $Scheme_type;
						$investment_details[$count]['dop'] = $investment->purchase_date;
						$investment_details[$count]['amount_inv'] = $fmt->format($investment->amount_inv);
						$investment_details[$count]['purchase_nav'] = $investment->purchase_nav;
						$investment_details[$count]['units'] = round($investment->units,4);


						//$current_nav = HistoricNavs::where('scheme_code','106212')->first()->pluck('nav');

							if ($request['date'] == 'empty') {
								$current_nav = HistoricNavs::where('scheme_code',$investment->scheme_code)->orderBy('date','aesc')->first();
								//return $current_nav;
								//echo $current_nav['nav'];
								$nav_date = $current_nav['date'];
								$current_nav = $current_nav['nav'];
								
								//echo $current_nav['nav'];
								//echo $nav_date;
								//echo $current_nav;
								//echo "empty date";
								
							}

							if ($request['date'] != 'empty') {

								$change_date = $request['date'];



								$change_date = date('Y-m-d',strtotime($change_date));
								//return $change_date;
								//return $change_date;
								$current_nav = HistoricNavs::where('scheme_code',$investment->scheme_code)->where('date','<=' ,$change_date)->orderBy('date','aesc')->first();

								
								//return $current_nav;
								$nav_date = $current_nav['date'];
								$current_nav = $current_nav['nav'];
								
								//echo "non empty";
								//return $nav_date;

								/*foreach ($current_nav as $nav) {
									$pass_nav = $nav->nav;
								}*/
							    $nav_date_one = HistoricNavs::where('scheme_code',$investment->scheme_code)->orderBy('date','desc')->first();
								$nav_date_two = HistoricNavs::where('scheme_code',$investment->scheme_code)->where('date','<',$nav_date_one['date'])->orderBy('date','desc')->first();
								$navState = (($nav_date_one['nav']-$nav_date_two['nav'])/$nav_date_two['nav']) * 100;
								$navState = round($navState, 2);
							}


						//return $nav_date;
						$nav_date = date('d-m-Y',strtotime($nav_date));
						//return $nav_date;
						//return $nav_date;
						//echo $current_nav['nav'];
						//echo $investment->dop;
						$profi_loss = round((($investment->units) * $current_nav) - (($investment->units) * ($investment->purchase_nav)),2);
						$investment_details[$count]['current_nav'] = $current_nav;
						$investment_details[$count]['current_market_value'] = $fmt->format(round(($investment->units) * $current_nav,2));
						$investment_details[$count]['folio_number'] = $investment->folio_number;
						$investment_details[$count]['p_or_loss'] = $fmt->format($profi_loss);
						$investment_details[$count]['delta'] = $navState;
						
						

						$current_m_value = ($investment->units) * $current_nav; 
						$total_m_value += $current_m_value;
						if ($Scheme_type != "liquid") {
							$xirr_total += $current_m_value;
						}
						// $profit_or_loss +=  (($investment->units) * $current_nav) - (($investment->units) * ($investment->purchase_nav));

						$initial_value = ($investment->purchase_nav) * ($investment->units);
						$absolute_returns = (($current_m_value - $initial_value)/$initial_value) * 100;
						//echo $absolute_returns;
						$investment_details[$count]['abs_returns'] = round($absolute_returns,2);

						// $absolute_returns_avg += $absolute_returns;
						//echo $absolute_returns;
						//echo $absolute_returns_avg;

						$today = Carbon::now()->subDay(1)->toDateString();
						$today = date('Y-m-d',strtotime($today));
						$purchase_date = $investment->purchase_date;

						$date_diff = date_diff(date_create($purchase_date),date_create($today));
						$date_diff = $date_diff->format("%a");


						$annualised_return = (($absolute_returns * 365)/$date_diff) ;
						$investment_details[$count]['ann_returns'] = round($annualised_return,2);

						
						$count++;
				}

				//echo ($absolute_returns_avg/$count);
				$profit_or_loss = $total_m_value - $total_amount;
				$absolute_returns_avg = (($total_m_value - $total_amount)/$total_amount) * 100;

				// $absolute_returns_avg = round(($profit_or_loss/$total_amount)*100,2);
				$absolute_returns_avg = round($absolute_returns_avg, 2);
				$current_m_value = round($current_m_value,2);
				$profit_or_loss = round($profit_or_loss,2);
				$total_m_value = round($total_m_value,2);

				//print_r($total_amount);
				//print_r($current_m_value);
				//print_r($profit_or_loss);

				//return $investment_details;
				//print_r($current_nav);*/

				//return $total_amount;
				array_push($date_Array, $change_date);
				array_push($amount_Array, $xirr_total);
				$xirr_Val = PHPExcel_Calculation_Financial::XIRR($amount_Array,$date_Array);
				$xirr_Val = $xirr_Val * 100;
				$xirr_Val = round($xirr_Val, 2);

				$total_amount = $fmt->format($total_amount);
				$total_m_value = $fmt->format($total_m_value);
				$profit_or_loss = $fmt->format($profit_or_loss);

				return response()->json(['msg'=>'1','investment_details'=>$investment_details,'total_amount'=>$total_amount,'total_m_value'=>$total_m_value,'profit_or_loss'=>$profit_or_loss,'absolute_return_avg' => $absolute_returns_avg,'pan'=>$pan,'inv_id'=>$investor_id,'inv_type'=>$investor_type,'nav_date'=>$nav_date,'xirr_value'=>$xirr_Val,'sensex_val'=>$sensex_val,'nifty_val'=>$nifty_val,'smallcap'=>$smallcap_val,'midcap'=>$midcap_val]);
				}




				
			}

    }

    public function downloadInvestments(Request $request){
    	   	$validator = \Validator::make($request->all(),[
    	   		'investor_id' => 'required',
    	   		'investor_type' => 'required',
    	   		'ex_nav_date' => 'required',
			]);

			if ($validator->fails()) {
				
				return redirect()->back()->withErrors($validator->errors());
				
			}
			else{

				$investor_id = $request['investor_id'];
				$investor_type = $request['investor_type'];


				if ($investor_type == 'cis') {
					//echo "cis received";

				$group_id = $request['investor_id'];
				$group_mem_id = array();
				$group_mem_id = GroupMembers::where('group_id',$group_id)->pluck('id')->toArray();

				$count = 0;
				$cis_details = array();
				$yesterday_date = Carbon::now()->subDay(1)->toDateString();
				$yesterday_date = date('Y-m-d',strtotime($yesterday_date));
				$investment_detail = array();
				$total_m_value = 0;
				$profit_or_loss = 0;
				$absolute_returns_avg = 0;
				$total_amount_inv =0;
				$scheme_inv_total = 0;
				$current_market_value = 0;

				$grand_total = 0;
				$grand_market_value = 0;
				$grand_profit_or_loss = 0;
				$grand_absolute_returns = 0;
				$units = 0;

				$unique_scheme = array();

				$check_them = array();
				$investor_ids = array();
				$investor_id = array();
				$separate_inv = array();

				$inv_pass = array();

				$change_date = $request['ex_nav_date'];

					foreach ($group_mem_id as $member_id) {
						$investor_id[] = $member_id;
					}

				$investment_detail = InvestmentDetails::whereIn('investor_id',$investor_id)->where('investor_type','subperson')->where('purchase_date','<=',$change_date)->get();

				//print_r($investment_detail);

				foreach ($investment_detail as $sep_inv) {
					
					/*if (array_key_exists($sep_inv->investor_id, $separate_inv)) {

					}
					else{*/

						$investor_name = GroupMembers::where('id',$sep_inv->investor_id)->pluck('member_name');

						$investor_name = $investor_name[0];

						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Investor Name'] = $investor_name ;

						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Scheme Name'] = $sep_inv->scheme_name; 
						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Amount Invested'] = $sep_inv->amount_inv; 
						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Purchase date'] = date('d-M-Y',strtotime($sep_inv->purchase_date));

						$current_nav = HistoricNavs::where('scheme_code',$sep_inv->scheme_code)->where('date','<=' ,$change_date)->orderBy('date','aesc')->first();

						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Purchase NAV'] = $sep_inv->purchase_nav;
						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Units'] = $sep_inv->units;

						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current NAV'] = $current_nav['nav'];

						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Current Market Value'] = round($current_nav['nav'] * $sep_inv->units,2);

						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Unrealised Profit/Loss'] = round(($current_nav['nav'] * $sep_inv->units) - ($sep_inv->amount_inv),2);

						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Absolute Returns'] = round((($separate_inv[$sep_inv->investor_id][$sep_inv->id]['Unrealised Profit/Loss'])/($sep_inv->amount_inv)) * 100,2);

						$separate_inv[$sep_inv->investor_id][$sep_inv->id]['Folio Number'] = $sep_inv->folio_number;      

					//}

				}

				//return $separate_inv;
				//print_r($investment_detail);

					foreach ($investment_detail as $investments) {
						
						if (array_key_exists($investments->scheme_code, $inv_pass)) {
							
							
							$inv_pass[$investments->scheme_code]['Scheme Name'] = $investments->scheme_name;

							$inv_pass[$investments->scheme_code]['Amount Invested'] += $investments->amount_inv;

							//$current_nav = HistoricNavs::where('scheme_code',$investments->scheme_code)->orderBy('date','aesc')->first();
							$current_nav = HistoricNavs::where('scheme_code',$investments->scheme_code)->where('date','<=' ,$change_date)->orderBy('date','aesc')->first();

							$inv_pass[$investments->scheme_code]['Units'] += round(($investments->units),4);
							//$inv_pass[$investments->scheme_code]['units'] = $units;
							$inv_pass[$investments->scheme_code]['Current NAV'] = $current_nav['nav'];

										
							$current_market_value = (($inv_pass[$investments->scheme_code]['Units']) * $current_nav['nav']);

							$current_market_value = round($current_market_value,2);
							$inv_pass[$investments->scheme_code]['Current Market Value'] = $current_market_value;

							$inv_pass[$investments->scheme_code]['Unrealised Profit/Loss'] = (($inv_pass[$investments->scheme_code]['Current Market Value']) - ($inv_pass[$investments->scheme_code]['Amount Invested']));

							$inv_pass[$investments->scheme_code]['Absolute Returns (%)'] = round(((($inv_pass[$investments->scheme_code]['Current Market Value']) - $inv_pass[$investments->scheme_code]['Amount Invested'])/($inv_pass[$investments->scheme_code]['Amount Invested'])) * 100,2);
						}
						else{

							$inv_pass[$investments->scheme_code]['Scheme Name'] = $investments->scheme_name;

							$inv_pass[$investments->scheme_code]['Amount Invested'] = $investments->amount_inv;
							

							//$current_nav = HistoricNavs::where('scheme_code',$investments->scheme_code)->orderBy('date','aesc')->first();
							$current_nav = HistoricNavs::where('scheme_code',$investments->scheme_code)->where('date','<=' ,$change_date)->orderBy('date','aesc')->first();
							$inv_pass[$investments->scheme_code]['Current NAV'] = $current_nav['nav'];

							$inv_pass[$investments->scheme_code]['Units'] = round(($investments->units),4);
										
							$current_market_value = (($investments->units) * $current_nav['nav']);

							$current_market_value = round($current_market_value,2);

							$inv_pass[$investments->scheme_code]['Current Market Value'] = $current_market_value;

							$inv_pass[$investments->scheme_code]['Unrealised Profit/Loss'] = (($inv_pass[$investments->scheme_code]['Current Market Value']) - ($inv_pass[$investments->scheme_code]['Amount Invested']));

							$inv_pass[$investments->scheme_code]['Absolute Returns (%)'] = round(((($inv_pass[$investments->scheme_code]['Current Market Value']) - $inv_pass[$investments->scheme_code]['Amount Invested'])/($inv_pass[$investments->scheme_code]['Amount Invested'])) * 100,2);

						}

						$grand_total += $investments->amount_inv;

						$current_nav = HistoricNavs::where('scheme_code',$investments->scheme_code)->orderBy('date','aesc')->first();
						$current_nav = $current_nav['nav'];
						$units_held = $investments->units;

						$current_market_value = $units_held * $current_nav;

						$grand_market_value += $current_market_value;

						

						$count++;	
					}

						/*$profit_or_loss = $grand_market_value - $grand_total;
						$grand_profit_or_loss += $profit_or_loss;
						$grand_absolute_returns = ((($grand_market_value -$grand_total)/$grand_total) * 100);

						$grand_total = round($grand_total,2);
						$grand_market_value = round($grand_market_value,2);
						$grand_profit_or_loss = round($grand_profit_or_loss,2);	
						$grand_absolute_returns = round($grand_absolute_returns,2);*/

							//return $investment_detailss;

							//print_r($inv_pass);

							$final_array = array();

							$count = 0;
							$pro_array =  array();
							$member_name = '';

							//print_r($separate_inv);
							/*foreach ($separate_inv as $inv) {

								$pro_array[$count] = $inv;
								$count++;
								
							}*/


							/*foreach ($separate_inv as $inv) {

								foreach ($inv as $in) {
									echo $in['investor_name'];
								}

			               		

			               	}*/





							$file_content = \Excel::create('CIS Report', function($excel) use ($inv_pass,$separate_inv) {
			                $excel->sheet('CIS Report', function($sheet) use ($inv_pass)
			                {
			                    $sheet->fromArray($inv_pass);
			                    $sheet->setWidth('A', 50);
			                    $sheet->setWidth('B', 15);
			                    $sheet->setWidth('C', 20);
			                    $sheet->setWidth('D', 18);
			                    $sheet->setWidth('E', 20);
			                    $sheet->setWidth('F', 20);
			                    $sheet->setWidth('G', 20);

			                    
			                });

			               

			               foreach ($separate_inv as $inv) {

			               		//echo $inv['investor_name'];

			               		foreach($inv as $in){
									$member_name = $in['Investor Name'];
									//array_slice($in, 0,1);
			               		}

			               		//foreach ($inv as $in) {
			               			
			               			

				               		$excel->sheet($member_name, function($sheet) use ($inv)
					                {
					                 	$sheet->fromArray($inv);  
					                 	$sheet->setWidth('A', 20); 
					                 	$sheet->setWidth('B', 50); 
					                 	$sheet->setWidth('C', 20);
					                 	$sheet->setWidth('D', 15);
					                 	$sheet->setWidth('E', 15);
					                 	$sheet->setWidth('F', 15);
					                 	$sheet->setWidth('G', 15);
					                 	$sheet->setWidth('H', 25);
					                 	$sheet->setWidth('I', 25);
					                 	$sheet->setWidth('J', 18);
					                 	$sheet->setWidth('K', 15);

					                 /*$sheet->cell('A1:B1:C1:D1:E1:F1:G1:H1:I1:J1:K1:L1:M1:N1:O1:P1', function($cell) {

								    $cell->setFontSize(12);

								    $cell->setFontWeight('bold');
									});*/

					                });
			               		//}
			               }
			            	})->download('xlsx');
				}
				else{
					$change_date = $request['ex_nav_date'];
					$investor_id = $request['investor_id'];
					$investor_type = $request['investor_type'];


					$change_date = date('Y-m-d',strtotime($change_date));
					$investment_details = InvestmentDetails::where('investor_type',$investor_type)
					->where('investor_id',$investor_id)
					->where('purchase_date','<=',$change_date)->get();

					//->orderby('purchase_date','desc')
					//->get();

					//return $investment_details;



					//$investment_details = $investment_details->toArray();

					//print_r($investment_details);

					$export = array();
					$yesterday_date = Carbon::now()->subDay(1)->toDateString();
					$yesterday_date = date('Y-m-d',strtotime($yesterday_date));
					foreach ($investment_details as $investment_detail) {

						$current_nav = HistoricNavs::where('scheme_code',$investment_detail->scheme_code)->orderBy('date','aesc')->first();

						$current_market_value = ($investment_detail->units) * $current_nav['nav'];
						$p_or_loss = (($investment_detail->units) * $current_nav['nav']) - (($investment_detail->units) * ($investment_detail->purchase_nav));

						$today = Carbon::now()->subDay(1)->toDateString();
						$today = date('Y-m-d',strtotime($today));
						$purchase_date = $investment_detail->purchase_date;

						$date_diff = date_diff(date_create($purchase_date),date_create($today));
						$date_diff = $date_diff->format("%a");

						$abs_returns = (($current_market_value - $investment_detail->amount_inv)/$investment_detail->amount_inv) * 100;
						$ann_returns = (($abs_returns *365)/$date_diff);

						$date=date_create($investment_detail->purchase_date);
						$purchase_date = date_format($date,"d-M-y");

						$export[] = array(

							'Scheme Name' => $investment_detail->scheme_name,
							//'Scheme Type' => $investment_detail->scheme_type,
							'Folio Number' => $investment_detail->folio_number,
							'Date of Purchase' => $purchase_date,
							'Amount Invested' => $investment_detail->amount_inv,
							'Purchase NAV' => $investment_detail->purchase_nav,
							'Units' => round($investment_detail->units,4),
							'Current NAV' => $current_nav['nav'],
							'Current Market Value' => round($current_market_value,2),
							'Unrealised Profit/Loss' => round($p_or_loss,2),		
							'Absolute Returns' => round($abs_returns,2),
							'Annualised Return' => round($ann_returns,2),

							);
					}

					//print_r($export);
					$file_content = \Excel::create('Investment Summary', function($excel) use ($export) {
	                $excel->sheet('Investment Summary', function($sheet) use ($export)
	                {
	                    $sheet->fromArray($export);
	                });
	            	})->download('xlsx');

				}
            	
			}
    }

    public function cis(Request $request){
		$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );
    	
    	    $validator = \Validator::make($request->all(),[

				'group_id' => 'required',
				'date' => 'required',

			]);

			if ($validator->fails()) {
				
				return redirect()->back()->withErrors($validator->errors());
				
			}
			else{
				$group_id = $request['group_id'];
				$group_mem_id = array();
				$group_mem_id = GroupMembers::where('group_id',$group_id)->pluck('id')->toArray();

				$count = 0;
				$cis_details = array();
				$yesterday_date = Carbon::now()->subDay(1)->toDateString();
				$yesterday_date = date('Y-m-d',strtotime($yesterday_date));
				$investment_detail = array();
				$total_m_value = 0;
				$profit_or_loss = 0;
				$absolute_returns_avg = 0;
				$total_amount_inv =0;
				$scheme_inv_total = 0;
				$current_market_value = 0;

				$grand_total = 0;
				$grand_market_value = 0;
				$grand_profit_or_loss = 0;
				$grand_absolute_returns = 0;
				$units = 0;
				$change_date = '';

				$current_nav = '';

				$xirr_total = 0;

				$unique_scheme = array();

				$check_them = array();
				$investor_ids = array();

				$inv_pass = array();
				$cis_date = '';

					foreach ($group_mem_id as $member_id) {
						$investor_id[] = $member_id;
					}


					if ($request['date'] == 'empty') {

						$investment_detail = InvestmentDetails::whereIn('investor_id',$investor_id)
						->where('investor_type','subperson')
						->where('purchase_date','<=',$yesterday_date)
						->get();
					}
					if ($request['date'] != 'empty') {

						$change_date = $request['date'];
						$change_date = date('Y-m-d',strtotime($change_date));
						$investment_detail = InvestmentDetails::whereIn('investor_id',$investor_id)
						->where('investor_type','subperson')
						->where('purchase_date','<=',$change_date)
						->get();
					}

				
					$dateArray = [];
					$amountArray = [];

					foreach ($investment_detail as $investments) {
						$Scheme_type = SchemeDetails::where('scheme_code',$investments->scheme_code)->value('scheme_type');

						if ($Scheme_type != "liquid") {
							array_push($dateArray, $investments->purchase_date);
							array_push($amountArray, (-1 * $investments->amount_inv));
						}

						if ($request['date'] == 'empty') {
								$current_nav = HistoricNavs::where('scheme_code',$investments->scheme_code)->orderBy('date','aesc')->first();
								//return $current_nav;
								//echo $current_nav['nav'];
								$nav_date = $current_nav['date'];
								$change_date = date('d-m-Y',strtotime($nav_date));

								$current_nav = $current_nav['nav'];
								
								//echo $current_nav['nav'];
								//echo $nav_date;
								//echo $current_nav;
								//echo "empty date";
								
							}

							if ($request['date'] != 'empty') {

								$change_date = $request['date'];
								//return $change_date;
								$change_date = date('Y-m-d',strtotime($change_date));
								//return $change_date;
								$current_nav = HistoricNavs::where('scheme_code',$investments->scheme_code)->where('date','<=' ,$change_date)->orderBy('date','aesc')->first();
								$nav_date_one = HistoricNavs::where('scheme_code',$investments->scheme_code)->orderBy('date','desc')->first();
								$nav_date_two = HistoricNavs::where('scheme_code',$investments->scheme_code)->where('date','<',$nav_date_one['date'])->orderBy('date','desc')->first();
								$navState = (($nav_date_one['nav']-$nav_date_two['nav'])/$nav_date_two['nav']) * 100;
								$navState = round($navState, 2);
								
									//return $current_nav;
								$nav_date = $current_nav['date'];
								$change_date = date('d-m-Y',strtotime($nav_date));
								//return $nav_date;
								$current_nav = $current_nav['nav'];
								
								//echo "non empty";
								//return $nav_date;

								/*foreach ($current_nav as $nav) {
									$pass_nav = $nav->nav;
								}*/
							}

						if (array_key_exists($investments->scheme_code, $inv_pass)) {
							$inv_pass[$investments->scheme_code]['delta'] = $navState;
							
							$inv_pass[$investments->scheme_code]['total_amount_inv'] += $investments->amount_inv;
							$inv_pass[$investments->scheme_code]['scheme_name'] = $investments->scheme_name;



							$inv_pass[$investments->scheme_code]['units'] += ($investments->units);
							//$inv_pass[$investments->scheme_code]['units'] = $units;
							$inv_pass[$investments->scheme_code]['current_nav'] = $current_nav;

										
							$current_market_value = (($inv_pass[$investments->scheme_code]['units']) * $current_nav);

							$current_market_value = round($current_market_value,2);
							$inv_pass[$investments->scheme_code]['current_market_value'] = $current_market_value;

							$inv_pass[$investments->scheme_code]['profit_or_loss'] = (($inv_pass[$investments->scheme_code]['current_market_value']) - ($inv_pass[$investments->scheme_code]['total_amount_inv']));

							$inv_pass[$investments->scheme_code]['absolute_returns'] = round(((($inv_pass[$investments->scheme_code]['current_market_value']) - $inv_pass[$investments->scheme_code]['total_amount_inv'])/($inv_pass[$investments->scheme_code]['total_amount_inv'])) * 100,2);
						}
						else{
							$inv_pass[$investments->scheme_code]['delta'] = $navState;

							$inv_pass[$investments->scheme_code]['total_amount_inv'] = $investments->amount_inv;
							$inv_pass[$investments->scheme_code]['scheme_name'] = $investments->scheme_name;


							$inv_pass[$investments->scheme_code]['current_nav'] = $current_nav;

							$inv_pass[$investments->scheme_code]['units'] = ($investments->units);
										
							$current_market_value = (($investments->units) * $current_nav);

							$current_market_value = round($current_market_value,2);

							$inv_pass[$investments->scheme_code]['current_market_value'] = $current_market_value;

							$inv_pass[$investments->scheme_code]['profit_or_loss'] = (($inv_pass[$investments->scheme_code]['current_market_value']) - ($inv_pass[$investments->scheme_code]['total_amount_inv']));

							if ($investments->scheme_code == "106212") {
								//echo $inv_pass[$investments->scheme_code]['profit_or_loss']."<br>";
							}

							$inv_pass[$investments->scheme_code]['absolute_returns'] = round(((($inv_pass[$investments->scheme_code]['current_market_value']) - $inv_pass[$investments->scheme_code]['total_amount_inv'])/($inv_pass[$investments->scheme_code]['total_amount_inv'])) * 100,2);

						}

						$grand_total += $investments->amount_inv;

						//$current_nav = HistoricNavs::where('scheme_code',$investments->scheme_code)->orderBy('date','aesc')->first();
						//$current_nav = $current_nav['nav'];
						$units_held = $investments->units;

						$current_market_value = $units_held * $current_nav;

						$grand_market_value += $current_market_value;

						if ($Scheme_type != "liquid") {
							$xirr_total += $current_market_value;
						}

						

						$count++;	
					}

						$profit_or_loss = $grand_market_value - $grand_total;
						$grand_profit_or_loss += $profit_or_loss;
						$grand_absolute_returns = ((($grand_market_value -$grand_total)/$grand_total) * 100);

						$grand_total = round($grand_total,2);
						$grand_market_value = round($grand_market_value,2);
						$grand_profit_or_loss = round($grand_profit_or_loss,2);	
						$grand_absolute_returns = round($grand_absolute_returns,2);

				

					if ($count == 0) {
						return response()->json(['msg' => '0']);
					}
					else{



						$export_date = date('Y-m-d',strtotime($change_date));
						$change_date = date('d-m-Y',strtotime($change_date));

						//return response()->json(['msg'=> '1','response'=>$cis_details]);
						$absolute_returns_avg = round(($absolute_returns_avg/$count),2);
						$current_market_value = round($current_market_value,2);
						$profit_or_loss = round($profit_or_loss,2);
						$total_m_value = round($total_m_value,2);

						array_push($dateArray, $export_date);
						array_push($amountArray, $xirr_total);

						$xirrVal = PHPExcel_Calculation_Financial::XIRR($amountArray,$dateArray);
						$xirrVal = $xirrVal * 100;
						$xirrVals = round($xirrVal,2);
						$grand_total = $fmt->format($grand_total);
						$grand_profit_or_loss = $fmt->format($grand_profit_or_loss);
						$grand_market_value = $fmt->format($grand_market_value);

						return response()->json(['msg'=>'1','absolute_returns_avg'=>$grand_absolute_returns,'total_amount_inv'=>$grand_total,'profit_or_loss'=>$grand_profit_or_loss,'total_m_value'=>$grand_market_value,'cis_details'=>$inv_pass,'date'=>$change_date,'export_date'=>$export_date, 'xirr_value'=>$xirrVals]);
						//print_r($inv_pass);
					}
				
				
			}
    }


        public function deleteInvestment(Request $request){
    	   	$validator = \Validator::make($request->all(),[

				'inv_id' => 'required',
				'investor_id' => 'required',
				'investor_type' => 'required',

			]);

			if ($validator->fails()) {
				
				return redirect()->back()->withErrors($validator->errors());
				
			}
			else{ 

				$delete_inv = InvestmentDetails::where('id',$request['inv_id'])->where('investor_id',$request['investor_id'])->where('investor_type',$request['investor_type'])->delete();

				if ($delete_inv) {
						$investments = InvestmentDetails::where('investor_id',$request['investor_id'])
					->where('investor_type',$request['investor_type'])
					->get();

					$count = 0;
					$investment_details = array();
					$current_m_value = 0;
					$profit_or_loss = 0;
					$total_amount = 0;
					$total_m_value = 0;
					$absolute_returns_avg = 0;

					$yesterday_date = Carbon::now()->subDay(1)->toDateString();
					$yesterday_date = date('Y-m-d',strtotime($yesterday_date));

					$total_amount = InvestmentDetails::where('investor_id',$request['investor_id'])
					->where('investor_type',$request['investor_type'])
					->sum('amount_inv');

					if (empty($total_amount)) {
						$total_amount = 0;
					}

					if (count($investments) == 0) {
						return response()->json(['msg'=>'1','investment_details'=>$investment_details,'total_amount'=>$total_amount,'total_m_value'=>$total_m_value,'profit_or_loss'=>$profit_or_loss,'absolute_return_avg' => $absolute_returns_avg]);
					}

					else{

						foreach ($investments as $investment) {

							$investment_details[$count]['investor_id'] = $request['investor_id'];
							$investment_details[$count]['investor_type'] = $request['investor_type'];
							$investment_details[$count]['investment_id'] = $investment->id;
							$investment_details[$count]['scheme_name'] = $investment->scheme_name;
							$investment_details[$count]['scheme_type'] = $investment->scheme_type;
							$investment_details[$count]['dop'] = $investment->purchase_date;
							$investment_details[$count]['amount_inv'] = $investment->amount_inv;
							$investment_details[$count]['purchase_nav'] = $investment->purchase_nav;
							$investment_details[$count]['units'] = round($investment->units,4);


							//$current_nav = HistoricNavs::where('scheme_code','106212')->first()->pluck('nav');
							$current_nav = HistoricNavs::where('scheme_code',$investment->scheme_code)->orderBy('date','aesc')->first();
							//echo $current_nav['nav'];
							//echo $investment->dop;
							$investment_details[$count]['current_nav'] = $current_nav['nav'];
							$investment_details[$count]['current_market_value'] = round(($investment->units) * $current_nav['nav'],2);
							$investment_details[$count]['folio_number'] = $investment->folio_number;
							$investment_details[$count]['p_or_loss'] = round((($investment->units) * $current_nav['nav']) - (($investment->units) * ($investment->purchase_nav)),2);
							
							

							$current_m_value = ($investment->units) * $current_nav['nav']; 
							$total_m_value += $current_m_value;
							$profit_or_loss +=  (($investment->units) * $current_nav['nav']) - (($investment->units) * ($investment->purchase_nav));

							$initial_value = ($investment->purchase_nav) * ($investment->units);
							$absolute_returns = (($current_m_value - $initial_value)/$initial_value) * 100;
							//echo $absolute_returns;
							$investment_details[$count]['abs_returns'] = round($absolute_returns,2);

							$absolute_returns_avg += $absolute_returns;
							//echo $absolute_returns;
							//echo $absolute_returns_avg;

							$today = Carbon::now()->subDay(1)->toDateString();
							$today = date('Y-m-d',strtotime($today));
							$purchase_date = $investment->purchase_date;

							$date_diff = date_diff(date_create($purchase_date),date_create($today));
							$date_diff = $date_diff->format("%a");


							$annualised_return = (($absolute_returns * 365)/$date_diff) ;
							$investment_details[$count]['ann_returns'] = round($annualised_return,2);

							
							$count++;
					}

					//echo ($absolute_returns_avg/$count);

					$absolute_returns_avg = round(($absolute_returns_avg/$count),2);
					$current_m_value = round($current_m_value,2);
					$profit_or_loss = round($profit_or_loss,2);
					$total_m_value = round($total_m_value,2);

					//print_r($total_amount);
					//print_r($current_m_value);
					//print_r($profit_or_loss);

					//return $investment_details;
					//print_r($current_nav);*/
					return response()->json(['msg'=>'1','investment_details'=>$investment_details,'total_amount'=>$total_amount,'total_m_value'=>$total_m_value,'profit_or_loss'=>$profit_or_loss,'absolute_return_avg' => $absolute_returns_avg]);
					}
				}
				else{
					return response()->json(['msg'=>'0']);
				}	

			}
    }


    public function checkPass(Request $request){

    	   $validator = \Validator::make($request->all(),[

				'pass' => 'required',
				

			]);

			if ($validator->fails()) {
				
				return redirect()->back()->withErrors($validator->errors());
				
			}
			else{ 

				$password = $request['pass'];

				// $hashed = \Hash::make($password);

				$admin_pass = User::where('email','vasudevgupta@gmail.com')->pluck('password');

				$admin_pass = $admin_pass[0];


				if (\Hash::check($password, $admin_pass))
				{
					    return response()->json(['msg'=>'1']);
				}
				else{
						return response()->json(['msg'=>'0']);
				}
			}

    }

    public function getAmc()
	{
		$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );
		$grand_total_inv = 0;
		$grand_total_crt = 0;
		$amcNames = amcnames::pluck('name');
		$schemebyamc = SchemeDetails::get()
						->groupBy('amc_name');
		$nameCount = count($amcNames);
		$final_data_set = [];
		for ($i=0; $i < $nameCount  ; $i++) { 
			$data_set = [];
			if (isset($schemebyamc[$amcNames[$i]])) {
				array_push($data_set, $amcNames[$i]);
				$amount_invs = 0;
				$current_val = 0;
				foreach ($schemebyamc[$amcNames[$i]] as $value) {
					$investments = InvestmentDetails::where('scheme_code',$value['scheme_code'])->get();
        			$historic_navs = HistoricNavs::where('scheme_code',$value['scheme_code'])
        							->orderBy('date', 'desc')
        							->first();

        			foreach ($investments as $investment) {

		        			$amount_invs += $investment['amount_inv'];	
		        			$current_val += $investment['units'] * $historic_navs['nav'];	
    				}	
				}

				$current_val = round($current_val, 2);
				$grand_total_inv += $amount_invs; 
				$grand_total_crt += $current_val;
				array_push($data_set, $amount_invs);
				array_push($data_set, $current_val);
				array_push($final_data_set, $data_set);
			}
		}


		$final_array = [];
		foreach ($final_data_set as $value) {
        	$data_set_array = [];
        	$avg = 0;
        		$avg = ($value[1]/$grand_total_inv) * 100;
	        	$avg = round($avg, 2);
     	
        	// array_push($data_set_array, $value[0], $value[1], $value[2], $avg);
			$amount = $fmt->format($value[1]);
			$amount2 = $fmt->format($value[2]);
	        $data_set_array['name'] = $value[0];
	        $data_set_array['AmountInvet'] = $amount;
	        $data_set_array['CurrentVal'] = $amount2;
	        $data_set_array['Avg'] = $avg;
        	array_push($final_array, $data_set_array);
        }

        $total['AmountInvet'] = $fmt->format($grand_total_inv);
        $total['CurrentVal'] = $fmt->format($grand_total_crt);
        $total['Avg'] = 100;
        // dd($final_array);
        return view('amc',['amc_arrya' => $final_array, 'grand_total' => $total]);

		
	}

	public function amcDetails(Request $request)
	{
		$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );

		$name = $request['name'];
		$grand_total_inv = 0;
		$grand_total_crt = 0;
		if ($name == 'L'){
			$name = "L&T";
		}
		$schemes = SchemeDetails::where('amc_name',$name)->pluck('scheme_code');
		$count = count($schemes);
		$investment_details = [];
		for ($i=0; $i < $count ; $i++) { 
			$HistoricNav = HistoricNavs::where('scheme_code',$schemes[$i])
        							->orderBy('date', 'desc')
        							->first();
        	$investments = InvestmentDetails::where('scheme_code',$schemes[$i])
        							->get();
        	foreach ($investments as $investment) {
        		$investment_detail = [];
        		$pan = "";
        		$investor_type = $investment['investor_type'];
				$current_val = $investment['units'] * $HistoricNav['nav'];
				$current_val = round($current_val, 2);
        		if ($investor_type == "subperson") {
					$pan = GroupMembers::where('id',$investment['investor_id'])->value('member_pan');
				}else if ($investor_type == "individual") {
					$pan = Person::where('id',$investment['investor_id'])->value('person_pan');
				}

				$grand_total_inv += $investment['amount_inv'];
				$grand_total_crt += $current_val;

				$investment_detail['name'] = $investment['scheme_name'];
				$investment_detail['pan'] = $pan;
				$investment_detail['invest_amount'] = $fmt->format($investment['amount_inv']);
				$investment_detail['current_val'] = $fmt->format($current_val);
				$investment_detail['date'] = $investment['purchase_date'];
				array_push($investment_details, $investment_detail);
        	}

		}
		$grand_total_inv = $fmt->format($grand_total_inv);
		$grand_total_crt = $fmt->format($grand_total_crt);
		return response()->json(['msg'=>'1', 'investments'=>$investment_details, 'investamt'=>$grand_total_inv, 'CurrentVal'=>$grand_total_crt]);
	}


	public function portfolio()
	{
		$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );
		$SchemeDetail = SchemeDetails::get()
                     ->groupBy('scheme_sub');
        $scheme_type = scheme_type::pluck('scheme_sub');
        $total_amount_invested = 0;

        $type_count = count($scheme_type) - 1;
        $scheme_array = [];
        for ($i=0; $i <= $type_count ; $i++) { 
        	$data_set = [];
        	if (isset($SchemeDetail[$scheme_type[$i]])) {
        		$total_amount = 0;
        		array_push($data_set, $scheme_type[$i]);
        		foreach ($SchemeDetail[$scheme_type[$i]] as $value) {
        			$invest_amount = InvestmentDetails::where('scheme_code',$value['scheme_code'])
        							->sum('units');
        			$historic_navs = HistoricNavs::where('scheme_code',$value['scheme_code'])
        							->orderBy('date', 'desc')
        							->first();
        			$sum_amount = $invest_amount * $historic_navs['nav'];
        			$total_amount += $sum_amount;
	          	}
	          	$total_amount = round($total_amount, 2);
	          	$total_amount_invested += $total_amount;
	          	array_push($data_set, $total_amount);
	          	array_push($scheme_array, $data_set);
        	}
        }

        $final_array = [];
        $equity = 0;
        $debt = 0;
        $balanced = 0;
        foreach ($scheme_array as $value) {
        	$data_set_array = [];
        	$avg = ($value[1]/$total_amount_invested) * 100;
        	$avg = round($avg, 2);

        	$type = scheme_type::where('scheme_sub',$value[0])->value('scheme_type');
        	if ($type == 'equity') {
        		$equity += $value[1];
        	}else if ($type == 'balanced') {
        		$balanced += $value[1];
        	}else{
        		$debt += $value[1];
        	}
        	$amount = $fmt->format($value[1]);
        	array_push($data_set_array, $value[0], $amount, $avg);
        	array_push($final_array, $data_set_array);
        }

        $equity_avg = round(($equity/$total_amount_invested)*100, 2);
        $debt_avg = round(($debt/$total_amount_invested)*100, 2);
        $balanced_avg = round(($balanced/$total_amount_invested)*100, 2);

        $equity = $fmt->format($equity);
        $debt = $fmt->format($debt);
        $balanced = $fmt->format($balanced);
        $total_amount_invested = $fmt->format($total_amount_invested);
        
		return view('sector',['sector_array' => $final_array, 'equity' => $equity, 'debt' => $debt, 'balanced' => $balanced, 'balanced_avg' => $balanced_avg, 'debt_avg' => $debt_avg, 'equity_avg' => $equity_avg, 'total' => $total_amount_invested]);
	}

	public function portfolioBygroup($id)
	{
		// dd('hello');
		$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );
		$total_amount_invested = 0;

		$SchemeDetail = SchemeDetails::get()
                     ->groupBy('scheme_sub');
        $scheme_type = scheme_type::pluck('scheme_sub');
		$group_id = $id;
		$group_mem_id = GroupMembers::where('group_id',$group_id)->pluck('id')->toArray();
		$change_date = Carbon::now();
		$change_date = date('Y-m-d',strtotime($change_date));
		$type_count = count($scheme_type) - 1;
        $scheme_array = [];
        for ($i=0; $i <= $type_count ; $i++) { 
        	$data_set = [];
        	if (isset($SchemeDetail[$scheme_type[$i]])) {
        		$total_amount = 0;
        		array_push($data_set, $scheme_type[$i]);
        		foreach ($SchemeDetail[$scheme_type[$i]] as $value) {
        			$invest_amount = InvestmentDetails::whereIn('investor_id',$group_mem_id)
										->where('investor_type','subperson')
										->where('purchase_date','<=',$change_date)
										->where('scheme_code',$value['scheme_code'])
										->sum('units');
        			$historic_navs = HistoricNavs::where('scheme_code',$value['scheme_code'])
        							->orderBy('date', 'desc')
        							->first();
        			$sum_amount = $invest_amount * $historic_navs['nav'];
        			$total_amount += $sum_amount;
	          	}
	          	$total_amount = round($total_amount, 2);
	          	$total_amount_invested += $total_amount;
	          	array_push($data_set, $total_amount);
	          	array_push($scheme_array, $data_set);
        	}
        }

		$final_array = [];
        $equity = 0;
        $debt = 0;
        $balanced = 0;
        foreach ($scheme_array as $value) {
        	$data_set_array = [];
        	$avg = 0;
    		$avg = ($value[1]/$total_amount_invested) * 100;
        	$avg = round($avg, 2);

        	$type = scheme_type::where('scheme_sub',$value[0])->value('scheme_type');
        	if ($type == 'equity') {
        		$equity += $value[1];
        	}else if ($type == 'balanced') {
        		$balanced += $value[1];
        	}else{
        		$debt += $value[1];
        	}
			$amount = $fmt->format($value[1]);
        	array_push($data_set_array, $value[0], $amount, $avg);
        	array_push($final_array, $data_set_array);
        }

        $equity_avg = round(($equity/$total_amount_invested)*100, 2);
        $debt_avg = round(($debt/$total_amount_invested)*100, 2);
        $balanced_avg = round(($balanced/$total_amount_invested)*100, 2);

        $equity = $fmt->format($equity);
        $debt = $fmt->format($debt);
        $balanced = $fmt->format($balanced);
        $total_amount_invested = $fmt->format($total_amount_invested);

        return view('sector',['sector_array' => $final_array, 'equity' => $equity, 'debt' => $debt, 'balanced' => $balanced, 'balanced_avg' => $balanced_avg, 'debt_avg' => $debt_avg, 'equity_avg' => $equity_avg, 'total' => $total_amount_invested]);
	}

	public function portfolioByperson($id)
	{
		$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );
		$total_amount_invested = 0;

		$SchemeDetail = SchemeDetails::get()
                     ->groupBy('scheme_sub');
        $scheme_type = scheme_type::pluck('scheme_sub');
        $change_date = Carbon::now();
		$change_date = date('Y-m-d',strtotime($change_date));
		$type_count = count($scheme_type) - 1;
        $scheme_array = [];
        for ($i=0; $i <= $type_count ; $i++) { 
        	$data_set = [];
        	if (isset($SchemeDetail[$scheme_type[$i]])) {
        		$total_amount = 0;
        		array_push($data_set, $scheme_type[$i]);
        		foreach ($SchemeDetail[$scheme_type[$i]] as $value) {
        			$invest_amount = InvestmentDetails::where('investor_id',$id)
										->where('purchase_date','<=',$change_date)
										->where('scheme_code',$value['scheme_code'])
										->sum('units');
        			$historic_navs = HistoricNavs::where('scheme_code',$value['scheme_code'])
        							->orderBy('date', 'desc')
        							->first();
        			$sum_amount = $invest_amount * $historic_navs['nav'];
        			$total_amount += $sum_amount;
	          	}
	          	$total_amount = round($total_amount, 2);
	          	$total_amount_invested += $total_amount;
	          	array_push($data_set, $total_amount);
	          	array_push($scheme_array, $data_set);
        	}
        }

		$final_array = [];
        $equity = 0;
        $debt = 0;
        $balanced = 0;
        foreach ($scheme_array as $value) {
        	$data_set_array = [];
        	$avg = 0;
    		$avg = ($value[1]/$total_amount_invested) * 100;
        	$avg = round($avg, 2);
        	$type = scheme_type::where('scheme_sub',$value[0])->value('scheme_type');
        	if ($type == 'equity') {
        		$equity += $value[1];
        	}else if ($type == 'balanced') {
        		$balanced += $value[1];
        	}else{
        		$debt += $value[1];
        	}
        	
        	$amount = $fmt->format($value[1]);
        	array_push($data_set_array, $value[0], $amount, $avg);
        	array_push($final_array, $data_set_array);
        }



        $equity_avg = round(($equity/$total_amount_invested)*100, 2);
        $debt_avg = round(($debt/$total_amount_invested)*100, 2);
        $balanced_avg = round(($balanced/$total_amount_invested)*100, 2);

        $equity = $fmt->format($equity);
        $debt = $fmt->format($debt);
        $balanced = $fmt->format($balanced);
        $total_amount_invested = $fmt->format($total_amount_invested);


        return view('sector',['sector_array' => $final_array, 'equity' => $equity, 'debt' => $debt, 'balanced' => $balanced, 'balanced_avg' => $balanced_avg, 'debt_avg' => $debt_avg, 'equity_avg' => $equity_avg, 'total' => $total_amount_invested]);
	}


	public function benchmark($id)
	{
		$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );

		$esensex_total = 0;
		$nifty_total = 0;

		$compound = 0;
		$rbi_current = 0;

		$etotal_current = 0;
		$etotal_inv = 0;

		$dtotal_current = 0;
		$dtotal_inv = 0;

		$btotal_current = 0;
		$btotal_inv = 0;

		$equity = [];
		$debt = [];
		$balanced = [];

		$today = Carbon::now();
		$dateOrder = date('Y-m-d',strtotime($today));
		$investments = InvestmentDetails::where('investor_id',$id)
				->where('investor_type','individual')
				->get();
		$investor_name = Person::where('id',$id)->value('name');


		foreach ($investments as $investment) {
			$data_set = [];
			$Scheme_type = SchemeDetails::where('scheme_code',$investment['scheme_code'])->value('scheme_type');
			$historic_navs = HistoricNavs::where('scheme_code',$investment['scheme_code'])->orderBy('date','aesc')->first();
			if ($Scheme_type == 'equity') {
				$etotal_current += $historic_navs['nav'] * $investment['units'];

				$inv_date = $investment['purchase_date'];
				$inv_amt = $investment['amount_inv'];
				$etotal_inv += $inv_amt;

				$get_sensex = historic_bse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
				$buy_sensex = $inv_amt/$get_sensex['closing_index'];
				$current_sensex = historic_bse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
				$current_sensex_val = $buy_sensex * $current_sensex['closing_index'];
				$current_sensex_val = round($current_sensex_val, 2);
				$esensex_total += $current_sensex_val;

				$get_nifty = historic_nse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
				$buy_nifty = $inv_amt/$get_nifty['closing_index'];
				$current_nifty = historic_nse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
				$current_nifty_val = $buy_nifty * $current_nifty['closing_index'];
				$current_nifty_val = round($current_nifty_val, 2);
				$nifty_total += $current_nifty_val;

				$data_set['name'] = $investor_name;
				$data_set['dop'] = $inv_date;
				$data_set['inv_amt'] = $fmt->format($inv_amt);
				$data_set['if_sensex'] = $fmt->format($current_sensex_val);
				$data_set['if_nifty'] = $fmt->format($current_nifty_val);
				// dd($data_set);
				array_push($equity, $data_set);

			}elseif ($Scheme_type == 'debt') {

				$rbi = $this->rbi_fd;
				// $rbi = array_reverse($rbi);
				$dtotal_current += $historic_navs['nav'] * $investment['units'];
				// dd($rbi);
				$inv_date = $investment['purchase_date'];
				$inv_amt = $investment['amount_inv'];
				$dtotal_inv += $inv_amt;
				$rate = 0;
				foreach ($rbi as $value) {
					if ($inv_date <= $value['date']) {
						break;
					}
					$rate = $value['value'];	
				}

				$rbi_set = ($rate/100) * $inv_amt;

				$to = Carbon::createFromFormat('Y-m-d', $dateOrder);
				$from = Carbon::createFromFormat('Y-m-d', $inv_date);
				$diff_in_days = $to->diffInDays($from);
				$rbi_val = (($rbi_set)/365) * $diff_in_days;
				$rbi_current += $rbi_val + $inv_amt;

				$data_set['name'] = $investor_name;
				$data_set['dop'] = $inv_date;
				$data_set['inv_amt'] = $fmt->format($inv_amt);
				$data_set['rbi_amt'] = $fmt->format(round(($rbi_val + $inv_amt), 2));

				array_push($debt, $data_set);

			}else{
				$btotal_current += $historic_navs['nav'] * $investment['units'];

				$inv_date = $investment['purchase_date'];
				$inv_amt = $investment['amount_inv'];
				$btotal_inv += $inv_amt;

				$get_sensex = historic_bse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
				$buy_sensex = $inv_amt/$get_sensex['closing_index'];
				$current_sensex = historic_bse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
				$current_sensex_val = $buy_sensex * $current_sensex['closing_index'];
				$current_sensex_val = round($current_sensex_val, 2);
				$current_sensex_val = (60/100) * $current_sensex_val;

				$rbi = $this->rbi_fd;
				$rate = 0;
				foreach ($rbi as $value) {
					if ($inv_date <= $value['date']) {
						break;
					}
					$rate = $value['value'];	
				}

				$rbi_set = ($rate/100) * $inv_amt;

				$to = Carbon::createFromFormat('Y-m-d', $dateOrder);
				$from = Carbon::createFromFormat('Y-m-d', $inv_date);
				$diff_in_days = $to->diffInDays($from);
				$rbi_val = (($rbi_set)/365) * $diff_in_days;
				$rbi_val = $rbi_val + $inv_amt;
				$rbi_val = (40/100) * $rbi_val;

				$data_set['name'] = $investor_name;
				$data_set['dop'] = $inv_date;
				$data_set['inv_amt'] = $fmt->format($inv_amt);
				$data_set['compound'] = $fmt->format(round(($rbi_val + $current_sensex_val), 2));

				$compound += $current_sensex_val + $rbi_val;

				array_push($balanced, $data_set);

			}
		}
		// dd($debt);

		$etotal_current = round($etotal_current, 2);
		$dtotal_current = round($dtotal_current, 2);
		$btotal_current = round($btotal_current, 2);
		$sensex_diff = $etotal_current - $esensex_total;
		$nifty_diff = $etotal_current - $nifty_total;
		$rbi_deff = $dtotal_current - $rbi_current;
		$compound_deff = $btotal_current - $compound;
		$sensex_diff = round($sensex_diff, 2);
		$nifty_diff = round($nifty_diff, 2);
		$compound = round($compound, 2);
		$rbi_current = round($rbi_current, 2);
		$rbi_deff = round($rbi_deff, 2);
		$compound_deff = round($compound_deff, 2);

		// dd($etotal_current,$dtotal_current,$btotal_current);
		$etotal_current = $fmt->format($etotal_current);
		$dtotal_current = $fmt->format($dtotal_current);
		$btotal_current = $fmt->format($btotal_current);
		$esensex_total = $fmt->format($esensex_total);
		$nifty_total = $fmt->format($nifty_total);
		$etotal_inv = $fmt->format($etotal_inv);
		$dtotal_inv = $fmt->format($dtotal_inv);
		$btotal_inv = $fmt->format($btotal_inv);
		$rbi_current = $fmt->format($rbi_current);
		$compound = $fmt->format($compound);

		return view('benchmark',['RFtotal_equity' => $etotal_current, 'RFtotal_debt' => $dtotal_current,'RFtotal_balanced' => $btotal_current, 'sensex' => $esensex_total, 'nifty' => $nifty_total, 'equity' => $equity, 'debt' => $debt,'balanced' => $balanced, 'sensex_diff' => $sensex_diff, 'nifty_diff' => $nifty_diff, 'invest_amount_equity' => $etotal_inv, 'invest_amount_debt' => $dtotal_inv, 'invest_amount_balanced' => $btotal_inv, 'rbi_current' => $rbi_current, 'compound' => $compound, 'rbi_deff' => $rbi_deff, 'compound_deff' => $compound_deff]);
	}

	public function bmGroup($id)
	{
		$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );

		$esensex_total = 0;
		$nifty_total = 0;

		$bsensex_total = 0;

		$etotal_current = 0;
		$etotal_inv = 0;

		$dtotal_current = 0;
		$dtotal_inv = 0;

		$btotal_current = 0;
		$btotal_inv = 0;

		$compound = 0;
		$rbi_current = 0;


		$equity = [];
		$debt = [];
		$balanced = [];

		$today = Carbon::now();
		$dateOrder = date('Y-m-d',strtotime($today));
		$investments = InvestmentDetails::where('investor_id',$id)
				->where('investor_type','subperson')
				->get();
		$investor_name = GroupMembers::where('id',$id)->value('member_name');


		foreach ($investments as $investment) {
			$data_set = [];
			$Scheme_type = SchemeDetails::where('scheme_code',$investment['scheme_code'])->value('scheme_type');
			$historic_navs = HistoricNavs::where('scheme_code',$investment['scheme_code'])->orderBy('date','aesc')->first();
			if ($Scheme_type == 'equity') {
				$etotal_current += $historic_navs['nav'] * $investment['units'];

				$inv_date = $investment['purchase_date'];
				$inv_amt = $investment['amount_inv'];
				$etotal_inv += $inv_amt;

				$get_sensex = historic_bse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
				$buy_sensex = $inv_amt/$get_sensex['closing_index'];
				$current_sensex = historic_bse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
				$current_sensex_val = $buy_sensex * $current_sensex['closing_index'];
				$current_sensex_val = round($current_sensex_val, 2);
				$esensex_total += $current_sensex_val;

				$get_nifty = historic_nse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
				$buy_nifty = $inv_amt/$get_nifty['closing_index'];
				$current_nifty = historic_nse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
				$current_nifty_val = $buy_nifty * $current_nifty['closing_index'];
				$current_nifty_val = round($current_nifty_val, 2);
				$nifty_total += $current_nifty_val;

				$data_set['name'] = $investor_name;
				$data_set['dop'] = $inv_date;
				$data_set['inv_amt'] = $fmt->format($inv_amt);
				$data_set['if_sensex'] = $fmt->format($current_sensex_val);
				$data_set['if_nifty'] = $fmt->format($current_nifty_val);
				// dd($data_set);
				array_push($equity, $data_set);

			}elseif ($Scheme_type == 'debt') {

				$rbi = $this->rbi_fd;
				$rbi = array_reverse($rbi);
				$dtotal_current += $historic_navs['nav'] * $investment['units'];

				$inv_date = $investment['purchase_date'];
				$inv_amt = $investment['amount_inv'];
				$dtotal_inv += $inv_amt;

				$rbi_set = 0;
				foreach ($rbi as $value) {
					if ($inv_date <= $value['date']) {
						$temp = $value['value'];
						$rbi_set = ($temp/100) * $inv_amt;
						break;
					}
				}


				$to = Carbon::createFromFormat('Y-m-d', $dateOrder);
				$from = Carbon::createFromFormat('Y-m-d', $inv_date);
				$diff_in_days = $to->diffInDays($from);
				$rbi_val = (($rbi_set)/365) * $diff_in_days;
				$rbi_current += $rbi_val + $inv_amt;

				$data_set['name'] = $investor_name;
				$data_set['dop'] = $inv_date;
				$data_set['inv_amt'] = $fmt->format($inv_amt);
				$data_set['rbi_amt'] = $fmt->format(round(($rbi_val + $inv_amt), 2));

				array_push($debt, $data_set);

			}else{
				$btotal_current += $historic_navs['nav'] * $investment['units'];

				$inv_date = $investment['purchase_date'];
				$inv_amt = $investment['amount_inv'];
				$btotal_inv += $inv_amt;

				$get_sensex = historic_bse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
				$buy_sensex = $inv_amt/$get_sensex['closing_index'];
				$current_sensex = historic_bse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
				$current_sensex_val = $buy_sensex * $current_sensex['closing_index'];
				$current_sensex_val = round($current_sensex_val, 2);
				$bsensex_total += $current_sensex_val;
				$current_sensex_val = (60/100) * $current_sensex_val;

				$rbi = $this->rbi_fd;
				$rate = 0;
				foreach ($rbi as $value) {
					if ($inv_date <= $value['date']) {
						break;
					}
					$rate = $value['value'];	
				}

				$rbi_set = ($rate/100) * $inv_amt;

				$to = Carbon::createFromFormat('Y-m-d', $dateOrder);
				$from = Carbon::createFromFormat('Y-m-d', $inv_date);
				$diff_in_days = $to->diffInDays($from);
				$rbi_val = (($rbi_set)/365) * $diff_in_days;
				$rbi_val = $rbi_val + $inv_amt;
				$rbi_val = (40/100) * $rbi_val;

				$data_set['name'] = $investor_name;
				$data_set['dop'] = $inv_date;
				$data_set['inv_amt'] = $fmt->format($inv_amt);
				$data_set['compound'] = $fmt->format(round(($rbi_val + $current_sensex_val), 2));

				$compound += $current_sensex_val + $rbi_val;

				array_push($balanced, $data_set);

			}
		}

		$etotal_current = round($etotal_current, 2);
		$dtotal_current = round($dtotal_current, 2);
		$btotal_current = round($btotal_current, 2);
		$sensex_diff = $etotal_current - $esensex_total;
		$nifty_diff = $etotal_current - $nifty_total;
		$rbi_deff = $dtotal_current - $rbi_current;
		$compound_deff = $btotal_current - $compound;
		$sensex_diff = round($sensex_diff, 2);
		$nifty_diff = round($nifty_diff, 2);
		$compound = round($compound, 2);
		$rbi_current = round($rbi_current, 2);
		$rbi_deff = round($rbi_deff, 2);
		$compound_deff = round($compound_deff, 2);

		$etotal_current = $fmt->format($etotal_current);
		$dtotal_current = $fmt->format($dtotal_current);
		$btotal_current = $fmt->format($btotal_current);
		$esensex_total = $fmt->format($esensex_total);
		$nifty_total = $fmt->format($nifty_total);
		$etotal_inv = $fmt->format($etotal_inv);
		$dtotal_inv = $fmt->format($dtotal_inv);
		$btotal_inv = $fmt->format($btotal_inv);
		$rbi_current = $fmt->format($rbi_current);
		$compound = $fmt->format($compound);

		return view('benchmark',['RFtotal_equity' => $etotal_current, 'RFtotal_debt' => $dtotal_current,'RFtotal_balanced' => $btotal_current, 'sensex' => $esensex_total, 'nifty' => $nifty_total, 'equity' => $equity, 'debt' => $debt,'balanced' => $balanced, 'sensex_diff' => $sensex_diff, 'nifty_diff' => $nifty_diff, 'invest_amount_equity' => $etotal_inv, 'invest_amount_debt' => $dtotal_inv, 'invest_amount_balanced' => $btotal_inv, 'rbi_current' => $rbi_current, 'compound' => $compound, 'rbi_deff' => $rbi_deff, 'compound_deff' => $compound_deff]);
	}

	public function bmCIS($id)
	{
		$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );

		$esensex_total = 0;
		$nifty_total = 0;

		$bsensex_total = 0;

		$etotal_current = 0;
		$etotal_inv = 0;

		$dtotal_current = 0;
		$dtotal_inv = 0;

		$btotal_current = 0;
		$btotal_inv = 0;

		$compound = 0;
		$rbi_current = 0;

		$equity = [];
		$debt = [];
		$balanced = [];

		$today = Carbon::now();
		$dateOrder = date('Y-m-d',strtotime($today));

		$group_mem_id = GroupMembers::where('group_id',$id)->pluck('id')->toArray();

		$investment_detail = InvestmentDetails::whereIn('investor_id',$group_mem_id)
						->where('investor_type','subperson')
						->get();

		foreach ($investment_detail as $investment) {
			$investor_name = GroupMembers::where('id',$investment['investor_id'])->value('member_name');
			$data_set = [];
			$Scheme_type = SchemeDetails::where('scheme_code',$investment['scheme_code'])->value('scheme_type');
			$historic_navs = HistoricNavs::where('scheme_code',$investment['scheme_code'])->orderBy('date','aesc')->first();
			if ($Scheme_type == 'equity') {
				$etotal_current += $historic_navs['nav'] * $investment['units'];

				$inv_date = $investment['purchase_date'];
				$inv_amt = $investment['amount_inv'];
				$etotal_inv += $inv_amt;

				$get_sensex = historic_bse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
				$buy_sensex = $inv_amt/$get_sensex['closing_index'];
				$current_sensex = historic_bse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
				$current_sensex_val = $buy_sensex * $current_sensex['closing_index'];
				$current_sensex_val = round($current_sensex_val, 2);
				$esensex_total += $current_sensex_val;

				$get_nifty = historic_nse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
				$buy_nifty = $inv_amt/$get_nifty['closing_index'];
				$current_nifty = historic_nse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
				$current_nifty_val = $buy_nifty * $current_nifty['closing_index'];
				$current_nifty_val = round($current_nifty_val, 2);
				$nifty_total += $current_nifty_val;

				$data_set['name'] = $investor_name;
				$data_set['dop'] = $inv_date;
				$data_set['inv_amt'] = $fmt->format($inv_amt);
				$data_set['if_sensex'] = $fmt->format($current_sensex_val);
				$data_set['if_nifty'] = $fmt->format($current_nifty_val);
				// dd($data_set);
				array_push($equity, $data_set);

			}elseif ($Scheme_type == 'debt') {
				
				$rbi = $this->rbi_fd;
				// $rbi = array_reverse($rbi);
				$dtotal_current += $historic_navs['nav'] * $investment['units'];
				// dd($rbi);
				$inv_date = $investment['purchase_date'];
				$inv_amt = $investment['amount_inv'];
				$dtotal_inv += $inv_amt;
				$rate = 0;
				foreach ($rbi as $value) {
					if ($inv_date <= $value['date']) {
						break;
					}
					$rate = $value['value'];	
				}

				$rbi_set = ($rate/100) * $inv_amt;

				$to = Carbon::createFromFormat('Y-m-d', $dateOrder);
				$from = Carbon::createFromFormat('Y-m-d', $inv_date);
				$diff_in_days = $to->diffInDays($from);
				$rbi_val = (($rbi_set)/365) * $diff_in_days;
				$rbi_current += $rbi_val + $inv_amt;

				$data_set['name'] = $investor_name;
				$data_set['dop'] = $inv_date;
				$data_set['inv_amt'] = $fmt->format($inv_amt);
				$data_set['rbi_amt'] = $fmt->format(round(($rbi_val + $inv_amt), 2));

				array_push($debt, $data_set);

			}else{
				$btotal_current += $historic_navs['nav'] * $investment['units'];

				$inv_date = $investment['purchase_date'];
				$inv_amt = $investment['amount_inv'];
				$btotal_inv += $inv_amt;

				$get_sensex = historic_bse::where('date','<=',$inv_date)->orderBy('date','desc')->first();
				$buy_sensex = $inv_amt/$get_sensex['closing_index'];
				$current_sensex = historic_bse::where('date','<=',$dateOrder)->orderBy('date','desc')->first();
				$current_sensex_val = $buy_sensex * $current_sensex['closing_index'];
				$current_sensex_val = round($current_sensex_val, 2);
				$bsensex_total += $current_sensex_val;
				$current_sensex_val = (60/100) * $current_sensex_val;

				$rbi = $this->rbi_fd;
				$rate = 0;
				foreach ($rbi as $value) {
					if ($inv_date <= $value['date']) {
						break;
					}
					$rate = $value['value'];	
				}

				$rbi_set = ($rate/100) * $inv_amt;

				$to = Carbon::createFromFormat('Y-m-d', $dateOrder);
				$from = Carbon::createFromFormat('Y-m-d', $inv_date);
				$diff_in_days = $to->diffInDays($from);
				$rbi_val = (($rbi_set)/365) * $diff_in_days;
				$rbi_val = $rbi_val + $inv_amt;
				$rbi_val = (40/100) * $rbi_val;

				$data_set['name'] = $investor_name;
				$data_set['dop'] = $inv_date;
				$data_set['inv_amt'] = $fmt->format($inv_amt);
				$data_set['compound'] = $fmt->format(round(($rbi_val + $current_sensex_val), 2));

				$compound += $current_sensex_val + $rbi_val;

				array_push($balanced, $data_set);

			}
		}
		// dd($equity);
		$etotal_current = round($etotal_current, 2);
		$dtotal_current = round($dtotal_current, 2);
		$btotal_current = round($btotal_current, 2);
		$sensex_diff = $etotal_current - $esensex_total;
		$nifty_diff = $etotal_current - $nifty_total;
		$rbi_deff = $dtotal_current - $rbi_current;
		$compound_deff = $btotal_current - $compound;
		$sensex_diff = round($sensex_diff, 2);
		$nifty_diff = round($nifty_diff, 2);
		$compound = round($compound, 2);
		$rbi_current = round($rbi_current, 2);
		$rbi_deff = round($rbi_deff, 2);
		$compound_deff = round($compound_deff, 2);

		$etotal_current = $fmt->format($etotal_current);
		$dtotal_current = $fmt->format($dtotal_current);
		$btotal_current = $fmt->format($btotal_current);
		$esensex_total = $fmt->format($esensex_total);
		$nifty_total = $fmt->format($nifty_total);
		$etotal_inv = $fmt->format($etotal_inv);
		$dtotal_inv = $fmt->format($dtotal_inv);
		$btotal_inv = $fmt->format($btotal_inv);
		$rbi_current = $fmt->format($rbi_current);
		$compound = $fmt->format($compound);

		return view('benchmark',['RFtotal_equity' => $etotal_current, 'RFtotal_debt' => $dtotal_current,'RFtotal_balanced' => $btotal_current, 'sensex' => $esensex_total, 'nifty' => $nifty_total, 'equity' => $equity, 'debt' => $debt,'balanced' => $balanced, 'sensex_diff' => $sensex_diff, 'nifty_diff' => $nifty_diff, 'invest_amount_equity' => $etotal_inv, 'invest_amount_debt' => $dtotal_inv, 'invest_amount_balanced' => $btotal_inv, 'rbi_current' => $rbi_current, 'compound' => $compound, 'rbi_deff' => $rbi_deff, 'compound_deff' => $compound_deff]);
	}

	public function sectorPDF()
	{
		$fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );
		$SchemeDetail = SchemeDetails::get()
                     ->groupBy('scheme_sub');
        $scheme_type = scheme_type::pluck('scheme_sub');
        $total_amount_invested = 0;

        $type_count = count($scheme_type) - 1;
        $scheme_array = [];
        for ($i=0; $i <= $type_count ; $i++) { 
        	$data_set = [];
        	if (isset($SchemeDetail[$scheme_type[$i]])) {
        		$total_amount = 0;
        		array_push($data_set, $scheme_type[$i]);
        		foreach ($SchemeDetail[$scheme_type[$i]] as $value) {
        			$invest_amount = InvestmentDetails::where('scheme_code',$value['scheme_code'])
        							->sum('units');
        			$historic_navs = HistoricNavs::where('scheme_code',$value['scheme_code'])
        							->orderBy('date', 'desc')
        							->first();
        			$sum_amount = $invest_amount * $historic_navs['nav'];
        			$total_amount += $sum_amount;
	          	}
	          	$total_amount = round($total_amount, 2);
	          	$total_amount_invested += $total_amount;
	          	array_push($data_set, $total_amount);
	          	array_push($scheme_array, $data_set);
        	}
        }

        $final_array = [];
        $equity = 0;
        $debt = 0;
        $balanced = 0;
        foreach ($scheme_array as $value) {
        	$data_set_array = [];
        	$avg = ($value[1]/$total_amount_invested) * 100;
        	$avg = round($avg, 2);

        	$type = scheme_type::where('scheme_sub',$value[0])->value('scheme_type');
        	if ($type == 'equity') {
        		$equity += $value[1];
        	}else if ($type == 'balanced') {
        		$balanced += $value[1];
        	}else{
        		$debt += $value[1];
        	}
        	$amount = $fmt->format($value[1]);
        	array_push($data_set_array, $value[0], $amount, $avg);
        	array_push($final_array, $data_set_array);
        }

        $equity_avg = round(($equity/$total_amount_invested)*100, 2);
        $debt_avg = round(($debt/$total_amount_invested)*100, 2);
        $balanced_avg = round(($balanced/$total_amount_invested)*100, 2);

        $equity = $fmt->format($equity);
        $debt = $fmt->format($debt);
        $balanced = $fmt->format($balanced);
        $total_amount_invested = $fmt->format($total_amount_invested);

		// $pdf = PDF::loadView('sectorpdf',['sector_array' => $final_array, 'equity' => $equity, 'debt' => $debt, 'balanced' => $balanced, 'balanced_avg' => $balanced_avg, 'debt_avg' => $debt_avg, 'equity_avg' => $equity_avg, 'total' => $total_amount_invested])->setPaper('a4', 'landscape')->setWarnings(false);
		// return $pdf->download('sectorpdf.pdf');
		return view('sectorpdf',['sector_array' => $final_array, 'equity' => $equity, 'debt' => $debt, 'balanced' => $balanced, 'balanced_avg' => $balanced_avg, 'debt_avg' => $debt_avg, 'equity_avg' => $equity_avg, 'total' => $total_amount_invested]);
	}

	public function getNotification()
	{
		$investments = InvestmentDetails::orderBy('created_at','desc')->paginate(5);
		$final_array = [];
		foreach ($investments as $value) {
			$dataset = [];
			if ($value['investor_type'] == "individual") {
				$name = Person::where('id',$value['investor_id'])->value('name');
				$dataset['name'] = $name;
			}else{
				$name = GroupMembers::where('id',$value['investor_id'])->value('member_name');
				$dataset['name'] = $name;
			}
			$dateFrom =  $value['updated_at'];
			$dateFrom->setTimezone(new DateTimeZone('Asia/Kolkata'));
			$IST = $dateFrom->format('d-m-Y H:i');
			$dataset['fundname'] = $value['scheme_name'];
			$dataset['amount']  = $value['amount_inv'];
			$dataset['date'] = $IST;
			$dataset['status'] = $value['is_notseen'];
			array_push($final_array, $dataset);
		}
		// dd($final_array);
		InvestmentDetails::where('is_notseen',1)
            ->update(['is_notseen' => 0]);
		return response()->json(['data' => $final_array]);
	}

	public function getTickets()
	{
		$tickets = ticketDetails::where('process_state','!=',3)->orderBy('priority','asc')->get()->toArray();
		$count = count($tickets);
		$Group = GroupMembers::all()->pluck('member_name')->toArray();
		$persons = Person::where('user_id','>',0)->pluck('name')->toArray();
		$names = array_merge($Group,$persons);

		// dd($tickets);

		return view('tickets',['count' => $count, 'names' => $names, 'tickets' => $tickets, 'val' => 0]);
	}

	public function addTicket(Request $request)
	{
		// if ($request['priority'] != 0) {
		// 	$count = ticketDetails::where('priority', $request['priority'])->get()->count();
		// 	if ($count > 0) {
		// 		return response()->json(['msg' => 2, 'info' => 'Priority alrady taken']);
		// 	}
		// }

		$date = date('Y-m-d',strtotime($request['etc']));
		$insert_ticket = ticketDetails::insert([
			'investor_name'=>$request['inv_name'],
			'priority'=>$request['priority'],
			'etc'=>$date,
			'description'=>$request['description'],
			'process_state'=>2 //2 in progress
		]);
		if ($insert_ticket) {
			return response()->json(['msg' => 1, 'info' => 'Success']);
		}
	}

	public function getTicketData(Request $request)
	{
		$data = ticketDetails::where('id',$request['id'])->get()->toArray();
		$data2 = ['current' => $data[0]['current_status'], 'status' => $data[0]['process_state']];
		return response()->json(['msg' => 1, 'data' => $data2]);
	}

	public function editTicket(Request $request)
	{
		if ($request['status'] == 3) {
		$update = ticketDetails::where('id',$request['id'])->update(['current_status'=>$request['current_status'], 'process_state'=>$request['status'], 'priority' => 0]);

		if ($update) {
			return response()->json(['msg'=>1]);
		}else{
			return response()->json(['msg'=>0]);
		}
		}else{
		$update = ticketDetails::where('id',$request['id'])->update(['current_status'=>$request['current_status'], 'process_state'=>$request['status']]);
		if ($update) {
			return response()->json(['msg'=>1]);
		}else{
			return response()->json(['msg'=>0]);
		}
		}

	}

	public function changePriority(Request $request)
	{
		set_time_limit(600);
		// $old = (int)$request['old'];
		// $new = (int)$request['new'];
		// $id = ticketDetails::where('priority',$old)->value('id');

		// $data = []; 
		// $val['old'] = $id;
		// $val['new'] = $new;
		// array_push($data, $val);
		// if ($old > $new) {
		// 	for($i=$new; $i < $old; $i++) {
		// 		$val['old'] = ticketDetails::where('priority',$i)->value('id');
		// 		$val['new'] = $i+1;
		// 		array_push($data, $val);
		// 	}
		// }else{
		// 	for ($i=$new; $i > $old ; $i--) {
		// 		$val['old'] = ticketDetails::where('priority',$i)->value('id');
		// 		$val['new'] = $i-1;
		// 		array_push($data, $val);
		// 	}
		// 	for ($i=$new; $i < $old; $i++) {
		// 		$val['old'] = ticketDetails::where('priority',$i)->value('id');
		// 		$val['new'] = $i+1;
		// 		array_push($data, $val);
		// 	}
		// }
		// foreach ($data as $value) {
			ticketDetails::where('id',$request['id'])->update(['priority'=>$request['new']]);
		// }

		// return response()->json(['msg' => 1]);
	}

	public function getTicketsFilter($id){
		$tickets = ticketDetails::where('process_state',$id)->orderBy('priority','asc')->get()->toArray();
		$count = count($tickets);
		$Group = GroupMembers::all()->pluck('member_name')->toArray();
		$persons = Person::where('user_id','>',0)->pluck('name')->toArray();
		$names = array_merge($Group,$persons);
		return view('tickets',['count' => $count, 'names' => $names, 'tickets' => $tickets, 'val' => $id]);
	}

	public function changeDate(Request $request)
	{
		$newdate = date('Y-m-d', strtotime($request['date']));
		$update = ticketDetails::where('id',$request['id'])->update(['etc'=>$newdate]);
		if ($update) {
			return response()->json(['msg'=>1]);
		}else{
			return response()->json(['msg'=>0]);
		}
	}

	public function deleteTicket(Request $request)
	{

		$delete_ticket = ticketDetails::where('id',$request['id'])->delete();

		if ($delete_ticket) {
			return response()->json(['msg' => 1]);
		}else{
			return response()->json(['msg' => 0]);
		}
	}

	public function exportForm()
	{
		// $form = $_SERVER['DOCUMENT_ROOT'].'/test.pdf';
  //   	if (file_exists($form)) {
		// 	unlink($form);
  //   	}
  //  		$pdf = PDF::loadView('exportform')->setPaper('a4', 'portrait')->setWarnings(false)->save('test.pdf');
  //  		// dd('hello');

    	$form = $_SERVER['DOCUMENT_ROOT'].'/Form.zip';
    	if (file_exists($form)) {
			unlink($form);
    	}
		// Get Group Info
		$groups = Group::all()->toArray();
		$group_members = [];
		foreach ($groups as $value) {
			$group_member = GroupMembers::where('group_id',$value['id'])->get()->toArray();
			foreach ($group_member as $value) {
				array_push($group_members, $value);
			}
		}
		// Get Individual Info
		$individual = Person::where('user_id',1)->get()->toArray();

		$collection_group = [];
		$count = 0;
		foreach ($group_members as $value) {
			$investments = InvestmentDetails::where('investor_id',$value['id'])->where('investor_type','subperson')->get()->toArray();
			foreach ($investments as $data) {
				$scheme_info = SchemeDetails::where('scheme_code',$data['scheme_code'])->get()->toArray();
				$scheme_types = $scheme_info[0]['scheme_type'];
				if ($scheme_types == 'equity' || $scheme_types == 'balanced') {
					$collection_group[$count]['investor_name'] = $value['member_name'];
					$collection_group[$count]['investor_pan'] = $value['member_pan'];
					$collection_group[$count]['scheme_name'] = $scheme_info[0]['scheme_name'];
					$collection_group[$count]['folio_number'] = $data['folio_number'];
					$collection_group[$count]['units'] = round($data['units'], 4);
					$collection_group[$count]['amc'] = $scheme_info[0]['amc_name'];
					if ($data['option_type'] == 'Monthly Dividend') {
						$collection_group[$count]['option'] = 'Monthly Dividend';
					}else{
						$collection_group[$count]['option'] = 'Growth';
					}
				}
				$count += 1;
			}
		}

		$collection_inv = [];
		$count = 0;
		foreach ($individual as $inv) {
			$investment = InvestmentDetails::where('investor_id',$inv['id'])->where('investor_type','individual')->get()->toArray();
			foreach ($investment as $myInv) {
				$scheme_info = SchemeDetails::where('scheme_code',$myInv['scheme_code'])->get()->toArray();
				$scheme_types = $scheme_info[0]['scheme_type'];
				if ($scheme_types == 'equity' || $scheme_types == 'balanced') {
					if ($myInv['option_type'] == 'Monthly Dividend') {
						$collection_inv[$count]['option'] = 'Monthly Dividend';
					}else{
						$collection_inv[$count]['option'] = 'Growth';
					}
					$collection_inv[$count]['investor_name'] = $inv['name'];
					$collection_inv[$count]['investor_pan'] = $inv['person_pan'];
					$collection_inv[$count]['scheme_name'] = $scheme_info[0]['scheme_name'];
					$collection_inv[$count]['folio_number'] = $myInv['folio_number'];
					$collection_inv[$count]['units'] = round($myInv['units'], 4);
					$collection_inv[$count]['amc'] = $scheme_info[0]['amc_name'];
				}
				$count += 1;
			}
		}
		
		$finalData = array_merge($collection_group,$collection_inv);

		set_time_limit(60000);
		$dataCount = count($finalData);
		$count = 0;
		$file_names = [];
		foreach ($finalData as $selected) {
			$inv_name = trim($selected['investor_name']);
			$inv_name = substr($inv_name, 0, 3);
			$amc_name = trim($selected['amc']);
			$amc_name = substr($amc_name, 0, 3);
			$file_name = $inv_name.'_'.$amc_name.'_'.$count;
			array_push($file_names, $file_name);
       		$pdf = PDF::loadView('exportform', ['selected' => $selected])->setPaper('a4', 'portrait')->setWarnings(false)->save($file_name.'.pdf');
       		$count += 1;
		}

		$zip = new ZipArchive;
		if ($zip->open('Form.zip', ZipArchive::CREATE) === TRUE)
		{
		   foreach ($file_names as $value) {
			   	$zip->addFile($value.'.pdf');  
		   }
		   
		   $zip->close();
		   foreach ($file_names as $value) {
		    	$path = $_SERVER['DOCUMENT_ROOT'].'/'.$value.'.pdf';
				unlink($path);
		   }
		}

		$file= public_path(). "/Form.zip";

		$headers = array(
		  'Content-Type: application/zip',
		);

		return response()->download($file, 'Form.zip', $headers);

	}



}
