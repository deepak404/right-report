<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\DailyNav;
use App\HistoricNavs;
use Carbon\Carbon;
use App\SchemeDetails;

class GetNavValues extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:nav';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gets the daily Nav value of the funds from the AMFII Website';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $scheme_code = SchemeDetails::get()->pluck('scheme_code');

        //$scheme_code = array("106212","112423","114239","128053","111803","133805","133926",  "104685","100122","118191","131666","100081","100520","118102","100349","112090","105758","103360","113177","101672","104481","107745","103196","101979","100499","101350","101862","102142","103174","103504","106235","112092","112496","112600","112938","117716","123690","101537","112096","107249");
        
        $count = 0;
        // $file = file("http://portal.amfiindia.com/spages//NAV0.txt");
        $file = file("https://www.amfiindia.com/spages/NAVAll.txt");
        $today = Carbon::now();
        $today = $today->toDateString();

        foreach ($scheme_code as $searchthis) {
            foreach ($file as $line) {
                if (strpos($line, $searchthis) !== FALSE) {
                    $line_exp = explode(";", $line);                    
                    $date = date("Y-m-d",strtotime($line_exp[7]));
                    /*\Log::info($date.' : '.$line_exp[0].' : '.$line_exp[4]);
                    $store_nav = new DailyNav();
                    $store_nav->scheme_code = $line_exp[0];
                    $store_nav->nav_value = $line_exp[4];
                    $store_nav->date = $date;
                    $store_nav->save();*/

                    // Storing in historic_navs

                    $historic_navs = new HistoricNavs();
                    $historic_navs->scheme_code = $line_exp[0];
                    $historic_navs->scheme_name = $line_exp[3];
                    $historic_navs->date = $date;
                    $historic_navs->nav = $line_exp[4];
                    $historic_navs->save();


                }
            }               
        }

   
    }
}
