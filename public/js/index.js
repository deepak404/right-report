$(document).ready(function(){
  var add_group_var;
  var remove_group_var;
  var add_person_var;
  var inv_id;
  var investor_type;
  var investor_id;


  $(document).on('click','#sidebar_icon',function(){

        var img_src = $('#sidebar_icon>img').attr('src');
        if (img_src == "icons/sidebar.png") {
          $('#sidebar_icon>img').attr('src','icons/sidebar_closed.png');
          $('#sidebar_wrapper').toggle(250,function(){
            $('#contentbar_wrapper').removeClass('col-lg-9 col-md-9 col-sm-9');
            $('#contentbar_wrapper').addClass('col-lg-12 col-md-12 col-sm-12');
          });
        }

        if (img_src == "icons/sidebar_closed.png") {
          $('#sidebar_icon>img').attr('src','icons/sidebar.png');
          $('#sidebar_wrapper').toggle(250,function(){
            $('#contentbar_wrapper').removeClass('col-lg-12 col-md-12 col-sm-12');
            $('#contentbar_wrapper').addClass('col-lg-9 col-md-9 col-sm-9');            
          });
        }


    /*$('#sidebar_wrapper').toggle(500,function(){
      $('#contentbar_wrapper').removeClass('col-lg-9 col-md-9 col-sm-9');
      $('#contentbar_wrapper').addClass('col-lg-12 col-md-12 col-sm-12');
    });*/






  });


      $('#add_scheme_btn').on('click',function(){
        $('#addSchemeModal').modal('show');
      });



      $('.add_group').on('click',function(){

          var group = '<div class="col-xs-12 padding-lr-zero client_bar">'+
          '<div class="col-xs-8 padding-lr-zero"><div class="col-xs-2"><a href="" class=""><i class="material-icons side_icon">group</i></a></div><div class="col-xs-10"><span class="client_name mont-reg"><input type = "text" class = "group_head" id = "group_head" placeholder = "Enter Group head" name = "group_head"/></span></div></div>'+
          '<div class="col-xs-4 text-center">'+
              '<i class="material-icons side_icon dot_btn dropdown-toggle" data-toggle = "dropdown" >more_horiz</i>'+
                  '<ul class="dropdown-menu sub_person_option">'+
                    '<li><a href="#" id="settings">Add Person</a></li>'+
                    '<li><a href="#" id="settings">Rename</a></li>'+
                    '<li><a href="#" id="logout" class="remove">Remove</a></li>'+
                  '</ul>'+

              '<span><a href="#debttypes" data-toggle = "collapse" class="side_icon_parent"><i class="material-icons side_icon key_right">keyboard_arrow_right</i></a></span>'+                
          '</div>'+
        '</div>'

          $('#sidebar').append(group);
      });


      //works fine till above

      jQuery(document).on('keydown', 'input.group_head', function(ev) {
        if(ev.which === 13) {
         console.log($(this).val());
         var add_group = addGroup($(this).val());
            if (add_group) {
              $(this).parent().parent().parent().parent().remove();
            }
        }
      });


      function addGroup(group_name){
        //console.log(group_name);
        var formData = "group_name= "+group_name;
        //console.log(formData);

        $.ajax({
            type: "POST",
            url: "/add_group",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            async : false,
            data: formData,
            cache: false,
            processData: false,
            success: function(data) {
                //appendgroup(data);
                console.log(data);
                add_group_var = "1";
                $('#sidebar').append('<div class="col-xs-12 padding-lr-zero client_bar">'+
                  '<div class="col-xs-8 padding-lr-zero"><div class="col-xs-2"><a href="#" class=""><i class="material-icons side_icon">group</i></a></div><div class="col-xs-10"><a data-id = "'+data.id+'" data-groupname="'+data.id+'" class="client_name mont-reg group">'+data.name+'</a></div></div>'+
                  '<div class="col-xs-4 text-center">'+
                      '<i class="material-icons side_icon dot_btn dropdown-toggle" data-toggle = "dropdown" >more_horiz</i>'+
                          '<ul class="dropdown-menu sub_person_option">'+
                            '<li><a href="#" class="add_group_person" data-id = "'+data.id+'">Add Person</a></li>'+
                            '<li><a href="#" class="group_rename" data-id = "'+data.id+'">Rename</a></li>'+
                            '<li><a href="#" id="logout" class="remove" data-id = "'+data.id+'">Remove</a></li>'+
                          '</ul>'+

                      '<span><a href="#'+data.id+'" data-toggle = "collapse" class="side_icon_parent"><i class="material-icons side_icon key_right">keyboard_arrow_right</i></a></span>'+                  
                  '</div>'+
              '</div>'+

                '<div class="col-xs-12 padding-lr-zero collapse" id="'+data.id+'">'+

                  '<div class="sub_person_bar col-xs-12">'+
                            '<div class="col-xs-10 cis_div"> '+                 
                                '<a href="#" class="cis" data-groupid = "'+data.id+'">CIS</a>'+
                            '</div>'+
                            '<div class="col-xs-2">'+
                              
                            '</div>'+
                  '</div>'+
                '</div>'



              );

            },
            error: function(xhr, status, error) {
                add_group_var = "0";
            },
        })
        //group_name.stopImmediatePropagation();
        //console.log(add_group_var);
        return add_group_var;
      }


      $(document).on('click','.remove',function(){
        var group_id = $(this).data('id');
        var formData = "group_id= "+group_id;
        console.log(formData);

        $.ajax({
            type: "POST",
            url: "/remove_group",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            async : false,
            data: formData,
            cache: false,
            processData: false,
            success: function(data) {
              var type = "g";
              removePeople(data,type,group_id);
                console.log(data);
            },
            error: function(xhr, status, error) {
                
            },
        })
      })



      /*ADD AND REMOVE PERSON SCRIPT BELOW*/


       $('.add_person').on('click',function(){

        //   console.log("person add called");
        //   var person = '<div class="sub_person_bar col-xs-12">'+
        //           '<div class="col-xs-10">'+                  
        //               '<span href="" class="sub-menu sub-person"><input type = "text" class = "person_name" id = "person" placeholder = "Enter person name" name = "person"/></span>'+
        //           '</div>'+
        //           '<div class="col-xs-2">'+
        //             '<span><a href="" class=""><i class="material-icons side_icon dot_btn">more_horiz</i></a></span>'+
        //           '</div>'+
        // '</div>';

        //   $('#sidebar').append(person);

        var investor_type = 'individual';
        $('#investor_type2').attr('value',investor_type);
        $('#group_id2').attr('value','');
        $('#inv_name').attr('value','');
        $('#pan').attr('value','');
        $('#email_id').attr('value','');
        $('#content_number').attr('value','');
        $('#address').attr('value','');
        $('#type').attr('value','add');
        $('#modal_header2').html('Add User');
        $('#addUserModal').modal('show');        

      });


      jQuery(document).on('keydown', 'input.person_name', function(ev) {
        if(ev.which === 13) {     
             console.log($(this).val());
             var add_person = addPerson($(this).val());
             if (add_person) {
              $('.person_name').parent().parent().parent().remove();
            }
        }
      });


      function addPerson(person_name){
        var formData = "person_name= "+person_name;
        //console.log(group_name);

        $.ajax({
            type: "POST",
            url: "/add_person",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            async : false,
            data: formData,
            cache: false,
            processData: false,
            success: function(data) {
                //appendgroup(data);
                console.log(data);
                add_person_var = "1";
                $('#sidebar').append('<div class="sub_person_bar col-xs-12">'+
                  '<div class="col-xs-10 padding-l-zero">'+ 
                      '<div class="col-xs-2 padding-l-zero">'+
                          '<i class="material-icons">person</i>'+
                        '</div>'+                  
                      '<div class = "col-xs-10 padding-l-zero">'+
                        '<a href="#" class="sub-menu individual ind_per" data-person = "'+data.id+'">'+data.name+'</a>'+
                      '</div>'+
                  '</div>'+
                  '<div class="col-xs-2">'+

                      '<div class="dropdown" id="drop">'+
                      '<i class="material-icons side_icon dropdown-toggle dot_btn sub_person_edit" id="main_menu" data-toggle = "dropdown">more_horiz</i>'+
                          '<ul class="dropdown-menu sub_person_option">'+
                            '<li><a href="#" id="settings" class="person_rename" data-id='+data.id+'>Rename</a></li>'+
                            '<li><a href="#" id="logout" class="person_remove" data-id = '+data.id+'>Remove</a></li>'+
                          '</ul>'+
                      '</div> '+                  
                  '</div>'+
                '</div>');

            },
            error: function(xhr, status, error) {
                add_person_var = "0";
            },
        })

        return add_person_var;
      }

      $(document).on('click','.person_remove',function(){
        var person_id = $(this).data('id');
        var formData = "person_id= "+person_id;

        $.ajax({
            type: "POST",
            url: "/remove_person",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            async : false,
            data: formData,
            cache: false,
            processData: false,
            success: function(data) {
                var type = 'p';
                removePeople(data,type,person_id);
                console.log(data);
            },
            error: function(xhr, status, error) {
                
            },
        })
      })



      /*ADD OR REMOVE PERSON ENDS*/


      /*function removePeople(data,type,person_id){
        console.log(data);
        console.log(type);
        console.log(person_id);

        if (type == "p") {
            console.log("person remove")
            if (data.msg == "1") {
              $(document).find('.ind_per[data-person='+person_id+']').parent().parent().remove();
            }

            else{
              console.log('not found');
            }

        }

        if (type == "g") {
            console.log("group remove");
            if (data.msg == "1") {
              $(document).find('.client_group[data-group='+person_id+']').parent().parent().parent().remove();
            }

            else{
              console.log('not found');
            }

        }
      }*/


      $(document).on('click','.individual',function(){


              //$('#arrow_img').css({'display':'none'});
              $('#client_det').empty().append('<span id="client_parent">'+$(this).text()+'</span>');
              console.log($(this).data('person'));
              $('#add_scheme_btn').attr('data-invid',$(this).data('person'));
              $('#add_scheme_btn').attr('data-invtype','individual');

              $('#ex_investor_id').attr('value',$(this).data('person'));
              $('#ex_investor_type').attr('value','individual');

              var investor_id = $(this).data('person');
              var investor_type = 'individual';
              var date = $('#nav_date').val();
              var formData = 'investor_id=' +investor_id+'&investor_type='+investor_type+'&date='+date;
              console.log(formData);

              $.ajax({
                  type: "POST",
                  url: "/get_investment",
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  async : false,
                  data: formData,
                  cache: false,
                  processData: false,
                  success: function(data) {
                    insertInvestments(data);
                      
                  },
                  error: function(xhr, status, error) {
                      
                  },
              });


      });


      $(document).on('click','.sub-person',function(){

              var group_id = $(this).data('groupid');
              var group_name = $(document).find('a[data-groupname='+group_id+']').text();
              console.log(group_name);

              $('#client_det').empty().append('<span id="client_parent">'+group_name+'</span><span><img src="icons/arrow.png" id="arrow_img" /></span><span id="client_child">'+$(this).text()+'</span>')

              $('#client_parent').text(group_name);
              $('#client_child').text($(this).text());
              

              

              $('#add_scheme_btn').attr('data-invid',$(this).data('subid'));
              $('#add_scheme_btn').attr('data-invtype','subperson');

              $('#ex_investor_id').attr('value',$(this).data('subid'));
              $('#ex_investor_type').attr('value','subperson');

              var investor_id = $(this).data('subid');
              var investor_type = 'subperson';
              var date = $('#nav_date').val();
              var formData = 'investor_id=' +investor_id+'&investor_type='+investor_type+'&date='+date;

              console.log(formData);

              $.ajax({
                  type: "POST",
                  url: "/get_investment",
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  async : false,
                  data: formData,
                  cache: false,
                  processData: false,
                  success: function(data) {
                    console.log(data);
                    insertInvestments(data);
                      
                  },
                  error: function(xhr, status, error) {
                      
                  },
              });


      });


      $(document).on('click','.group',function(){

              $('#client_det').empty().append('<span id="client_parent">'+$(this).text()+'</span>')
              $('#add_scheme_btn').attr('data-invid',$(this).data('id'));
              $('#add_scheme_btn').attr('data-invtype','group');
      });


      $('#add_scheme_btn').on('click',function(){

        var investor_id = $(this).attr('data-invid');
        var investor_type = $(this).attr('data-invtype');
        $('#investor_id').attr('value',investor_id);
        $('#investor_type').attr('value',investor_type);
        $('#addSchemeModal').modal('show');
        console.log(investor_id);
      });

     $(document).on('submit','#add_user_form',function(e){
        var formData = $('#add_user_form').serialize();
        console.log(formData);
        // alert('hello');
        $.ajax({
                  type: "POST",
                  url: "/add_person",
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  data: formData,
                  success: function(data) {
                    console.log(data);
                    if (data.msg == 1) {
                      location.reload();
                    }
                      // addInvestment(data);
                  },
                  error: function(xhr, status, error) {
                      
                  },
              });
        e.preventDefault();
     });

      $(document).on('submit','#add_scheme_form',function(e){
          $('#add_scheme_btn').css({'display':'inline-block'});
           var formData = $('#add_scheme_form').serialize();
           console.log(formData);

            $.ajax({
                  type: "POST",
                  url: "/add_investment",
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  async : false,
                  data: formData,
                  cache: false,
                  processData: false,
                  success: function(data) {
                    console.log(data);
                      addInvestment(data);
                  },
                  error: function(xhr, status, error) {
                      
                  },
              });

           e.preventDefault();

      });

      $('#dop').datepicker();


      function addInvestment(data){
        $('#add_scheme_btn').css({'display':'inline-block'});
        if (data.msg == "1") {
          console.log(data);
          console.log(data.investment.scheme_name);
          var object = data.investment;
          tableInsert();
          var insert = '<tr>'+
            '<td class="scheme_name"><div class = "col-xs-10"><p class="table_data scheme_name_p">'+object.scheme_name+'</p></div><div class = "col-xs-2"><i class="material-icons delete_investment" data-placement = "bottom" data-toggle = "tooltip" title="Delete Investment" data-investor_id = "'+object.investor_id+'" data-invid = "'+object.investment_id+'" data-invtype = "'+object.investor_type+'">delete</i></div></div></td>'+
            '<td><p class="table_data">'+object.scheme_type+'</p></td>'+
            '<td class="dop"><p class="table_data">'+object.purchase_date+'</p></td>'+
            '<td><p class="table_data">'+object.amount_inv+'</p></td>'+
            '<td><p class="table_data">'+object.purchase_nav+'</p></td>'+
            '<td><p class="table_data">'+object.units+'</p></td>'+
            '<td><p class="table_data">'+object.current_nav+'</p></td>'+
            '<td><p class="table_data">'+object.current_m_value+'</p></td>'+
            '<td><p class="table_data">'+object.p_or_loss+'</p></td>'+
            '<td><p class="table_data">'+object.abs_returns+'</p></td>'+
            '<td><p class="table_data">'+object.ann_returns+'</p></td>'+
            '<td><p class="table_data">'+object.folio_number+'</p></td>'+
          '</tr>';
            $('#investment_table_body').append(insert);

                $('#total_amount_inv').text(data.total_amount_inv);
                $('#current_market_value').text(data.total_m_value);
                $('#profit_or_loss').text(data.profit_or_loss);
                $('#absolute_ret_avg').text(data.absolute_returns_avg);

          $('#addSchemeModal').modal('hide');
        }
        if (data.msg == "0") {
          console.log(data);
        }
      }

      function insertInvestments(data){
        $('#xirr').text("XIRR Returns: "+data.xirr_value+"%");
        if (data.sensex_val < 0) {
          $('#sensex-icon').css({'color':'red'});
          $('#sensex').text(data.sensex_val+"%").css({'color':'red'});
        }else{
          $('sensex-icon').css({'color':'green'});
          $('#sensex').text(data.sensex_val+"%").css({'color':'green'});
        }

        if (data.nifty_val < 0) {
          $('#nifty-icon').css({'color':'red'});
          $('#nifty').text(data.nifty_val+"%").css({'color':'red'});
        }else{
          $('#nifty-icon').css({'color':'red'});
          $('#nifty').text(data.nifty_val+"%").css({'color':'green'});
        }

        if (data.smallcap < 0) {
          $('#smallcap').text(data.smallcap+"%").css({'color':'red'});
        }else{
          $('#smallcap').text(data.smallcap+"%").css({'color':'green'});
        }

        if (data.midcap < 0) {
          $('#midcap').text(data.midcap+"%").css({'color':'red'});
        }else{
          $('#midcap').text(data.midcap+"%").css({'color':'green'});
        }

        $('#add_scheme_btn').css({'display':'inline-block'});
          console.log(data);
          tableInsert();
          $('#investment_table_body').empty();

          if (data.msg == "0") {
            $('#total_amount_inv').text('-');
            $('#current_market_value').text('-');
            $('#profit_or_loss').text('-');
            $('#absolute_ret_avg').text('-');
          }

          if (data.msg == '1') {
              $('#enter_pan,#user_pan_no').css({'display':'none'});
              $('#pan_no_text,#edit_container').css({'display':'inline'});

            if (data.pan == '') {
              console.log(data.inv_type);
              console.log(data.inv_id);

              $(document).find('#enter_pan').attr('data-invtype',data.inv_type);
              $(document).find('#enter_pan').attr('data-invid',data.inv_id);
              $('#pan_container').css({'display':"inline"});
              $('#nav_as').css({'display':"inline"});
              $('#pan_no_text,#edit_container').css({'display':"none"});

              $('#enter_pan').css({'display':'inline-block'});
              $('#all-container').css({'display':'block'});


            }
            if (data.pan != '') {
              console.log(data.inv_type);
              console.log(data.inv_id);
              $(document).find('#edit_pan').attr('data-invtype',data.inv_type);
              $(document).find('#edit_pan').attr('data-invid',data.inv_id);
              $('#pan_container').css({'display':'block'});
              $('#user_pan_no').text(data.pan).css({'display':'inline'});
            }

            console.log(data.nav_date);

            $('#show_current_date').text(data.nav_date);
            $('#ex_nav_date').val(data.nav_date);
            
            $(document).find('#nav_date').attr('data-invtype',data.inv_type);
            $(document).find('#nav_date').attr('data-invid',data.inv_id);

            $.each(data.investment_details,function(key,object){
                console.log(object.scheme_name);

                var insert = '<tr>'+
                '<td class="scheme_name"><div class = "col-xs-10"><p class="table_data scheme_name_p">'+object.scheme_name+'</p></div><div class = "col-xs-2"><i class="material-icons delete_investment" data-placement = "bottom" data-toggle = "tooltip" title = "Delete Investment" data-investor_id = "'+object.investor_id+'" data-invid = "'+object.investment_id+'" data-invtype = "'+object.investor_type+'">delete</i></div></div></td>'+
                '<td><p class="table_data">'+object.scheme_type+'</p></td>'+
                '<td class="dop"><p class="table_data">'+object.dop+'</p></td>'+
                '<td><p class="table_data">'+object.amount_inv+'</p></td>'+
                '<td><p class="table_data">'+object.purchase_nav+'</p></td>'+
                '<td><p class="table_data">'+object.units+'</p></td>'+
                '<td><p class="table_data">'+object.current_nav+'</p></td>'+
                '<td><p class="table_data">'+object.current_market_value+'</p></td>'+
                '<td><p class="table_data">'+object.p_or_loss+'</p></td>'+
                '<td><p class="table_data pr-0 col-lg-8">'+object.abs_returns+'</p><i class="material-icons change_investment" data-placement = "bottom" data-invid = "'+object.investment_id+'">edit</i></td>'+
                '<td><p class="table_data">'+object.ann_returns+'</p></td>'+
                '<td><p class="table_data">'+object.folio_number+'</p></td>'+
              '</tr>';
                $('#investment_table_body').append(insert);

                
              });

              console.log(data.total_amount);

                $('#total_amount_inv').text(data.total_amount);
                $('#current_market_value').text(data.total_m_value);
                $('#profit_or_loss').text(data.profit_or_loss);
                $('#absolute_ret_avg').text(data.absolute_return_avg);
          }
      }


      $(document).on('click','.add_group_person',function(){
          
        $('#add_scheme_btn').css({'display':'inline-block'});
        var append_id = $(this).data('id');
        var investor_type = 'subperson';
        $('#investor_type2').attr('value',investor_type);
        $('#group_id2').attr('value',append_id);
        $('#inv_name').attr('value','');
        $('#pan').attr('value','');
        $('#email_id').attr('value','');
        $('#content_number').attr('value','');
        $('#address').attr('value','');
        $('#type').attr('value','add');
        $('#modal_header2').html('Add User');
        $('#addUserModal').modal('show');     


        //   var person = '<div class="sub_person_bar col-xs-12">'+
        //           '<div class="col-xs-10">'+                  
        //               '<span href="#" class="sub-menu sub-person"><input type = "text" class = "sub_person_name" id = "person" data-groupid = '+append_id+' placeholder = "Enter person name" name = "sub_person_name"/></span>'+
        //           '</div>'+
        //           '<div class="col-xs-2">'+
        //             '<div class="dropdown" id="drop">'+
        //               '<i class="material-icons side_icon dropdown-toggle dot_btn sub_person_edit" id="main_menu" data-toggle = "dropdown">more_horiz</i>'+
        //                   '<ul class="dropdown-menu sub_person_option">'+
        //                     '<li><a href="#" id="rename" class="sub_person_rename" data-id = "">Rename</a></li>'+
        //                     '<li><a href="#" id="remove" class="sub_person_remove" data-id = "">Remove</a></li>'+
        //                   '</ul>'+
        //               '</div>'+   
        //           '</div>'+
        // '</div>';

        //   $('#'+append_id).append(person);
        //   //$(document).find('#'+append_id).click();
      
      });



      /*Adding sub person below*/
      jQuery(document).on('keydown', 'input.sub_person_name', function(ev) {
        if(ev.which === 13) {     
             console.log($(this).val());
             var group_id = $(this).data('groupid');
             var add_sub_person = addSubPerson($(this).val(),group_id);
             if (add_sub_person) {
              $('.sub_person_name').parent().parent().parent().remove();
            }
        }
      });


      function addSubPerson(sub_person_name,group_id){
        $('#add_scheme_btn').css({'display':'inline-block'});
        $('#add_scheme_btn').css({'display':'inline-block'});
        var formData = "sub_person_name= "+sub_person_name+"&group_id="+group_id;
        //console.log(group_name);

        $.ajax({
            type: "POST",
            url: "/add_sub_person",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            async : false,
            data: formData,
            cache: false,
            processData: false,
            success: function(data) {
                //appendgroup(data);
                console.log(data);
                add_person_var = "1";
                $('#'+data.group_id).append('<div class="sub_person_bar col-xs-12">'+
                                    '<div class="col-xs-10">'+                  
                                        '<a href="#" class="sub-menu sub-person" data-subid = '+data.id+' data-subperson = '+data.id+'>'+data.member_name+'</a>'+
                                    '</div>'+
                                    '<div class="col-xs-2">'+
                                      '<div class="dropdown" id="drop">'+
                                        '<i class="material-icons side_icon dropdown-toggle dot_btn sub_person_edit" id="main_menu" data-toggle = "dropdown">more_horiz</i>'+
                                            '<ul class="dropdown-menu sub_person_option">'+
                                              '<li><a href="#" id="rename" class="sub_person_rename" data-id = '+data.id+'>Rename</a></li>'+
                                              '<li><a href="#" id="remove" class="sub_person_remove" data-id = '+data.id+'>Remove</a></li>'+
                                            '</ul>'+
                                        '</div>' + 
                                    '</div>'+
                          '</div>');

            },
            error: function(xhr, status, error) {
                add_person_var = "0";
            },
        })

        return add_person_var;
      }
      /*Adding Sub person ends*/



      $(document).on('click','.sub_person_remove',function(){
        $('#add_scheme_btn').css({'display':'inline-block'});
        var sub_person_id = $(this).data('id');
        var formData = "sub_person_id= "+sub_person_id;

        $.ajax({
            type: "POST",
            url: "/remove_sub_person",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            async : false,
            data: formData,
            cache: false,
            processData: false,
            success: function(data) {
                var type = 'sp';
                removePeople(data,type,sub_person_id);
                console.log(data);
            },
            error: function(xhr, status, error) {
                
            },
        })
      })



      /*ADD OR REMOVE PERSON ENDS*/


      function removePeople(data,type,person_id){
        $('#add_scheme_btn').css({'display':'inline-block'});
        console.log(data);
        console.log(type);
        console.log(person_id);

        if (type == "p") {
            console.log("person remove")
            if (data.msg == "1") {
              $(document).find('.ind_per[data-person='+person_id+']').parent().parent().parent().remove();
            }

            else{
              console.log('not found');
            }

        }

        if (type == "g") {
            console.log("group remove");
            if (data.msg == "1") {
              $(document).find('.client_name[data-groupname='+person_id+']').parent().parent().parent().remove();
            }

            else{
              console.log('not found');
            }

        }

         if (type == "sp") {
            console.log("Sub Person remove");
            if (data.msg == "1") {
              $(document).find('.sub-person[data-subperson='+person_id+']').parent().parent().remove();
            }

            else{
              console.log('not found');
            }

        }
      }

      $(document).on('click','.cis',function(){
        $('#add_scheme_btn').css({'display':'none'});
        
        var group_id = $(this).data('groupid');

        var group_name = $(document).find('.client_name[data-groupname='+group_id+']').text();
        console.log(group_name);
        $('#client_parent').text(group_name);
        $('#client_child').css({'display':'inline'});
        $('#client_child').text('Consolidate Investment Summary');
        $('#enter_pan').css({'display':'none'});

        //$('#nav_date').attr('data-invid',group_id);
        //$('#nav_date').attr('data-invtype','cis');
        var date = $('#nav_date').val();
        
        var formData = 'group_id='+group_id+'&date='+date;

        $('#pan_no_text,#user_pan_no,#edit_container').css({'display':'none'});
        $('#pan_container').css({'display':'block'});


              //$('#ex_investor_id').attr('value',$(this).data('subid'));
              //$('#ex_investor_type').attr('value','subperson');

          $.ajax({
            type: "POST",
            url: "/cis",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            async : false,
            data: formData,
            cache: false,
            processData: false,
            success: function(data) {
                
                //removePeople(data,type,sub_person_id);
                //console.log(data);
                insertCis(data,group_id);

            },
            error: function(xhr, status, error) {
                
            },
          })
      });

      function insertCis(data,group_id){

        if(data.msg == "1"){
          $('#total_amount_inv').text(data.total_amount_inv);
          $('#current_market_value').text(data.total_m_value);
          $('#profit_or_loss').text(data.profit_or_loss);
          $('#absolute_ret_avg').text(data.absolute_returns_avg);

          $('#show_current_date').text(data.date);
          //$('#nav_date').val(data.date);
          $('#xirr').text("XIRR Returns: "+data.xirr_value+"%");
          console.log(data.date);

          $('#ex_nav_date').val(data.export_date);
          $('#ex_investor_id').attr('value',group_id);
          $('#ex_investor_type').attr('value','cis');

          $('#investment_table').empty();
          $('#investment_table').append('<thead id="inv_table_head">'+
            '<tr>'+
              '<th><p class="table_heading">Scheme Name</p></th>'+
              '<th><p class="table_heading">Amt. Invested</p></th>'+
              '<th><p class="table_heading">Current Mrkt Val.</p></th>'+
              '<th><p class="table_heading">Unrealised Profit/Loss</p></th>'+
              '<th><p class="table_heading">Absolute Returns(%)</p></th>'+
            '</tr>'+
          '</thead>'+
          '<tbody id="investment_table_body">'+
          '<tbody>');

            $.each(data.cis_details,function(key,value){
                console.log(value);
                var inv_amount = inrformat(value.total_amount_inv);
                var cur_val = inrformat(value.current_market_value);
                var profit_loss = inrformat(value.profit_or_loss);

                var append_cis = '<tr>'+
                  '<td class="scheme_name"><p class="table_data scheme_name_p">'+value.scheme_name+'</p></td>'+
                  
                  '<td class="dop"><p class="table_data">'+inv_amount+'</p></td>'+
                  '<td><p class="table_data">'+cur_val+'</p></td>'+
                  '<td><p class="table_data">'+profit_loss+'</p></td>'+
                  '<td><p class="table_data">'+value.absolute_returns+'</p></td>'+
                '</tr>';

                $('#investment_table_body').append(append_cis);
              

            });
        }
        if (data.msg == "0") {
          alert('No Investments Yet');
        }
      }

      function inrformat(data) {
                var x=data;
                x=x.toString();
                var afterPoint = '';
                if(x.indexOf('.') > 0)
                   afterPoint = x.substring(x.indexOf('.'),x.length);
                x = Math.floor(x);
                x=x.toString();
                var lastThree = x.substring(x.length-3);
                var otherNumbers = x.substring(0,x.length-3);
                if(otherNumbers != '')
                    lastThree = ',' + lastThree;
                var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
                return res;
      }

      function tableInsert(){
          $('#inv_table_head').empty();


          var append_head = '<tr>'+
            '<th><p class="table_heading">Scheme Name</p></th>'+
            '<th><p class="table_heading">Scheme Type</p></th>'+
            '<th><p class="table_heading">Date of Purchase</p></th>'+
            '<th><p class="table_heading">Amt. Invested</p></th>'+
            '<th><p class="table_heading">Purchase NAV</p></th>'+
            '<th><p class="table_heading">Units</p></th>'+
            '<th><p class="table_heading">Current Nav.</p></th>'+
            '<th><p class="table_heading">Current Mrkt Val.</p></th>'+
            '<th><p class="table_heading">Unrealised Profit/Loss</p></th>'+
            '<th><p class="table_heading">Absolute Returns(%)</p></th>'+
            '<th><p class="table_heading">Annualised Returns</p></th>'+
            '<th><p class="table_heading">Folio Number</p></th>'+

          '</tr>';

          $('#inv_table_head').append(append_head);
      }




      $(document).on('click','.group_rename',function(){
        $('#add_scheme_btn').css({'display':'inline-block'});
          var group_id = $(this).data('id');
          var current_group_name = $(document).find('.client_name[data-groupname='+group_id+']').text();
          console.log(current_group_name);
          var append_rename = '<input type="text" class="group_name_rename" id="group_name_rename" data-groupid="'+group_id+'" placeholder="Enter person name" name="group_name_rename" value = "'+current_group_name+'">';
          $(document).find('.client_name[data-groupname='+group_id+']').empty().append(append_rename);
          $(document).find('.group_name_rename[data-groupid='+group_id+']').val('').val(current_group_name).focus();
      });

      jQuery(document).on('keydown', 'input.group_name_rename', function(ev) {
        if(ev.which === 13) {
         //console.log($(this).val());
         var new_name = $(this).val();
         var rename_type = "group";
         var group_id = $(this).data('groupid');
         var formData = 'id='+group_id+'&rename_type='+rename_type+'&new_name='+new_name;
         console.log(formData);
          $.ajax({
                  type: "POST",
                  url: "/rename",
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  async : false,
                  data: formData,
                  cache: false,
                  processData: false,
                  success: function(data) {
                    console.log(data);
                    updateName(data,group_id,new_name);
                  },
                  error: function(xhr, status, error) {
                      
                  },
              });

        }
      });


      function updateName(data,group_id,new_name){
        $('#add_scheme_btn').css({'display':'inline-block'});
        if (data.msg == '1') {
         var parent =  $(document).find('.group_name_rename[data-groupid='+group_id+']').parent();
         parent.empty();
         parent.text(new_name);
        }
      }

      $(document).on('click','.person_rename',function(){
        $('#add_scheme_btn').css({'display':'inline-block'});
          var person_id = $(this).data('id');
          var current_person_name = $(document).find('.individual[data-person='+person_id+']').text();
          console.log(current_person_name);
          var append_rename = '<input type="text" class="person_name_rename" id="person_name_rename" data-personid="'+person_id+'" name="person_name_rename">';
          $(document).find('.individual[data-person='+person_id+']').empty().append(append_rename);
          $(document).find('.person_name_rename[data-personid='+person_id+']').val('').val(current_person_name).focus();
      });


      jQuery(document).on('keydown', 'input.person_name_rename', function(ev) {
        if(ev.which === 13) {
         //console.log($(this).val());
         var new_name = $(this).val();
         var rename_type = "individual";
         var person_id = $(this).data('personid');
         var formData = 'id='+person_id+'&rename_type='+rename_type+'&new_name='+new_name;
         console.log(formData);
          $.ajax({
                  type: "POST",
                  url: "/rename",
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  async : false,
                  data: formData,
                  cache: false,
                  processData: false,
                  success: function(data) {
                    console.log(data);
                    updatePersonName(data,person_id,new_name);
                  },
                  error: function(xhr, status, error) {
                      
                  },
              });

        }
      });

      function updatePersonName(data,person_id,new_name){
        $('#add_scheme_btn').css({'display':'inline-block'});
        if (data.msg == '1') {
         var parent =  $(document).find('.person_name_rename[data-personid='+person_id+']').parent();
         parent.empty();
         parent.text(new_name);
        }
      }

      $(document).on('click','.sub_person_rename',function(){
        $('#add_scheme_btn').css({'display':'inline-block'});
          var sub_person_id = $(this).data('id');
          var current_sub_person_name = $(document).find('.sub-person[data-subperson='+sub_person_id+']').text();
          console.log(current_sub_person_name);
          var append_rename = '<input type="text" class="subperson_name_rename" id="subperson_name_rename" data-subpersonid="'+sub_person_id+'" name="subperson_name_rename">';
          $(document).find('.sub-person[data-subperson='+sub_person_id+']').empty().append(append_rename);
          $(document).find('.subperson_name_rename[data-subpersonid='+sub_person_id+']').val('').val(current_sub_person_name).focus();
      });


      jQuery(document).on('keydown', 'input.subperson_name_rename', function(ev) {
        $('#add_scheme_btn').css({'display':'inline-block'});
        if(ev.which === 13) {
         //console.log($(this).val());
         var new_name = $(this).val();
         var rename_type = "subperson";
         var subperson_id = $(this).data('subpersonid');
         var formData = 'id='+subperson_id+'&rename_type='+rename_type+'&new_name='+new_name;
         console.log(formData);
          $.ajax({
                  type: "POST",
                  url: "/rename",
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  async : false,
                  data: formData,
                  cache: false,
                  processData: false,
                  success: function(data) {
                    console.log(data);
                    updateSubPersonName(data,subperson_id,new_name);
                  },
                  error: function(xhr, status, error) {
                      
                  },
              });

        }
      });

      function updateSubPersonName(data,person_id,new_name){
        $('#add_scheme_btn').css({'display':'inline-block'});
        if (data.msg == '1') {
         var parent =  $(document).find('.subperson_name_rename[data-subpersonid='+person_id+']').parent();
         parent.empty();
         parent.text(new_name);
        }
      }

      $(document).on('click','.edit_investment',function(){
        // alert('edit import'+$(this).data('inv_id'));
        var inv_id = $(this).data('inv_id');
        $('#ivest_id').attr('value',inv_id);
        $('#edit_model').modal('show');
      });

      // $(document).on('click','#edit_inv',function(e){
      //   // alert($('#update_invtype').serialize());
      //   var formData = $('#update_invtype').serialize();
      //   console.log(formData);
      //   $.ajax({
      //             type: "POST",
      //             url: "/invUpdate",
      //             headers: {
      //                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      //             },
      //             data: formData,
      //             success: function(data) {
      //               if (data['msg'] == 1) {
      //                 $('#edit_model').modal('hide');
      //                 alert('done!!!!!');
      //               }else {
      //                 $('#edit_model').modal('hide');
      //                 alert('Try again!!!!');
      //               }
      //             },
      //             error: function(xhr, status, error) {
                      
      //             },
      //         });
      //   e.preventDefault();
      // });


      $(document).on('click','.delete_investment',function(){

        $('#adminPasswordModal').modal('show');
        inv_id = $(this).data('invid');
        investor_type = $(this).data('invtype');
        investor_id = $(this).data('investor_id');

         /*var formData = 'inv_id='+inv_id+'&investor_id='+investor_id+'&investor_type='+investor_type;
         console.log(formData);
          $.ajax({
                  type: "POST",
                  url: "/delete_investment",
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  async : false,
                  data: formData,
                  cache: false,
                  processData: false,
                  success: function(data) {
                    if (data.msg == "1") {
                      insertInvestments(data);
                    }
                    else{
                      alert('Cannot Delete NOw');
                    }
                    
                  },
                  error: function(xhr, status, error) {
                      
                  },
              });*/

      });

      $(document).on('click','#check_admin_pass',function(){
          var pass = $('#admin_password').val();
          console.log('testing');
          var formData = 'pass='+pass;
          //console.log(formData);
          $.ajax({
                  type: "POST",
                  url: "/check_pass",
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  async : false,
                  data: formData,
                  cache: false,
                  processData: false,
                  success: function(data) {
                    if (data.msg == "1") {
                      deleteInvestment();
                    }
                    if (data.msg == "0") {
                      alert('Wrong Admin Password');
                      $('#admin_password').val('');
                    }
                    
                  },
                  error: function(xhr, status, error) {
                      
                  },
              });

      });

      function deleteInvestment(){

          var formData = 'inv_id='+inv_id+'&investor_id='+investor_id+'&investor_type='+investor_type;
           //console.log(formData);
            $.ajax({
                    type: "POST",
                    url: "/delete_investment",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    async : false,
                    data: formData,
                    cache: false,
                    processData: false,
                    success: function(data) {
                      if (data.msg == "1") {
                        insertInvestments(data);
                        $('#admin_password').val('');
                        $('#adminPasswordModal').modal('hide');
                      }
                      else{
                        alert('Cannot Delete Now');
                      }
                      
                    },
                    error: function(xhr, status, error) {
                        
                    },
                });

      }


      $(document).on('click', '.side_icon_parent' ,function(){ 


 var current_text = $(this).find('i').text();
 //console.log(current_text);
 if (current_text == 'keyboard_arrow_down') {
    $(this).find('i').text('keyboard_arrow_right');

    $(this).find('i').css({'color':'#7f7f7f'});
    $(this).parent().parent().parent().css({'border-left':'none','transform' : '0.5s'});

    
 }


 if (current_text == 'keyboard_arrow_right') {
    $(this).find('i').text('keyboard_arrow_down');
    $(this).find('i').css({'color':'#00AF64'});
    $(this).parent().parent().parent().css({'border-left':'3px solid #00AF64','transform' : '0.5s'});
 }
   
});

$(document).on('click', '.user_side_icon_parent' ,function(){ 

     var current_text = $(this).find('i').text();

     if (current_text == 'keyboard_arrow_down') {
        
        $(this).parent().parent().parent().css({'border-left':'3px solid #00AF64','transform' : '0.5s'});        
     }


     if (current_text == 'keyboard_arrow_right') {
        
        $(this).parent().parent().parent().css({'border-left':'none','transform' : '0.5s'});
     }
});

$('#settings').on('click',function(){

    $('#settingsModal').modal('show');
});

$('[data-toggle="tooltip"]').tooltip();


      jQuery(document).on('keydown', 'input.pan_no', function(ev) {
        if(ev.which === 13) {     
            //console.log($(this).val());
            var investor_type = $(this).data('invtype');
            var investor_id = $(this).data('invid');
            var pan = $(this).val();
            var formData = '&investor_id='+investor_id+'&investor_type='+investor_type+'&pan='+pan;
            console.log(formData);
              $.ajax({
                  type: "POST",
                  url: "/add_pan",
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  async : false,
                  data: formData,
                  cache: false,
                  processData: false,
                  success: function(data) {
                    if (data.msg == "1") {
                      
                      updatePan(data);
                      //insertInvestments(data);
                    }
                    else{
                      alert('Cannot add PAN Now');
                    }
                    
                  },
                  error: function(xhr, status, error) {
                      
                  },
              });

        }
      });

      function updatePan(data){

        if (data.msg == "1") {

            $('#enter_pan').css({'display':'none'});
            $('#pan_container').css({'display':'block'});
            $('#user_pan_no').text(data.pan).css({'display':'inline'});
            $('#addition_status').text('PAN Added Successfully');
            $('#userStatusModal').modal('show');
        }
        if (data.msg == "0") {
            //alert("PAN addition failed");
            $('#addition_status').text('PAN Update failed');
            $('#userStatusModal').modal('show');
        }
      }

      $(document).on('click','#edit_pan',function(){
            var investor_type = $(this).attr('data-invtype');
            var investor_id = $(this).attr('data-invid');
            var formData = 'investor_id=' +investor_id+'&investor_type='+investor_type;
            $.ajax({
                        type: "POST",
                        url: "/get_person",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        async : false,
                        data: formData,
                        cache: false,
                        processData: false,
                        success: function(data) {
                          if (data['msg'] == 1) {
                            // console.log(data['data']);
                            var mydata = data['data'];
                            editGroupUser(mydata[0]);
                          }else if (data['msg'] == 2) {
                            var mydata = data['data'];
                            editUser(mydata[0]);
                          }

                        },
                        error: function(xhr, status, error) {
                            
                        },
                      })
            

            // $('#pan_container').css({'display':'none'});
            // $(document).find('#enter_pan').attr('data-invtype',investor_type);
            // $(document).find('#enter_pan').attr('data-invid',investor_id);
            // $(document).find('#enter_pan').css({'display':'block'});

            // $(document).find('#enter_pan').val($('#user_pan_no').text());



            // $('#enter_pan').css({'display':'block'});


      });

      function editGroupUser(data){
        $('#investor_type2').attr('value','subperson');
        $('#group_id2').attr('value',data['id']);
        $('#inv_name').attr('value',data['member_name']);
        $('#pan').attr('value',data['member_pan']);
        $('#email_id').attr('value',data['email']);
        $('#content_number').attr('value',data['contact']);
        $('#address').attr('value',data['address']);
        $('#type').attr('value','edit');
        $('#modal_header2').html('Edit User');


        $('#addUserModal').modal('show'); 
        console.log(data);
      }

      function editUser(data){
        $('#investor_type2').attr('value','individual');
        $('#group_id2').attr('value',data['id']);
        $('#inv_name').attr('value',data['name']);
        $('#pan').attr('value',data['person_pan']);
        $('#email_id').attr('value',data['email']);
        $('#content_number').attr('value',data['contact']);
        $('#address').attr('value',data['address']);
        $('#type').attr('value','edit');
        $('#modal_header2').html('Edit User');


        $('#addUserModal').modal('show'); 
        console.log(data);
      }

      $('#nav_date').datepicker({
        dateFormat: 'dd-mm-yy',
        beforeShowDay: $.datepicker.noWeekends,
        changeMonth : true,
        changeYear : true,
      });


      $(document).on('change','#nav_date',function(){
              var investor_id = $(this).attr('data-invid');
              var investor_type = $(this).attr('data-invtype');;
              var date = $(this).val();

              $('#ex_nav_date').val(date);

              if (investor_type == "cis") {

                  var formData = 'group_id='+investor_id+'&date='+date;
                  console.log(formData);
                  $.ajax({
                        type: "POST",
                        url: "/cis",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        async : false,
                        data: formData,
                        cache: false,
                        processData: false,
                        success: function(data) {
                            
                            //removePeople(data,type,sub_person_id);
                            //console.log(data);
                            insertCis(data,investor_id);
                        },
                        error: function(xhr, status, error) {
                            
                        },
                      })

              }

              else{
                var formData = 'investor_id=' +investor_id+'&investor_type='+investor_type+'&date='+date;
                console.log(formData);

                $.ajax({
                    type: "POST",
                    url: "/get_investment",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    async : false,
                    data: formData,
                    cache: false,
                    processData: false,
                    success: function(data) {
                      insertInvestments(data);
                        
                    },
                    error: function(xhr, status, error) {
                        
                    },
                });
              }
      });

      $('#set_date').on('click',function(){
        $('#setDateModal').modal('show');
      });


var today = new Date();
var dd = today.getDate()-1//since we are getting yesterday's NAV;
var mm = today.getMonth()+1; //January is 0!

var yyyy = today.getFullYear();
if(dd<10){
    dd='0'+dd;
} 
if(mm<10){
    mm='0'+mm;
} 
var today = dd+'-'+mm+'-'+yyyy;
$('#nav_date').val(today);


$(document).on('click','.noti-icon',function(){

  var holder_name = $(this).parent().prev().find('a').text();
  $('#query_holder').text(holder_name);

  var investor_id = $(this).parent().parent().eq(0).find('a').data('subid');
  console.log(investor_id);
  var investor_type = "subperson";
  var formData = 'investor_id=' +investor_id+'&investor_type='+investor_type;

  $.ajax({
                  type: "POST",
                  url: "/get_query",
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  async : false,
                  data: formData,
                  cache: false,
                  processData: false,
                  success: function(data) {

                    showQueries(data,investor_id,investor_type);
                    //insertInvestments(data);
                      
                  },
                  error: function(xhr, status, error) {
                      
                  },
              });

    //
});

$(document).on('click','.ind-noti-icon',function(){

  var holder_name = $(this).parent().prev().find('a').text();
  $('#query_holder').text(holder_name);

  var investor_id = $(this).parent().prev().find('a').data('id');
  console.log(investor_id);
  var investor_type = 'ind';
  var formData = 'investor_id=' +investor_id+'&investor_type=' +investor_type;
  console.log(formData);
  var investor_type = "ind";

  $.ajax({
                  type: "POST",
                  url: "/get_query",
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  async : false,
                  data: formData,
                  cache: false,
                  processData: false,
                  success: function(data) {

                    showQueries(data,investor_id,investor_type);
                    //insertInvestments(data);
                      
                  },
                  error: function(xhr, status, error) {
                      
                  },
              });

    //
});

//
/*$(document).on('focus',".query_date", function(){
    $(this).datepicker();
});​*/

function showQueries(data,investor_id,investor_type){
  //console.log(data);
  if (data.msg == "1") {
      console.log(data.query);
      $('#query_body').empty();
      $('#add_query').attr('data-invid',investor_id);
      $('#add_query').attr('data-invtype',investor_type);
      //$('#add_query').attr('data-qid',investor_id);
      $.each(data.query,function(key,value){
          $('#query_body').append(
            '<tr>'+
        
        '<input type= "hidden" name = "investor_id" class= "q_investor_id" data-qid = '+value.id+' value = '+value.investor_id+'>'+
        '<input type= "hidden" name = "query_id" class="q_investment_id" data-qid = '+value.id+' value = '+value.id+'>'+
        '<input type= "hidden" name = "investor_type" class="q_investor_type" data-qid = '+value.id+' value = '+value.id+'>'+
        '<td><p class = "query_string" data-qid = '+value.id+'>'+value.query+'</p></td>'+
        '<td><input type="text" name="query_date" value ='+value.date+' data-qid = '+value.id+' class="query_date" id="query_date"></td>'+
        '<td><select id="query_select" class="query_select" data-qid = '+value.id+'><option>InProcess</option><option>Complete</option></select></td>'+
        '<td><i class="material-icons delete-query" data-invid = "'+value.investor_id+'" data-qid = '+value.id+'>delete</i></td>'+
        '<td><input type="button" name="save_query" data-qid = '+value.id+' data-investorid='+investor_id+' id="save_query" value="save" class="btn btn-primary save-query-btn"></td>'+
      
      '</tr>'
      );

          if (value.status == 1) {
            //var current_value = $(document).find('.query_select[data-qid='+value.id+']').;
            //console.log(current_value);
            $(document).find('.query_select[data-qid='+value.id+']').prop("selectedIndex", 1);
          }
          else{
            $(document).find('.query_select[data-qid='+value.id+']').prop("selectedIndex", 0);
          }
      });
  }
  else{

  }

  $('#getQueryModal').modal('show');
}

$('.query_date').datepicker();
$(document).on('focus','.query_date',function(){
    $(this).datepicker();
});

$(document).on('click','.save-query-btn',function(){

    var q_id = $(this).data('qid');
    var investor_id = $(document).find($('.q_investor_id[data-qid='+q_id+']')).val();
    //var query_id = $(this).data('qid');
    var query = $(document).find($('.query_string[data-qid='+q_id+']')).text();
    var date = $(document).find($('.query_date[data-qid='+q_id+']')).val();
    var status = $(document).find($('.query_select[data-qid='+q_id+'] option:selected')).text();
    var formData = 'query_id='+q_id+'&query='+query+'&date='+date+'&status='+status+'&investor_id='+investor_id;
    console.log(formData);

    $.ajax({
                  type: "POST",
                  url: "/update_query",
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  async : false,
                  data: formData,
                  cache: false,
                  processData: false,
                  success: function(data) {

                    showQueries(data);
                      
                  },
                  error: function(xhr, status, error) {
                      
                  },
              });
})

$(document).on('click','#add_query',function(){
  var investor_id = $(this).data('invid');
  var investor_type = $(this).data('invtype');
  //var query_id = $(this).data('qid');
  var append_this = '<tr id = "new_query_row" data-rowid = '+investor_id+'>'+
        
        
        '<td><input type="text" id="new_query_text" name = "new_query_text"></td>'+
        '<td><input type="text" name="new_query_date" class="query_date" id="new_query_date"></td>'+
        '<td><select id="new_query_select" class="query_select" style = "cursor:not-allowed"><option>InProcess</option><option>Complete</option></select></td>'+
        '<td><i class="material-icons" style = "cursor:not-allowed">delete</i></td>'+
        '<td><input type="button" name="save_query" data-invid = '+investor_id+' data-invtype = '+investor_type+' id="save_new_query" value="save" class="btn btn-primary save-new-query-btn"></td>'+
      
      '</tr>';

      $('#query_body').append(append_this);
}); 

$(document).on('click','.save-new-query-btn',function(){
    var investor_id = $(this).data('invid');
    var investor_type = $(this).data('invtype');

    var query_text = $(document).find('#new_query_text').val();
    var query_date = $(document).find('#new_query_date').val();


    var formData = 'investor_id='+investor_id+'&investor_type='+investor_type+'&query_text='+query_text+'&query_date='+query_date;
    console.log(formData);

    $.ajax({
                  type: "POST",
                  url: "/add_query",
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  async : false,
                  data: formData,
                  cache: false,
                  processData: false,
                  success: function(data) {
                    $(document).find('#new_query_row[data-rowid='+investor_id+']').remove();
                    showQueries(data,investor_id,investor_type);
                    //insertInvestments(data);
                      
                  },
                  error: function(xhr, status, error) {
                      
                  },
              });

    
});


$(document).on('click','.delete-query',function(){

    var investor_id = $(this).data('invid');
    var query_id = $(this).data('qid');


    var formData = 'investor_id='+investor_id+'&query_id='+query_id;
    console.log(formData);

    $.ajax({
                  type: "POST",
                  url: "/delete_query",
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  async : false,
                  data: formData,
                  cache: false,
                  processData: false,
                  success: function(data) {
                    $(document).find('#new_query_row[data-rowid='+investor_id+']').remove();
                    showQueries(data,investor_id);
                    //insertInvestments(data);
                      
                  },
                  error: function(xhr, status, error) {
                      
                  },
              });

    
  });

  $(document).on('click','.portfolio_details',function(){
      window.open('/portfolio', '_blank');
  });
  
  $(document).on('click','.amc_details',function(){
      window.open('/getAmc','_blank');
      // alert('amc');
        // var date = $('#nav_date').val();
        // var formData = 'date='+date;
        //   $.ajax({
        //         type: "POST",
        //         url: "/getAmc",
        //         headers: {
        //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //         },
        //         async : false,
        //         data: formData,
        //         cache: false,
        //         processData: false,
        //         success: function(data) {

        //         },
        //         error: function(xhr, status, error) {
                    
        //         },
        //     });
  });

  $(document).on('click','#export_btn', function(){
    if ($("#export_as").css('display') == 'none') {
        $("#export_as").css("display","block");
      }else{
        $("#export_as").css("display","none");
      }
    // alert($('#export_form').serialize());

  });

  $(document).on('click','#export_as_xls', function(){
        window.open('/getAmc','_blank');
  });

  $(document).on('click','.change_investment',function(){
      var investment_id = $(this).data('invid');
      $('#ivest_id').val(investment_id);
      $('#edit_model').modal('show');
  });

  $(document).on('submit','#update_aum',function(e){
    e.preventDefault();
    var formData = $('#update_aum').serialize();
    $.ajax({
      type:'POST',
      url:'/update_aum',
      data:formData,
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(data) {
        if (data.msg == 1) {
          $('#edit_model').modal('hide');
          alert('AUM Status Updated.');
        }else if (data.msg == 2) {
          console.log('error');
        }
      },
      error: function(xhr, status, error) {
          
      },
    })
  })

});


