$(document).ready(function(){
  // $('#add_scheme_btn').on('click',function(){
  //     $('#addSchemeModal').modal('show');
  // });
	var id = '';
	var old_val = '';

  $('.input-field').keyup(function(){
    var formData = "id="+$(this).data('id')+"&new="+$(this).val();

    $.ajax({
                  type: "POST",
                  url: "/change_priority",
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  async : false,
                  data: formData,
                  cache: false,
                  processData: false,
                  success: function(data) {
                    if (data.msg == 1) {
                      location.reload();
                    }
                  },
                  error: function(xhr, status, error) {
                      
                  },
              });
  });

  $(document).on('click','#sidebar_icon',function(){

        var img_src = $('#sidebar_icon>img').attr('src');
        if (img_src == "icons/sidebar.png") {
          $('#sidebar_icon>img').attr('src','icons/sidebar_closed.png');
          $('#sidebar_wrapper').toggle(250,function(){
            $('#contentbar_wrapper').removeClass('col-lg-9 col-md-9 col-sm-9');
            $('#contentbar_wrapper').addClass('col-lg-12 col-md-12 col-sm-12');
          });
        }

        if (img_src == "icons/sidebar_closed.png") {
          $('#sidebar_icon>img').attr('src','icons/sidebar.png');
          $('#sidebar_wrapper').toggle(250,function(){
            $('#contentbar_wrapper').removeClass('col-lg-12 col-md-12 col-sm-12');
            $('#contentbar_wrapper').addClass('col-lg-9 col-md-9 col-sm-9');            
          });
        }
  });

  $(document).on('click', '#add-ticket', function(){
    $('#addNewTicket').modal('show');
  });

  $('#etc_id').datepicker();
  $('#new_date').datepicker();

  $('.edit_priority').on('click',function(){
  	var values = getTicketData($(this).data('value'));
  });

  $('.change_priority').on('click', function(){
  	$('#id_val').val($(this).data('value'));
  	$('#old_val').val($(this).data('pri'));
	$('#adminPasswordModal').modal('show');
  });

  $('.change_etc').on('click', function(){
  	$('#id_val2').val($(this).data('value'));
	$('#adminPasswordModal2').modal('show');
  });

  $('#edit_ticket_btn').on('click',function(e){
  	e.preventDefault();
	var formData = $('#edit_ticket_form').serialize();

	  	$.ajax({
	                  type: "POST",
	                  url: "/edit_ticket",
	                  headers: {
	                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                  },
	                  async : false,
	                  data: formData,
	                  cache: false,
	                  processData: false,
	                  success: function(data) {
						location.reload();
	                  },
	                  error: function(xhr, status, error) {
	                      
	                  },
              });


  });

  function	getTicketData(val){
  	var formData = "id="+val;

  	$.ajax({
                  type: "POST",
                  url: "/get_ticket_data",
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  async : false,
                  data: formData,
                  cache: false,
                  processData: false,
                  success: function(data) {
                  	if (data.msg == 1) {
                  		var values = data.data;
                  		$('#ticket-id').val( val);
                  		$('#current_value').val(values['current']);
                  		if (values['status'] == null) {
	                  		$('#status_id').val(values['status']);
                  		}else{
	                  		$('#status_id').val(values['status']);
                  		}
					  	$('#editTicket').modal('show');
                  	}
                  },
                  error: function(xhr, status, error) {
                      
                  },
              });
  }

  $('#add_ticket_btn').on('click',function(e){
  	e.preventDefault();
  	var formData = $('#add_new_ticker_form').serialize();

  	$.ajax({
                  type: "POST",
                  url: "/add_ticket",
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  async : false,
                  data: formData,
                  cache: false,
                  processData: false,
                  success: function(data) {
                  	if (data.msg == 1) {
					location.reload();
                  	}else if (data.msg == 2) {
                  		alert(data.info);
                  	}
                  },
                  error: function(xhr, status, error) {
                      
                  },
              });
  });



  $('#check_admin_pass').on('click',function(){
	    id = $('#id_val').val();
	    old_val =$('#old_val').val();
	  	var formData = "pass="+$('#admin_password').val();

  	$.ajax({
                  type: "POST",
                  url: "/admin_check_pass",
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  async : false,
                  data: formData,
                  cache: false,
                  processData: false,
                  success: function(data) {
                  	if (data.msg == 1) {
					$('#adminPasswordModal').modal('hide');
					$('#changePriority').modal('show');
                  	}else if (data.msg == 0) {
                  		alert('Wrong Admin Password');
                  	}
                  },
                  error: function(xhr, status, error) {
                      
                  },
              });
  });


$('#check_admin_pass2').on('click',function(){
	    id = $('#id_val2').val();
	  	var formData = "pass="+$('#admin_password2').val();

  	$.ajax({
                  type: "POST",
                  url: "/admin_check_pass",
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  async : false,
                  data: formData,
                  cache: false,
                  processData: false,
                  success: function(data) {
                  	if (data.msg == 1) {
					$('#adminPasswordModal2').modal('hide');
					$('#changedate').modal('show');
                  	}else if (data.msg == 0) {
                  		alert('Wrong Admin Password');
                  	}
                  },
                  error: function(xhr, status, error) {
                      
                  },
              });
  });

  $('#change_btn').on('click',function(){
	

	var formData = "id="+id+"&old="+old_val+"&new="+$('#priority_change').val();

  	$.ajax({
                  type: "POST",
                  url: "/change_priority",
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  async : false,
                  data: formData,
                  cache: false,
                  processData: false,
                  success: function(data) {
                  	if (data.msg == 1) {
                  		location.reload();
                  	}else if (data.msg == 0) {
                      alert('priority must in number');
                    }
                  },
                  error: function(xhr, status, error) {
                      
                  },
              });
  });


  $('#change_date').on('click',function(){
  	var formData = "id="+id+"&date="+$('#new_date').val();

  	$.ajax({
                  type: "POST",
                  url: "/change_date",
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  async : false,
                  data: formData,
                  cache: false,
                  processData: false,
                  success: function(data) {
                  	if (data.msg == 1) {
                  		location.reload();
                  	}else if (data.msg == 0) {
                  		alert('Wrong Admin Password');
                  	}
                  },
                  error: function(xhr, status, error) {
                      
                  },
              });
  });

  $('.delete_priority').on('click',function(){
  	$('#id_val3').val($(this).data('value'));
	$('#adminPasswordModal3').modal('show');
  });

  $('#check_admin_pass3').on('click',function(){
	    id = $('#id_val3').val();
  		var formData = "pass="+$('#admin_password3').val();

  	$.ajax({
                  type: "POST",
                  url: "/admin_check_pass",
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  async : false,
                  data: formData,
                  cache: false,
                  processData: false,
                  success: function(data) {
                  	if (data.msg == 1) {
                  		deleteTicket()
                  	}else if (data.msg == 0) {
                  		alert('Wrong Admin Password');
                  	}
                  },
                  error: function(xhr, status, error) {
                      
                  },
          });
  });

  function deleteTicket()
  {
  	var formData = "id="+id;

  	$.ajax({
                  type: "POST",
                  url: "/delete_ticket",
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  async : false,
                  data: formData,
                  cache: false,
                  processData: false,
                  success: function(data) {
                  	if (data.msg == 1) {
                  		location.reload();
                  	}
                  },
                  error: function(xhr, status, error) {
                      
                  },
          });
  }

  $('#newuser').on('click',function(){
    $('#ticket-name').empty();     
    $( "#ticket-name" ).append( '<input type="text" class="mont-reg" name="inv_name" id = "inv_name" required placeholder="Investor Name">');
 
  });

});
