$(document).ready(function(){


       	$('#equity-chart').donut();
        $('#debt-chart').donut();
        $('#gold-chart').donut();
        $('#hybrid-chart').donut();

        $('#onetime, #future, #monthly').on('change',function(){

                if ($('#onetime').is(":checked")) {
                  $('.radio').find('#one_img').attr('src','icons/r_en.png');
                  $('.radio').find('#future_img').attr('src','icons/r_dis.png');
                  $('.radio').find('#monthly_img').attr('src','icons/r_dis.png');
                }

                else if ($('#future').is(":checked")) {
                  $('.radio').find('#one_img').attr('src','icons/r_dis.png');
                  $('.radio').find('#future_img').attr('src','icons/r_en.png');
                  $('.radio').find('#monthly_img').attr('src','icons/r_dis.png');
                }

                else if ($('#monthly').is(":checked")) {
                  $('.radio').find('#one_img').attr('src','icons/r_dis.png');
                  $('.radio').find('#future_img').attr('src','icons/r_dis.png');
                  $('.radio').find('#monthly_img').attr('src','icons/r_en.png');
                }

            });


        $('#low-risk, #own, #moderate-risk,#high-risk').on('change',function(){

                if ($('#own').is(":checked")) {
                  $('.port-radio').find('#own-img').attr('src','icons/r_en.png');
                  $('.port-radio').find('#low-img').attr('src','icons/r_dis.png');
                  $('.port-radio').find('#mod-img').attr('src','icons/r_dis.png');
                  $('.port-radio').find('#high-img').attr('src','icons/r_dis.png');

                  $('#custom-image').attr('src','icons/custom_on.png');
                  $('#conser-image').attr('src','icons/conser_off.png');
                  $('#moder-image').attr('src','icons/mod_off.png');
                  $('#aggr-image').attr('src','icons/aggr_off.png');

                  $('#custom-text').css({'color':'#00AF64'});
                  $('#conser-text').css({'color':'#797979'});
                  $('#moder-text').css({'color':'#797979'});
                  $('#aggr-text').css({'color':'#797979'});

                }

                else if ($('#low-risk').is(":checked")) {
                  $('.port-radio').find('#own-img').attr('src','icons/r_dis.png');
                  $('.port-radio').find('#low-img').attr('src','icons/r_en.png');
                  $('.port-radio').find('#mod-img').attr('src','icons/r_dis.png');
                  $('.port-radio').find('#high-img').attr('src','icons/r_dis.png');

                  $('#custom-image').attr('src','icons/custom_off.png');
                  $('#conser-image').attr('src','icons/conser_on.png');
                  $('#moder-image').attr('src','icons/mod_off.png');
                  $('#aggr-image').attr('src','icons/aggr_off.png');

                  $('#custom-text').css({'color':'#797979'});
                  $('#conser-text').css({'color':'#00AF64'});
                  $('#moder-text').css({'color':'#797979'});
                  $('#aggr-text').css({'color':'#797979'});
                  
                }

                else if ($('#moderate-risk').is(":checked")) {
                  $('.port-radio').find('#own-img').attr('src','icons/r_dis.png');
                  $('.port-radio').find('#low-img').attr('src','icons/r_dis.png');
                  $('.port-radio').find('#mod-img').attr('src','icons/r_en.png');
                  $('.port-radio').find('#high-img').attr('src','icons/r_dis.png');

                  $('#custom-image').attr('src','icons/custom_off.png');
                  $('#conser-image').attr('src','icons/conser_off.png');
                  $('#moder-image').attr('src','icons/mod_on.png');
                  $('#aggr-image').attr('src','icons/aggr_off.png');

                  $('#custom-text').css({'color':'#797979'});
                  $('#conser-text').css({'color':'#797979'});
                  $('#moder-text').css({'color':'#00AF64'});
                  $('#aggr-text').css({'color':'#797979'});
                }

                else if ($('#high-risk').is(":checked")) {
                  $('.port-radio').find('#own-img').attr('src','icons/r_dis.png');
                  $('.port-radio').find('#low-img').attr('src','icons/r_dis.png');
                  $('.port-radio').find('#mod-img').attr('src','icons/r_dis.png');
                  $('.port-radio').find('#high-img').attr('src','icons/r_en.png');

                  $('#custom-image').attr('src','icons/custom_off.png');
                  $('#conser-image').attr('src','icons/conser_off.png');
                  $('#moder-image').attr('src','icons/mod_off.png');
                  $('#aggr-image').attr('src','icons/aggr_on.png');

                  $('#custom-text').css({'color':'#797979'});
                  $('#conser-text').css({'color':'#797979'});
                  $('#moder-text').css({'color':'#797979'});
                  $('#aggr-text').css({'color':'#00AF64'});
                }

            });


$(function() {
    if ($('#radio').is(":checked", true)) {
        $('.checkbox-div input[type="checkbox"]').prop('checked', true);
    }

    $('#eq-show-less').on('click', function() {
        $('#equity-body').slideToggle('slow', function() {
            $('#eq-show-less').text($(this).is(':visible') ? "SHOW LESS" : 'SHOW MORE');
        });
    });

    $('#debt-show-less').on('click', function() {
        $('#debt-body').slideToggle('slow', function() {
            $('#debt-show-less').text($(this).is(':visible') ? "SHOW LESS" : 'SHOW MORE');
        });
    });

    $('#elss-show-less').on('click', function() {
        $('#elss-body').slideToggle('slow', function() {
            $('#elss-show-less').text($(this).is(':visible') ? "SHOW LESS" : 'SHOW MORE');
        });
    });

     $('#gold-show-less').on('click', function() {
        $('#gold-body').slideToggle('slow', function() {
            $('#gold-show-less').text($(this).is(':visible') ? "SHOW LESS" : 'SHOW MORE');
        });
    });

})


            $('#inv-icon').on('click',function(){

                $('#invDetailsModal').modal('show');
                $('.navbar').css({'z-index':'0'});
            });

            $('.cancel-inv-link').on('click',function(){
                  //$('#invDetailsModal').modal('hide');
                $('#cancelInvModal').modal('show');

                $('#cancelInvModal').on('shown.bs.modal', function() {
                     $('#invDetailsModal').css({'opacity':'0'});
                     
                  });

                 $('#cancelInvModal').on('hidden.bs.modal', function() {
                     $('#invDetailsModal').css({'opacity':'1'});
                     
                  });
            });


            $('#fut-icon').on('click',function(){
                $('#invSummaryModal').modal('show');
            });

            $('#sche-sel-date,#month-start-date,#month-end-date,#one-sel-date').datepicker({

                inline: true,
      showOtherMonths: true,
      dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
      dateFormat: "dd/M/y",


             beforeShow: function() {
                setTimeout(function(){
                $('.ui-datepicker').css('z-index', 99999999999999);
                $('.ui-datepicker').css('padding', 0);
                //$('.ui-datepicker').css('left', '100px');

                }, 0);
                }


            });


              var checkboxes = $('.checkbox-div').find('input[type=checkbox]');
              var checkboximg = $('.checkbox-div').find('img');
              $('input[name="port_type"]').on('change', function() {

                      var parentDiv=$(this).parents(".checkbox-div:eq(0)");

                      if( $(this).is(':checked') && $(this).attr('id') == 'own') 
                        {
                            checkboxes.prop('checked',false).prop('disabled',false);
                            checkboximg.attr("src","icons/check_dis.png");
                            
                            

                        }
                      if (($(this).is(':checked')) && (($(this).attr('id') == 'low-risk') || ($(this).attr('id') == 'moderate-risk') || ($(this).attr('id') == 'high-risk'))) {

                            checkboxes.prop('disabled',true);
                            checkboximg.attr("src","icons/check_dis.png");
                      }

                });


                  $('.checkbox-div input').on('change', function() {
                      var parentDiv=$(this).parents(".checkbox-div:eq(0)");
                      if($(this).is(":checked")){
                        $(parentDiv).find("img").attr("src","icons/check_en.png");
                      }
                      else{
                      $(parentDiv).find("img").attr("src","icons/check_dis.png");
                      }
                  });


              $('#wd-maintain, #wd-now').on('change',function(){

                if ($('#wd-maintain').is(":checked")) {
                  $('.withdraw-radio-div').find('#wd-maintain-img').attr('src','icons/r_en.png');
                  $('.withdraw-radio-div').find('#wd-now-img').attr('src','icons/r_dis.png');
                  $('.funds-table').css({'opacity':'0.5','pointer-events':'none'});

                    var checkboxes = $('.checkbox-div').find('input[type=checkbox]');
                    var checkboximg = $('.checkbox-div').find('img');
                      checkboxes.prop('checked',false);
                      checkboximg.attr("src","icons/check_dis.png");

                }

                if ($('#wd-now').is(":checked")) {
                  $('.withdraw-radio-div').find('#wd-maintain-img').attr('src','icons/r_dis.png');
                  $('.withdraw-radio-div').find('#wd-now-img').attr('src','icons/r_en.png');
                   $('.funds-table').css({'opacity':'1','pointer-events':'auto'});
                }
              });



});