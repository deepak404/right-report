<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
                DB::table('scheme_details')->insert([

        	/*Below Five are Debt funds -- For Conservative Portfolio*/
            [
            	'scheme_code' => '106212',
            	'scheme_name' => 'SBI Ultra Short Term Debt Fund Growth',

            ],

            [
            	'scheme_code' => '112423',
            	'scheme_name' => 'L&T Ultra Short Term Fund Growth',

            ],

            [
            	'scheme_code' => '114239',
            	'scheme_name' => 'ICICI Prudential Regular Savings Fund Growth',
 
            ],

            [
            	'scheme_code' => '128053',
            	'scheme_name' => 'HDFC Corporate Debt Opportunities Fund Growth',

            ],

            [
            	'scheme_code' => '111803',
            	'scheme_name' => 'Birla Sun Life Medium Term Plan Growth',

            ],

            [
              'scheme_code' => '133805',
              'scheme_name' => 'Kotak Low Duration Fund- Regular Plan Growth',

            ],

            [
              'scheme_code' => '133926',
              'scheme_name' => 'DSP BlackRock Ultra Short Term Fund - Regular Plan - Growth',

            ],


            /*Debt funds Ends -- For Conservative Portfolio*/


            /*Below Four are Balanced funds -- For Moderate Portfolio*/
            [
            	'scheme_code' => '104685',
            	'scheme_name' => 'ICICI Prudential Balanced Advantage Fund Growth',

            ],

            [
            	'scheme_code' => '100122',
            	'scheme_name' => 'HDFC Balanced Fund Growth',

            ],

            [
            	'scheme_code' => '118191',
            	'scheme_name' => 'L&T India Prudence Fund Growth',

            ],

            [
            	'scheme_code' => '131666',
            	'scheme_name' => 'Birla Sun Life Balanced Advantage Fund Growth',

            ],

            [
              'scheme_code' => '100081',
              'scheme_name' => 'DSP BlackRock Balanced Fund Growth',

            ],



            /*Balanced funds Ends -- For Moderate Portfolio*/


            /*Below Eleven are Equity funds -- For Aggressive Portfolio*/
            [
            	'scheme_code' => '100520',
            	'scheme_name' => 'Franklin India Prima Plus Growth',

            ],

            [
            	'scheme_code' => '118102',
            	'scheme_name' => 'L&T India Value Fund-Regular Plan Growth',

            ],

            [
            	'scheme_code' => '100349',
            	'scheme_name' => 'ICICI Prudential Top 100 Fund Growth',

            ],

            [
            	'scheme_code' => '112090',
            	'scheme_name' => 'Kotak Select Focus Fund Growth',

            ],

           /* [
            	'scheme_code' => '105989',
            	'scheme_name' => 'DSP BlackRock Micro Cap Fund Growth',
            	'scheme_type' => 'eq',
            	'scheme_priority' => '6',
            ],*/


            /*[
            	'scheme_code' => '112932',
            	'scheme_name' => 'Mirae Asset Emerging Bluechip Fund Growth',
            	'scheme_type' => 'eq',
            	'scheme_priority' => '7',
            ],*/

            [
            	'scheme_code' => '105758',
            	'scheme_name' => 'HDFC Mid-Cap Opportunities Fund Growth',

            ],

            [
            	'scheme_code' => '103360',
            	'scheme_name' => 'Franklin India Smaller Companies Fund Growth',

            ],

             [
              'scheme_code' => '113177',
              'scheme_name' => 'Reliance Small Cap Fund Growth',

            ],

            [
              'scheme_code' => '101672',
              'scheme_name' => 'Tata Equity P/E Fund Growth',

            ],

            [
              'scheme_code' => '104481',
              'scheme_name' => 'DSP BlackRock Small & Mid Cap Fund Growth',

            ],


            /*Equity Funds ends*/




            /*Tax Saver Funds begins*/

            [
            	'scheme_code' => '107745',
            	'scheme_name' => 'Birla Sun Life Relief 96 Growth',

            ],

            [
              'scheme_code' => '103196',
              'scheme_name' => 'Reliance Tax Saver (ELSS) Fund-Growth Plan',

            ],

            [
            	'scheme_code' => '101979',
            	'scheme_name' => 'HDFC TaxSaver-Growth Plan',

            ],

            /*Tax Saver funds Ends*/


            /*New funds*/

            [
            	'scheme_code' => '100499',
            	'scheme_name' => 'Franklin India Dynamic Accrual Fund ',

            ],

            [
            	'scheme_code' => '101350',
            	'scheme_name' => 'ICICI Prudential Long term Plan ',

            ],

            [
            	'scheme_code' => '101862',
            	'scheme_name' => 'Reliance Banking Fund ',

            ],

            [
            	'scheme_code' => '102142',
            	'scheme_name' => 'Sundaram Rural India Fund ',

            ],

            [
            	'scheme_code' => '103174',
            	'scheme_name' => 'Birla Sunlife Frontline Equity',

            ],

            [
            	'scheme_code' => '103504',
            	'scheme_name' => 'SBI Blue Chip Fund ',

            ],

            [
            	'scheme_code' => '106235',
            	'scheme_name' => 'Reliance Top 200 Fund ',

            ],

            [
            	'scheme_code' => '112092',
            	'scheme_name' => 'Franklin Build India Fund  ',

            ],

            [
            	'scheme_code' => '112496',
            	'scheme_name' => 'L&T Midcap Fund ',

            ],

            [
            	'scheme_code' => '112600',
            	'scheme_name' => 'L&T Infrastructure Fund  ',

            ],

            [
            	'scheme_code' => '112936',
            	'scheme_name' => 'Reliance Regular Savings Fund - Debt Option ',

            ],

            [
            	'scheme_code' => '117716',
            	'scheme_name' => 'Kotak Income Opportunities Fund',

            ],

            [
            	'scheme_code' => '123690',
            	'scheme_name' => 'Kotak Banking & PSU Debt Fund',

            ],

            [
                'scheme_code' => '101537',
                'scheme_name' => 'Sundaram Select Focus',

            ],

            [
                'scheme_code' => '112096',
                'scheme_name' => 'ICIC Prudential Long term growth ',

            ],

            [
                'scheme_code' => '107249',
                'scheme_name' => 'Franklin India Ultra-short Bond Fund - Super Institutional - Growth',

            ],


        ]);
    }
}
