<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoricNsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historic_nses', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->double('opening_index');
            $table->double('high');
            $table->double('low');
            $table->double('closing_index');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('historic_nses');
    }
}
