
<!DOCTYPE html>
<html>
<head>
	<title>CIS PDF</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/cispdf.css">
</head>
<body>
	<header>
  	<div id="header-view">
  		<img class="logo" src = "icons/rfib_logo.png"/>
  		<div id="address-div">
  			<p class="m-b-0">Level 1, No 1, Balaji First Avenue,</p> 
  			<p class="m-b-0">T.Nagar,</p> 
  			<p class="m-b-0">Chennai - 600017</p>
  			<p class="m-b-0">Ph: +91 8825888200</p>
  		</div>
  	</div>
 </header>
<div id="info-div">
	<div id="title-div">
		<p id="name" style="padding-bottom: 5px;"><strong>Consolidated Investment Summary</strong><span style="font-size: 12px; font-weight: normal !important;">(as on {{$date}})</span> </p>
	</div>

	<div id="user-info">
		<div id="user-info-one">
			<p class="m-b-0"><strong>Name of Investor       :</strong><span style="font-size: 12px; font-weight: normal !important;">{{$name}}</span></p>	
      <p class="m-b-0"><strong>Address of Investor    :</strong><span style="font-size: 12px; font-weight: normal !important;">{{$address}}</span></p>
		</div>
		<div id="user-info-two">
      <p class="m-b-0"><strong>E-mail ID      :</strong><span style="font-size: 12px; font-weight: normal !important;">{{$email}}</span></p>
      <p class="m-b-0"><strong>Contact Number :</strong><span style="font-size: 12px; font-weight: normal !important;">{{$contact}}</span></p>
      <p class="m-b-0"><strong>PAN Number     :</strong><span style="font-size: 12px; font-weight: normal !important;">{{$pan}}</span></p>
		</div>
	</div>
<?php 
  $sip_inv_total = 0;
  $sip_current_total = 0;
  $one_time_inv_total = 0;
  $one_time_current_total = 0;
  $liquid_inv_total = 0;
  $liquid_current_total = 0;
  $fmt = new NumberFormatter( 'en_IN', NumberFormatter::DECIMAL );
 ?>
</div>
  <main>

  <?php if ($inv['one_time'] > 0): ?>
  <table class="table table-bordered">
          <thead class="table-head">
            <tr>
              <th>Scheme Name</th>
              <th>Folio Number</th>
              <th>Purchase date</th>
              <th>Amount Invested</th>
              <th>Purchase NAV</th>
              <th>Units</th>
              <th>Current NAV</th>
              <th>Current Market Value</th>
              <th>Unrealised Profit/Loss</th>
              <th>Absolute Returns</th>
              <th>Annualised Returns</th>  
            </tr>
          </thead>
          <tbody>
            @foreach($inv as $in)
            @if($in['invest_type'] == '1')
            <?php 
              $one_time_current_total += $in['Current Market Value'];
              $one_time_inv_total += $in['Amount Invested'];
             ?>
            <tr>
              <td style="width: 300px;">{{$in['Scheme Name']}}</td>
              <td style="width: 80px;">{{$in['Folio Number']}}</td>
              <td style="width: 70px;">{{$in['Purchase date']}}</td>
              <td><?php echo $fmt->format($in['Amount Invested']); ?></td>
              <td style="width: 50px;">{{$in['Purchase NAV']}}</td>
              <td>{{$in['Units']}}</td>
              <td>{{$in['Current NAV']}}</td>
              <td><?php echo $fmt->format(round($in['Current Market Value'], 2)); ?></td>
              <td><?php echo $fmt->format(round($in['Unrealised Profit/Loss']), 2); ?></td>
              <td><?php echo round($in['Absolute Returns'], 2); ?></td>
              <td><?php echo round($in['Annualised Returns'], 2); ?></td>  
            </tr>
            @endif
            @endforeach
          </tbody>
          <tr id="total-tr">
              <th style="text-align: left !important;">Total</th>
              <th></th>
              <th></th>
              <th><?php echo $fmt->format($one_time_inv_total); ?></th>
              <th></th>
              <th></th>
              <th></th>
              <th style="text-align: right;"><?php echo $fmt->format($one_time_current_total); ?></th>
              <th style="text-align: right;"><?php echo $fmt->format(round($one_time_current_total - $one_time_inv_total)); ?></th>
              <th style="text-align: right;"><?php echo round((($one_time_current_total - $one_time_inv_total)/$one_time_inv_total)*100, 2); ?></th>
              <th></th>
          </tr>
      </table>
  <?php endif ?>


    
  <?php if ($inv['sip'] > 0): ?>
          <p>
            <strong>SYSTEMATIC INVESTMENT PLAN</strong>
          </p>
    <table class="table table-bordered">
      <thead class="table-head">
        <tr>
          <th>Scheme Name</th>
          <th>Folio Number</th>
          <th>Purchase date</th>
          <th>Amount Invested</th>
          <th>Purchase NAV</th>
          <th>Units</th>
          <th>Current NAV</th>
          <th>Current Market Value</th>
          <th>Unrealised Profit/Loss</th>
          <th>Absolute Returns</th>
          <th>Annualised Returns</th>  
        </tr>
      </thead>
      <tbody>
        @foreach($inv as $in)
        @if($in['invest_type'] == '2')
        <?php 
          $sip_current_total += $in['Current Market Value'];
          $sip_inv_total += $in['Amount Invested'];
         ?>
        <tr>
          <td style="width: 300px;">{{$in['Scheme Name']}}</td>
          <td style="width: 80px;">{{$in['Folio Number']}}</td>
          <td style="width: 70px;">{{$in['Purchase date']}}</td>
          <td style="width: 60px;">{{$in['Amount Invested']}}</td>
          <td style="width: 50px;">{{$in['Purchase NAV']}}</td>
          <td>{{$in['Units']}}</td>
          <td style="width: 50px;">{{$in['Current NAV']}}</td>
          <td style="width: 70px;">{{round($in['Current Market Value'], 2)}}</td>
          <td>{{round($in['Unrealised Profit/Loss'], 2)}}</td>
          <td style="width: 60px;">{{round($in['Absolute Returns'], 2)}}</td>
          <td style="width: 70px;">{{round($in['Annualised Returns'], 2)}}</td>  
        </tr>
        @endif
        @endforeach
      </tbody>
          <tr id="total-tr">
              <th style="text-align: left !important;">Total</th>
              <th></th>
              <th></th>
              <th><?php echo $fmt->format($sip_inv_total); ?></th>
              <th></th>
              <th></th>
              <th></th>
              <th style="text-align: right;"><?php echo $fmt->format($sip_current_total) ?></th>
              <th style="text-align: right;"><?php echo $fmt->format(round($sip_current_total - $sip_inv_total)); ?></th>
              <th style="text-align: right;"><?php echo round((($sip_current_total - $sip_inv_total)/$sip_inv_total)*100, 2); ?></th>
              <th></th>
          </tr>
  </table>
  <?php endif ?>
    

  <?php if ($inv['liquid'] > 0): ?>
    <p><strong>LIQUID FUND</strong></p>
    <table class="table table-bordered">
      <thead class="table-head">
        <tr>
          <th>Scheme Name</th>
          <th>Folio Number</th>
          <th>Purchase date</th>
          <th>Amount Invested</th>
          <th>Purchase NAV</th>
          <th>Units</th>
          <th>Current NAV</th>
          <th>Current Market Value</th>
          <th>Unrealised Profit/Loss</th>
          <th>Absolute Returns</th>
          <th>Annualised Returns</th>  
        </tr>
      </thead>
      <tbody>
        @foreach($inv as $in)
        @if($in['invest_type'] == '3')
      <?php 
          $liquid_current_total += $in['Current Market Value'];
          $liquid_inv_total += $in['Amount Invested'];
         ?>
        <tr>
          <td style="width: 300px;">{{$in['Scheme Name']}}</td>
          <td style="width: 80px;">{{$in['Folio Number']}}</td>
          <td style="width: 70px;">{{$in['Purchase date']}}</td>
          <td style="width: 60px;">{{$in['Amount Invested']}}</td>
          <td style="width: 50px;">{{$in['Purchase NAV']}}</td>
          <td>{{$in['Units']}}</td>
          <td style="width: 50px;">{{$in['Current NAV']}}</td>
          <td style="width: 70px;">{{round($in['Current Market Value'], 2)}}</td>
          <td>{{round($in['Unrealised Profit/Loss'], 2)}}</td>
          <td style="width: 60px;">{{round($in['Absolute Returns'], 2)}}</td>
          <td style="width: 70px;">{{$in['Annualised Returns']}}</td>  
        </tr>
        @endif
        @endforeach
      </tbody>
          <tr id="total-tr">
              <th style="text-align: left !important;">Total</th>
              <th></th>
              <th></th>
              <th><?php echo $fmt->format($liquid_inv_total); ?></th>
              <th></th>
              <th></th>
              <th></th>
              <th style="text-align: right;"><?php echo $fmt->format($liquid_current_total); ?></th>
              <th style="text-align: right;"><?php echo $fmt->format(round($liquid_current_total - $liquid_inv_total, 2)); ?></th>
              <th style="text-align: right;"><?php echo round((($liquid_current_total - $liquid_inv_total)/$liquid_inv_total)*100, 2); ?></th>
              <th></th>
          </tr>
  </table>
  <?php endif ?>
    

    </main>
      <footer>
      	<div id="footer-view">
      	   <p id="footer-info">Disclaimer: Mutual Fund investments are subject to market risks, read all scheme related documents carefully before investing.</p>
	    </div>
      </footer>
</body>
</html>