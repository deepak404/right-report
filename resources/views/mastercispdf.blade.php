<?php $fmt = new NumberFormatter($locale = 'en_IN', NumberFormatter::DECIMAL);?>
<!DOCTYPE html>
<html>
<head>
	<title>CIS PDF</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/cispdf.css">
</head>
<body>
	<header>
  	<div id="header-view">
  		<img class="logo" src = "icons/rfib_logo.png"/>
  		<div id="address-div">
  			<p class="m-b-0">Level 1, No 1, Balaji First Avenue,</p> 
  			<p class="m-b-0">T.Nagar,</p> 
  			<p class="m-b-0">Chennai - 600017</p>
  			<p class="m-b-0">Ph: +91 8825888200</p>
  		</div>
  	</div>
 </header>
<div id="info-div">
	<div id="title-div">
		<p id="name" style="padding-bottom: 5px;"><strong>Consolidated Investment Summary</strong> <span style="font-size: 12px; font-weight: normal !important;">(as on {{date('d-m-Y',strtotime($date))}})</span></p>
	</div>


</div>
  <main>
	<table class="table">
          <thead class="table-head">
            <tr>
              <th>Scheme Name</th>
              <th>Amount Invested</th>
              <th>Current Market Value</th>
              <th>Unrealised Profit/Loss</th>
            </tr>
          </thead>
          <tbody>
            @foreach($inv as $in)
          	<tr style="padding: 0px; height: 20px!important;">
              <td style="width: 300px;">{{$in['Scheme Name']}}</td>
              <td style="float: right; width: 100px; text-align: right;"><?php echo $fmt->format($in['Amount Invested']); ?></td>
              <td style="float: right; width: 150px; text-align: right;"><?php echo $fmt->format($in['Current Market Value']) ?></td>
              <td style="float: right; width: 130px; text-align: right;"><?php echo $fmt->format($in['Unrealised Profit/Loss']) ?></td>
            </tr>
            @endforeach
          </tbody>
          <tr id="total-tr">
              <th style="text-align: left !important;">Total</th>
              <th style="text-align: right !important;"><?php echo $fmt->format($total); ?></th>
              <th style="text-align: right !important;"><?php echo $fmt->format($current); ?></th>
              <th style="text-align: right !important;"><?php echo $fmt->format($pl); ?></th>
          </tr>
        
      </table>
    </main>
      <footer>
      	<div id="footer-view">
      	   <p id="footer-info">Disclaimer: Mutual Fund investments are subject to market risks, read all scheme related documents carefully before investing.</p>
	    </div>
      </footer>
</body>
</html>