<!DOCTYPE html>
<html lang="en">
<head>
  <title>RightReport</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="css/login.css">
  <link rel="stylesheet" href="css/login-responsive.css">
  <link rel="stylesheet" href="css/helper.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400" rel="stylesheet">
</head>
<body>




  
<div class = "container">

    <div class="content">
        <div class=" row login-div">
          <img class = "login-logo center-block" src="img/Right-repor-logo.svg">
          <p class="login-info-text mont-light">Log into your account</p>
          <p class = "text-center blue" id = "gen_rep">Generate Reports on the go!</p>

          <div class=" col-lg-12 col-sm-12 col-md-12 col-xs-12 form-div padding-lr-zero">
            <form action="/login" role="form" method="POST" action="{{ url('/login') }}">
            {{ csrf_field() }}
              <div class="form-group">
                <input type="email" name="email" id = "email" required placeholder="Enter your email">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              </div>

              <div class="form-group">
                <input type="password" name="password" id = "password" required placeholder="Password">
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
              </div>

              <div class="form-group">
                <input type="submit" class = " center-block btn btn-primary blue-btn" id="login-btn" value="Login" id = "submit">
              </div>

            </form>
          </div>
        </div>
    </div> <!--Content Ends -->

</div>


</body>
</html>
